﻿using System;
using SFML.Graphics;

namespace trafficsim.Helper
{
    /// <summary>
    /// Helper class for converting color space from HSV to RGB.
    /// </summary>
    public static class HSVColor
    {
        /// <param name="hueDegrees">Hue, in degrees.</param>
        /// <param name="saturation">Saturation (from 0 to 1).</param>
        /// <param name="value">Value (from 0 to 1).</param>
        /// <returns>SFML color object equivalent to specified HSV color data.</returns>
        public static Color FromHSV(double hueDegrees, double saturation, double value)
        {
            return new Color(
                (byte) Math.Min(255, F(5, hueDegrees, saturation, value) * 256),
                (byte) Math.Min(255, F(3, hueDegrees, saturation, value) * 256),
                (byte) Math.Min(255, F(1, hueDegrees, saturation, value) * 256)
                );
        }

        private static double F(int n, double hueDegrees, double saturation, double value)
        {
            double k = (n + hueDegrees / 60.0) % 6;
            return value - value * saturation * Math.Max(Math.Min(1, Math.Min(k, 4 - k)), 0);
        }
    }
}