﻿namespace trafficsim.Helper
{
    /// <summary>
    /// Wrapper class for holding a reference to basic type value.
    /// </summary>
    /// <typeparam name="T">A basic type.</typeparam>
    public class Ref<T> where T : struct
    {
        /// <summary>
        /// Object's stored value.
        /// </summary>
        public T Value { get; set; }

        /// <param name="value">Value to wrap in a referencable object.</param>
        public Ref(T value)
        {
            Value = value;
        }
    }
}