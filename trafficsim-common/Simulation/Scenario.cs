﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using SFML.Graphics;
using trafficsim.Helper;
using trafficsim.Simulation.Factory;
using trafficsim.Simulation.Intersection;
using trafficsim.Simulation.Model;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Struct;

namespace trafficsim.Simulation
{
    /// <summary>
    /// Class responsible for managing simulation ticking and drawing.
    /// </summary>
    public class Scenario
    {
        /// <summary>
        /// Radius, in meters, of always visible area.
        /// </summary>
        public double ViewRadius { set; get; } = 40;
        /// <summary>
        /// The speed of the simulation. TimeWarp = 0.5 means simulation at half real time. TimeWarp = 2 meas simulation
        /// at twice real time.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Thrown for TimeWarp smaller than .</exception>
        public double TimeWarp
        {
            get => _timeWarp;
            set
            {
                _timeWarp = value;
                if (_timeWarp < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
                _executionCountPerTick = Math.Max(1, (int) Math.Floor(_timeWarp));
                _timeDiffMultiplier = _timeWarp / _executionCountPerTick;
            }
        }
        /// <summary>
        /// Amount of time simulated in seconds.
        /// </summary>
        public double TimePassed { get; private set; } = 0;
        /// <summary>
        /// Amount of agents who finished their paths.
        /// </summary>
        public Ref<int> TotalThroughput { get; set; } = new Ref<int>(0);
        
        public Ref<int> TotalAgentsSpawned { get; set; } = new Ref<int>(0);

        public Ref<double> AgentLifetimeSum { get; set; } = new Ref<double>(0);

        /// <summary>
        /// List of all road segments this scenario is made out of.
        /// </summary>
        public readonly List<IRoadSegment> RoadSegments = null;
        /// <summary>
        /// Reference to agents pool size.
        /// </summary>
        public Ref<int> AgentLimit { get; set; } = null;
        /// <summary>
        /// List of all intersection managers present in this scenario.
        /// </summary>
        public List<IIntersectionManager> Intersections { get; } = null;
        /// <summary>
        /// Backing field for TimeWarp.
        /// </summary>
        private double _timeWarp = 1;
        /// <summary>
        /// Multiplier for the amount of time processed in each simulation tick.
        /// </summary>
        private double _timeDiffMultiplier = 1;
        /// <summary>
        /// Number of simulation ticks between each redraw.
        /// </summary>
        private int _executionCountPerTick = 1;

        /// <param name="roadSegments">IEnumerable of all road segments this scenario will be made out of.</param>
        /// <param name="intersections">IEnumerable of all intersection managers present for this scenario.</param>
        public Scenario(IEnumerable<IRoadSegment> roadSegments, IEnumerable<IIntersectionManager> intersections = null)
        {
            RoadSegments = new List<IRoadSegment>(roadSegments);
            Intersections = intersections != null ? new List<IIntersectionManager>(intersections) : new List<IIntersectionManager>();
        }

        /// <summary>
        /// Constructs a scenario using data from a file.
        /// </summary>
        /// <param name="path">Path to file containing scenario data.</param>
        /// <returns>New scenario object with road segments and intersections.</returns>
        /// <exception cref="XmlSchemaException">
        /// If input file fails schema validation.
        /// </exception>
        /// <exception cref="XmlException">
        /// If input file does not exist, or file has errors other than related to schema.
        /// </exception>
        public static Scenario FromFile(string path)
        {
            XmlTextReader schemaReader = new XmlTextReader(AppDomain.CurrentDomain.BaseDirectory + "Resources/Schema.xsd");
            XmlSchema schema = XmlSchema.Read(schemaReader, (_, args) => throw args.Exception);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Schemas.Add(schema);
            XmlReaderSettings settings = new XmlReaderSettings {IgnoreComments = true};
            XmlReader reader = XmlReader.Create(path, settings);
            xmlDoc.Load(reader);
            xmlDoc.Validate((_, args) => throw args.Exception);
            XmlNode root = xmlDoc.DocumentElement;

            uint viewRadius = root.Attributes["ViewRadius"] != null ? uint.Parse(root.Attributes["ViewRadius"].Value) : 40;
            Ref<int> agentLimit = root.Attributes["AgentLimit"] != null ? new Ref<int>(int.Parse(root.Attributes["AgentLimit"].Value)) : null;
            Ref<int> throughput = new Ref<int>(0);
            Ref<int> totalAgentsSpawned = new Ref<int>(0);
            Ref<double> lifetimeSum = new Ref<double>(0);

            List<VehicleClass> vehicleClasses;

            if (root["VehicleClasses"] != null)
            {
                vehicleClasses = new List<VehicleClass>();
                foreach (XmlNode vehicleClassNode in root["VehicleClasses"])
                {
                    VehicleClass vehicleClass = new VehicleClass
                    {
                        Probability = double.Parse(vehicleClassNode.Attributes[0].Value, new CultureInfo("en"))
                    };
                    foreach (XmlNode paramNode in vehicleClassNode.ChildNodes)
                    {
                        switch (paramNode.Name)
                        {
                            case "Width": vehicleClass.Width = ParseMinMaxAttributes(paramNode); break;
                            case "Length": vehicleClass.Length = ParseMinMaxAttributes(paramNode); break;
                            case "SafeDecelerationThreshold": vehicleClass.SafeDecelerationThreshold = ParseMinMaxAttributes(paramNode); break;
                            case "LaneChangeTime": vehicleClass.LaneChangeTime = ParseMinMaxAttributes(paramNode); break;
                            case "SafeTimeHeadway": vehicleClass.SafeTimeHeadway = ParseMinMaxAttributes(paramNode); break;
                            case "MaximumAcceleration": vehicleClass.MaximumAcceleration = ParseMinMaxAttributes(paramNode); break;
                            case "ComfortableDeceleration": vehicleClass.ComfortableDeceleration = ParseMinMaxAttributes(paramNode); break;
                            case "AccelerationExponent": vehicleClass.AccelerationExponent = ParseMinMaxAttributes(paramNode); break;
                            case "MinimumDistance": vehicleClass.MinimumDistance = ParseMinMaxAttributes(paramNode); break;
                            case "Politeness": vehicleClass.Politeness = ParseMinMaxAttributes(paramNode); break;
                            case "LaneChangeThreshold": vehicleClass.LaneChangeThreshold = ParseMinMaxAttributes(paramNode); break;
                            case "RegardForSpeedLimit": vehicleClass.RegardForSpeedLimit = ParseMinMaxAttributes(paramNode); break;
                        }
                    }
                    vehicleClasses.Add(vehicleClass);
                }
            }
            else
            {
                vehicleClasses = VehicleClass.GetDefaultVehicleClasses();
            }

            MultiClassVehicleFactory factory = new MultiClassVehicleFactory(vehicleClasses);

            List<IRoadSegment> segmentsList = new List<IRoadSegment>();
            Dictionary<string, IRoadSegment> segmentsMap = new Dictionary<string, IRoadSegment>();
            Dictionary<string, IntersectionRoadSegmentType> intersectionRoadSegmentTypes = new Dictionary<string, IntersectionRoadSegmentType>();
            Dictionary<IRoadSegment, List<(string id, double probability, TurnDirection direction)>> segmentsForwardConnections = new Dictionary<IRoadSegment, List<(string id, double probability, TurnDirection direction)>>();
            Dictionary<IRoadSegment, List<(string id, double bias)>> segmentsSidewaysConnections = new Dictionary<IRoadSegment, List<(string id, double bias)>>();
            Dictionary<SpawnerSegment, List<string>> spawnersRoutes = new Dictionary<SpawnerSegment, List<string>>();

            foreach (XmlNode intersectionNode in root["Intersections"])
            {
                IntersectionRoadSegmentType type;
                switch (intersectionNode.Name)
                {
                    case "RightHandRuleIntersectionManager":
                        type = IntersectionRoadSegmentType.RightHandRuleIntersectionSegment;
                        break;
                    case "TrafficLightsIntersectionManager":
                        type = IntersectionRoadSegmentType.TrafficLightsIntersectionSegment;
                        break;
                    default:
                        type = IntersectionRoadSegmentType.RightOfWayIntersectionSegment;
                        break;
                }

                foreach (XmlNode crossingNode in intersectionNode["PriorityRules"])
                {
                    intersectionRoadSegmentTypes[crossingNode.Attributes["PrioritySegmentId"].Value] = type;
                    intersectionRoadSegmentTypes[crossingNode.Attributes["YieldingSegmentId"].Value] = type;
                }

            }

            foreach (XmlNode segmentNode in root["RoadSegments"])
            {
                IRoadSegment segment = null;

                switch (segmentNode.Name)
                {
                    case "SpawnerSegment":
                        segment = new SpawnerSegment(factory)
                        {
                            SpawnFrequency = double.Parse(segmentNode.Attributes["SpawnFrequency"].Value, new CultureInfo("en")),
                            AgentLimit = agentLimit,
                            Id = segmentNode.Attributes["Id"].Value,
                            TotalAgentsSpawned = totalAgentsSpawned
                        };
                        segmentsForwardConnections.Add(segment, new List<(string id, double probability, TurnDirection direction)>(SegmentNodeNext(segmentNode)));
                        spawnersRoutes.Add(segment as SpawnerSegment, new List<string>(SpawnerNodeRoute(segmentNode)));
                        break;
                    case "DespawnerSegment":
                        segment = new DespawnerSegment()
                        {
                            AgentLimit = agentLimit,
                            Throughput = throughput,
                            AgentLifetimeSum = lifetimeSum
                        };
                        break;
                    case "RoadSegment":
                        List<Vector> vertices = new List<Vector>(SegmentNodeVertices(segmentNode));
                        if (intersectionRoadSegmentTypes.ContainsKey(segmentNode.Attributes["Id"].Value))
                        {
                            switch (intersectionRoadSegmentTypes[segmentNode.Attributes["Id"].Value])
                            {
                                case IntersectionRoadSegmentType.RightHandRuleIntersectionSegment:
                                    segment = new RightHandRuleIntersectionSegment(vertices);
                                    break;
                                case IntersectionRoadSegmentType.TrafficLightsIntersectionSegment:
                                    segment = new TrafficLightsIntersectionSegment(vertices);
                                    break;
                                default:
                                    segment = new RightOfWayIntersectionSegment(vertices);
                                    break;
                            }
                        }
                        else
                        {
                            segment = new RegularRoadSegment(vertices);
                        }

                        if (segmentNode.Attributes["SpeedLimit"] != null)
                        {
                            (segment as RegularRoadSegment).SpeedLimit =
                                double.Parse(segmentNode.Attributes["SpeedLimit"].Value, new CultureInfo("en"));
                        }
                        segmentsForwardConnections.Add(segment, new List<(string id, double probability, TurnDirection direction)>(SegmentNodeNext(segmentNode)));
                        segmentsSidewaysConnections.Add(segment, new List<(string id, double bias)>(SegmentNodeAdjacent(segmentNode)));
                        break;
                }

                if (segment != null)
                {
                    segmentsList.Add(segment);
                    if (segmentNode.Attributes["Id"] != null)
                    {
                        if (segmentsMap.ContainsKey(segmentNode.Attributes["Id"].Value))
                        {
                            throw new XmlException("Duplicate identifier \"" + segmentNode.Attributes["Id"].Value + "\".");
                        }
                        segmentsMap[segmentNode.Attributes["Id"].Value] = segment;
                    }
                }
            }

            foreach (KeyValuePair<IRoadSegment, List<(string id, double probability, TurnDirection direction)>> pair in segmentsForwardConnections.Where(pair => pair.Value.Any()))
            {
                foreach ((string id, double probability, TurnDirection direction) nextInfo in pair.Value)
                {
                    if (!segmentsMap.ContainsKey(nextInfo.id))
                    {
                        throw new XmlException("Tried to reference segment id \"" + nextInfo.id + "\", but no such segment exists.");
                    }
                    IRoadSegment nextSegment = segmentsMap[nextInfo.id];
                    pair.Key.AddNext(nextSegment, nextInfo.probability, nextInfo.direction);
                }
            }

            foreach (KeyValuePair<IRoadSegment, List<(string id, double bias)>> pair in segmentsSidewaysConnections.Where(pair => pair.Value.Any()))
            {
                foreach ((string id, double bias) adjacentInfo in pair.Value)
                {
                    if (!segmentsMap.ContainsKey(adjacentInfo.id))
                    {
                        throw new XmlException("Tried to reference segment id \"" + adjacentInfo.id + "\", but no such segment exists.");
                    }
                    if (!(segmentsMap[adjacentInfo.id] is RegularRoadSegment))
                    {
                        throw new XmlException("Segment with id \"" + adjacentInfo.id + "\" is not a regular road segment and therefore can't have an adjacent connection.");
                    }
                    if (segmentsMap[adjacentInfo.id] is RightOfWayIntersectionSegment)
                    {
                        throw new XmlException("Segment with id \"" + adjacentInfo.id + "\" is an intersection segment and therefore can't have an adjacent connection.");
                    }
                    IRoadSegment adjacentSegment = segmentsMap[adjacentInfo.id];
                    (pair.Key as RegularRoadSegment).AdjacentLanes.Add(new AdjacentLaneInfo
                    {
                        Lane = adjacentSegment as RegularRoadSegment,
                        Bias = adjacentInfo.bias
                    });
                }
            }

            foreach (KeyValuePair<SpawnerSegment, List<string>> routePair in spawnersRoutes)
            {
                if (routePair.Value.Any())
                {
                    routePair.Key.Route = new HashSet<IRoadSegment>(segmentsMap
                        .Where(segmentIdPair => routePair.Value.Contains(segmentIdPair.Key))
                        .Select(segmentIdPair => segmentIdPair.Value));
                }
            }

            List<IIntersectionManager> intersections = new List<IIntersectionManager>();

            foreach (XmlNode intersectionNode in root["Intersections"])
            {
                switch (intersectionNode.Name)
                {
                    case "RightHandRuleIntersectionManager":
                        intersections.Add(new RightHandRuleIntersectionManager(
                            IntersectionSegmentNodePriorityRules<RightHandRuleIntersectionSegment>(intersectionNode, segmentsMap),
                            intersectionNode["SidesClockwise"].Cast<XmlNode>().Select(sideNode =>
                                 sideNode.ChildNodes.Cast<XmlNode>()
                                     .Where(sideSegmentNode =>
                                         segmentsMap.ContainsKey(sideSegmentNode.Attributes["Id"].Value))
                                     .Select(sideSegmentNode =>
                                         segmentsMap[sideSegmentNode.Attributes["Id"].Value] as
                                             RightHandRuleIntersectionSegment)
                            )
                        ));
                        break;
                    case "TrafficLightsIntersectionManager":
                        intersections.Add(new TrafficLightsIntersectionManager(
                            IntersectionSegmentNodePriorityRules<TrafficLightsIntersectionSegment>(intersectionNode, segmentsMap),
                            intersectionNode["TrafficLightsPhases"].Cast<XmlNode>().Select(phaseNode =>
                                new TrafficLightsPhase
                                {
                                    PhaseLengthSeconds = double.Parse(phaseNode.Attributes["Length"].Value),
                                    OpenSegments = phaseNode.ChildNodes.Cast<XmlNode>()
                                        .Where(openSegmentNode =>
                                            segmentsMap.ContainsKey(openSegmentNode.Attributes["Id"].Value))
                                        .Select(openSegmentNode =>
                                            segmentsMap[openSegmentNode.Attributes["Id"].Value] as
                                                TrafficLightsIntersectionSegment)
                                }
                            )
                        ));
                        break;
                    case "RightOfWayIntersectionManager":
                        intersections.Add(new RightOfWayIntersectionManager(
                            IntersectionSegmentNodePriorityRules<RightOfWayIntersectionSegment>(intersectionNode, segmentsMap))
                        );
                        break;
                }
            }

            return new Scenario(segmentsList, intersections) { ViewRadius = viewRadius, TotalThroughput = throughput, TotalAgentsSpawned = totalAgentsSpawned, AgentLifetimeSum = lifetimeSum, AgentLimit = agentLimit };
        }

        /// <summary>
        /// Computes all intersections and ticks all road segments (including updating agents and moving them). Takes
        /// into account TimeWarp.
        /// </summary>
        /// <param name="timeDiff">
        /// Amount of time between executions of this method. This is unrelated to TimeWarp. If simulation is displayed
        /// at 60 frames per second, then run this method between each redraw with timeDiff = 1/60 s.
        /// </param>
        public void DoSimulationTick(double timeDiff)
        {
            if (_timeDiffMultiplier == 0)
            {
                return;
            }
            
            for (int i = 0; i < _executionCountPerTick; ++i)
            {
                TimePassed += timeDiff * _timeDiffMultiplier;
                foreach (IIntersectionManager intersection in Intersections)
                {
                    intersection.ComputeIntersection(timeDiff * _timeDiffMultiplier);
                }
                foreach (IRoadSegment segment in RoadSegments)
                {
                    segment.SimulateAccelerationChanges();
                }
                foreach (IRoadSegment segment in RoadSegments)
                {
                    segment.UpdateAgentsPositionsAndVelocities(timeDiff * _timeDiffMultiplier);
                }
                foreach (IRoadSegment segment in RoadSegments)
                {
                    segment.MoveAgentsToRespectiveSegments();
                }
                foreach (IRoadSegment segment in RoadSegments)
                {
                    segment.SimulateLaneChanges();
                }
            }
        }

        /// <summary>
        /// Draws all road segments and agents on them.
        /// </summary>
        /// <param name="window">SFML window object to draw to.</param>
        public void Draw(RenderWindow window)
        {
            foreach (IRoadSegment segment in RoadSegments)
            {
                segment.DrawRoad(window);
            }
            foreach (IRoadSegment segment in RoadSegments)
            {
                segment.DrawAgents(window);
            }
        }

        public double GetScore()
        {
            return 1.0 / (TotalAgentsSpawned.Value - TotalThroughput.Value);
        }

        /// <returns>True if all routes are legal. See <see cref="SpawnerSegment.RouteHasDeadEnds"/></returns>
        public bool HasDeadEndRoutes()
        {
            return RoadSegments.Where(s => s is SpawnerSegment).Cast<SpawnerSegment>().Any(spawner => spawner.RouteHasDeadEnds());
        }

        /// <summary>
        /// Parses XML node with Min and Max attributes of type double.
        /// </summary>
        /// <param name="paramNode">XML node object.</param>
        /// <param name="minAttrName">Name of the Min attribute (if any other than "Min").</param>
        /// <param name="maxAttrName">Name of the Max attribute (if any other than "Max").</param>
        /// <returns>Tuple containing Min and Max values.</returns>
        private static (double min, double max) ParseMinMaxAttributes(XmlNode paramNode, string minAttrName = "Min", string maxAttrName = "Max")
        {
            return (min: double.Parse(paramNode.Attributes[minAttrName].Value, new CultureInfo("en")), max: double.Parse(paramNode.Attributes[maxAttrName].Value, new CultureInfo("en")));
        }

        /// <summary>
        /// Parses XML node of vectors.
        /// </summary>
        /// <param name="segmentNode">
        /// XML node object, whose children nodes are vectors (have attributes X and Y of type double).
        /// </param>
        /// <returns>IEnumerable of vectors.</returns>
        private static IEnumerable<Vector> SegmentNodeVertices(XmlNode segmentNode)
        {
            return segmentNode["Vertices"].Cast<XmlNode>().Select(node =>
                new Vector(double.Parse(node.Attributes["X"].Value, new CultureInfo("en")), double.Parse(node.Attributes["Y"].Value, new CultureInfo("en"))));
        }

        /// <summary>
        /// Parses XML node containing next road segments data.
        /// </summary>
        /// <param name="segmentNode">
        /// XML node object with children named Next with attributes SegmentId (string), Probability (double) and
        /// Direction (one of: "Left", "Right", "Forward").
        /// </param>
        /// <returns>
        /// IEnumerable of tuples with segment id, probability and turn direction.
        /// </returns>
        private static IEnumerable<(string id, double probability, TurnDirection direction)> SegmentNodeNext(XmlNode segmentNode)
        {
            return segmentNode["Next"]?.Cast<XmlNode>().Select(node => (
                id: node.Attributes["SegmentId"].Value,
                probability: double.Parse(node.Attributes["Probability"]?.Value ?? "1", new CultureInfo("en")),
                direction: GetTurnDirection(node.Attributes["Direction"]?.Value ?? "Forward")
            )) ?? new (string id, double probability, TurnDirection direction)[] {};
        }

        /// <summary>
        /// Parses XML node containing adjacent road segments data.
        /// </summary>
        /// <param name="segmentNode">
        /// XML node object with children named Adjacent with attributes SegmentId (string) and Bias (double).
        /// </param>
        /// <returns>
        /// IEnumerable of tuples with segment id and bias.
        /// </returns>
        private static IEnumerable<(string id, double bias)> SegmentNodeAdjacent(XmlNode segmentNode)
        {
            return segmentNode["Adjacent"]?.Cast<XmlNode>().Select(node => (
                id: node.Attributes["SegmentId"].Value,
                bias: double.Parse(node.Attributes["Bias"]?.Value ?? "0", new CultureInfo("en"))
            )) ?? new (string id, double bias)[] {};
        }

        /// <summary>
        /// Parses XML node containing route data.
        /// </summary>
        /// <param name="spawnerNode">
        /// XML node object with children named Segment with attribute SegmentId (string).
        /// </param>
        /// <returns>
        /// IEnumerable segment ids.
        /// </returns>
        private static IEnumerable<string> SpawnerNodeRoute(XmlNode spawnerNode)
        {
            return spawnerNode["Route"]?.Cast<XmlNode>().Select(node => node.Attributes["SegmentId"].Value) ?? new string[] {};
        }

        private static IEnumerable<(T prioritySegment, T yieldingSegment)>
            IntersectionSegmentNodePriorityRules<T>(XmlNode intersectionSegmentNode, Dictionary<string, IRoadSegment> segmentsMap)
            where T : RightOfWayIntersectionSegment
        {
            return intersectionSegmentNode["PriorityRules"]
                .Cast<XmlNode>()
                .Where(ruleNode => segmentsMap.ContainsKey(ruleNode.Attributes["PrioritySegmentId"].Value) &&
                                   segmentsMap.ContainsKey(ruleNode.Attributes["YieldingSegmentId"].Value))
                .Select(ruleNode => (
                    prioritySegment: segmentsMap[ruleNode.Attributes["PrioritySegmentId"].Value] as T,
                    yieldingSegment: segmentsMap[ruleNode.Attributes["YieldingSegmentId"].Value] as T
                ));
        }

        /// <summary>
        /// Parses turn direction string.
        /// </summary>
        /// <param name="directionString">"Forward", "Left" or "Right".</param>
        /// <returns>Respectively TurnDirection.Forward, TurnDirection.Left or TurnDirection.Right.</returns>
        private static TurnDirection GetTurnDirection(string directionString)
        {
            switch (directionString)
            {
                case "Left": return TurnDirection.Left;
                case "Right": return TurnDirection.Right;
                default: return TurnDirection.Forward;
            }
        }
    }
}