﻿using System.Collections.Generic;

namespace trafficsim.Simulation.Vehicle
{
    /// <summary>
    /// Class for sorting SortedSets of Agents
    /// </summary>
    public class AgentPositionComparer: IComparer<Agent>
    {
        /// <summary>
        /// Method to compare Agents' positions.
        /// </summary>
        /// <param name="x">First agent.</param>
        /// <param name="y">Second agent.</param>
        /// <returns>Result of comparison (see <see cref="System.Collections.Generic.IComparer.Compare"/>).</returns>
        public int Compare(Agent x, Agent y)
        {
            if ((x?.Position ?? 0) < (y?.Position ?? 0)) return -1;
            else if ((x?.Position ?? 0) > (y?.Position ?? 0)) return 1;
            else return 0;
        }
    }
}