﻿using System;
using System.Collections.Generic;
using System.Windows;
using SFML.Graphics;
using SFML.System;
using trafficsim.Helper;
using trafficsim.Simulation.Model;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Struct;

namespace trafficsim.Simulation.Vehicle
{
    /// <summary>
    /// Class for managing physical properties of the vehicle, updating them and making decisions using behavioral
    /// models.
    /// </summary>
    public class Agent
    {
        /// <summary>
        /// Longitudinal position of the vehicle on the road segment the agent is on in meters.
        /// </summary>
        public double Position { get; set; } = 0;
        /// <summary>
        /// Forward velocity of the vehicle in meters per second.
        /// </summary>
        public double Velocity { get; set; } = 0;
        /// <summary>
        /// Forward acceleration of the vehicle in meters per second square.
        /// </summary>
        public double Acceleration { get; set; } = 0;
        /// <summary>
        /// Length of the vehicle in meters.
        /// </summary>
        public double Length { set; get; } = 4;
        /// <summary>
        /// Width of the vehicle in meters.
        /// </summary>
        /// <remarks>
        /// Affects only visuals, does not matter for the simulation.
        /// </remarks>
        public double Width { set; get; } = 1.8;
        /// <summary>
        /// Maximum deceleration this vehicle can apply in meters per second squared.
        /// </summary>
        /// <remarks>
        /// Though this is a physical property of the vehicle, this is not a hard limit and is not enforced while
        /// applying acceleration to the velocity. It is there as a parameter to behavioral models and they are
        /// responsible for obeying it.
        /// </remarks>
        public double SafeDecelerationThreshold = 4;
        /// <summary>
        /// Amount of time necessary to change lanes in seconds.
        /// </summary>
        /// <remarks>
        /// While changing lanes, agent can't again choose to change lane.
        /// </remarks>
        public double LaneChangeTime { get; set; } = 3;
        /// <summary>
        /// Color override for visualisation.
        /// </summary>
        /// <remarks>
        /// When null (default), vehicle's color is determined by its velocity.
        /// </remarks>
        public Color? Color { get; set; } = null;
        /// <summary>
        /// Multiple of the speed limit, that this agent will try to adhere to.
        /// </summary>
        /// <remarks>
        /// For example with RegardForSpeedLimit = 1.2 agent driving on road with speed limit 12 m/s, the agent will try
        /// to maintain speed 14.4 m/s.
        /// </remarks>
        public double RegardForSpeedLimit { get; set; } = 1;
        /// <summary>
        /// The lane this agent is right about to change to. When set to other than null, the agent will change lane
        /// within the same simulation tick and this value will be reset to null.
        /// </summary>
        public RegularRoadSegment NewLane { get; private set; } = null;
        /// <summary>
        /// Collection of road segments, which this agent can take.
        /// </summary>
        public ICollection<IRoadSegment> Route { get; set; } = null;
        /// <summary>
        /// Number of agents who finished this route.
        /// </summary>
        public Ref<int> RouteThroughput { get; set; } = null;
        /// <summary>
        /// Lifetime of the agent in seconds.
        /// </summary>
        public double Lifetime { get; set; } = 0;

        public bool Selected = false;
        /// <summary>
        /// Car following behavioral model object.
        /// </summary>
        private readonly ICarFollowingModel _carFollowingModel = null;
        /// <summary>
        /// Lane changing behavioral model object.
        /// </summary>
        private readonly ILaneChangeModel _laneChangeModel = null;
        /// <summary>
        /// Cache map of decisions for decisions on road splits.
        /// </summary>
        private readonly Dictionary<IRoadSegment, IRoadSegment> _roadSplitsDecisions =
            new Dictionary<IRoadSegment, IRoadSegment>();
        /// <summary>
        /// Approximate position of the center of front axis of the vehicle. Used to determine vehicle position on
        /// euclidean plane.
        /// </summary>
        private Vector _frontXyPosition;
        /// <summary>
        /// Approximate position of the center of back axis of the vehicle. Used to determine the rotation of the
        /// vehicle on euclidean plane.
        /// </summary>
        private Vector? _tailXyPosition = null;
        /// <summary>
        /// Road segment this agent is changing lane from. Used to interpolate position (on plane) when changing lanes.
        /// </summary>
        private RegularRoadSegment _oldLane = null;
        /// <summary>
        /// Progress of lane change, where 0 is start and 1 is finish.
        /// </summary>
        private double _laneChangeProgress = 1;
        /// <summary>
        /// Random number generator. Set up for repeatability.
        /// </summary>
        private static readonly Random Random = new Random(0);
        /// <summary>
        /// Amount of time in seconds between turn signal is lit up each period.
        /// </summary>
        private readonly double _turnSignalHalfPeriod;
        /// <summary>
        /// Progress of turn signal half period, where 0 is start and 1 is finish.
        /// </summary>
        private double _turnSignalProgress = 0;
        /// <summary>
        /// Turn signal state. _turnSignalLit = true does not necessarily mean the turn signal should be lit right now.
        /// It is merely used as a square wave for turn signal blinking. Whether turn signal should be blinking, or
        /// turned off, is determined on the fly.
        /// </summary>
        private bool _turnSignalLit = false;

        /// <summary>
        /// Sets up agent with behavioral model and draws random turn signal blinking period. Optionally predetermines
        /// agent split decisions.
        /// </summary>
        /// <param name="carFollowingModel">Car following behavior model object.</param>
        /// <param name="laneChangeModel">Lane changing behavior model object.</param>
        /// <param name="roadSplitsDecisions">Predetermined turn decisions map.</param>
        public Agent(ICarFollowingModel carFollowingModel, ILaneChangeModel laneChangeModel,
            IDictionary<IRoadSegment, IRoadSegment> roadSplitsDecisions = null)
        {
            _carFollowingModel = carFollowingModel;
            _laneChangeModel = laneChangeModel;
            if (roadSplitsDecisions != null)
            {
                _roadSplitsDecisions = new Dictionary<IRoadSegment, IRoadSegment>(roadSplitsDecisions);
            }

            _turnSignalHalfPeriod = Random.NextDouble() * 0.2 + 0.4;
        }

        /// <summary>
        /// Applies velocity to position and updates lane change and turn signal progress.
        /// </summary>
        /// <param name="timeDiff">Amount of simulated time passage in seconds.</param>
        public void UpdatePosition(double timeDiff)
        {
            Position += Velocity * timeDiff;

            if (_laneChangeProgress < 1)
            {
                _laneChangeProgress += timeDiff / LaneChangeTime;
            }
            else
            {
                _oldLane = null;
            }

            if (_turnSignalProgress < 1)
            {
                _turnSignalProgress += timeDiff / _turnSignalHalfPeriod;
            }
            else
            {
                _turnSignalProgress = 0;
                _turnSignalLit = !_turnSignalLit;
            }

        }

        /// <summary>
        /// Applies acceleration to velocity.
        /// </summary>
        /// <param name="timeDiff">Amount of simulated time passage in seconds.</param>
        public void UpdateVelocity(double timeDiff)
        {
            Velocity = Math.Max(0.0, Velocity + Acceleration * timeDiff);
        }

        /// <summary>
        /// Updates acceleration according to car following model.
        /// </summary>
        /// <param name="obstacleInfo">Nearest obstacle in front of the vehicle.</param>
        /// <param name="speedLimit">Current speed limit in meters per second for the agent.</param>
        public virtual void UpdateAcceleration(ObstacleInfo obstacleInfo, double speedLimit)
        {
            Acceleration = GetAccelerationForObstacle(obstacleInfo, speedLimit);
        }

        /// <summary>
        /// Gets acceleration this agent would apply given an obstacle and speed limit.
        /// </summary>
        /// <param name="obstacleInfo">Obstacle that would be nearest in front of the agent.</param>
        /// <param name="speedLimit">Speed limit in meters per second.</param>
        /// <returns>Acceleration in meters per second squared</returns>
        public double GetAccelerationForObstacle(ObstacleInfo obstacleInfo, double speedLimit)
        {
            return _carFollowingModel?.GetAcceleration(Velocity, obstacleInfo, speedLimit * RegardForSpeedLimit)
                   ?? Acceleration;
        }

        /// <summary>
        /// Makes decision on road split, commits to this decision and returns the desired next road segment. If cached
        /// decision exists, it returns it. Otherwise it makes new decision and caches is.
        /// </summary>
        /// <param name="from">Road segment the decision is made from.</param>
        /// <returns>The road segment the agent wants to move to.</returns>
        public IRoadSegment DecideOnSegmentChange(IRoadSegment from)
        {
            if (!_roadSplitsDecisions.ContainsKey(from))
            {
                IRoadSegment next = from.GetRandomNext(Route);
                if (next != null)
                {
                    _roadSplitsDecisions[from] = next;
                }
                else
                {
                    return next;
                }
            }

            return _roadSplitsDecisions[from];
        }

        /// <summary>
        /// Discards segment change cache for a given starting road segment, discards lane change decision, updates
        /// _oldLane to next segment after _oneLane (if it has exactly one successor).
        /// </summary>
        /// <param name="from">Road segment the agent moved from.</param>
        public void ConfirmSegmentChange(IRoadSegment from)
        {
            ForgetDecisionOnSegmentChange(from);
            _oldLane = _oldLane?.GetNextRegularRoadSegment();
            NewLane = null;
        }

        /// <summary>
        /// Discards segment change cache for a given starting road segment.
        /// </summary>
        /// <param name="from">Road segment the agent would have moved from.</param>
        public void ForgetDecisionOnSegmentChange(IRoadSegment from)
        {
            if (_roadSplitsDecisions.ContainsKey(from))
            {
                _roadSplitsDecisions.Remove(from);
            }
        }

        /// <summary>
        /// Makes a decision whether to change to a given lane assuming that it is possible and safe, and commits to
        /// this decision.
        /// </summary>
        /// <param name="adjacentLane">
        /// Information about adjacent lane in question.
        /// </param>
        /// <param name="newAcceleration">
        /// Acceleration this agent would have if it changed lane.
        /// </param>
        /// <param name="oldFollowerOldAcceleration">
        /// Current acceleration of the current agent behind this agent.
        /// </param>
        /// <param name="oldFollowerNewAcceleration">
        /// Acceleration of the agent currently behind this agent, if this agent had changed lane.
        /// </param>
        /// <param name="newFollowerOldAcceleration">
        /// Current acceleration of the agent, that would be behind this agent if this agent had changed lane.
        /// </param>
        /// <param name="newFollowerNewAcceleration">
        /// Acceleration of the agent, that would be behind this agent if this agent had changed lane, if this agent had
        /// changed lane.
        /// </param>
        /// <remarks>Acceleration values should be in meter per second squared.</remarks>
        public void DecideOnLaneChange(AdjacentLaneInfo adjacentLane, double newAcceleration,
            double oldFollowerOldAcceleration, double oldFollowerNewAcceleration, double newFollowerOldAcceleration,
            double newFollowerNewAcceleration)
        {
            if (_laneChangeProgress >= 1 && (_laneChangeModel?.IncentiveToChange(Acceleration, newAcceleration,
                    oldFollowerOldAcceleration, oldFollowerNewAcceleration, newFollowerOldAcceleration,
                    newFollowerNewAcceleration, adjacentLane.Bias) ?? false))
            {
                if (Route == null || Route.Contains(adjacentLane.Lane))
                {
                    NewLane = adjacentLane.Lane;
                }
            }
        }

        /// <summary>
        /// Starts lane change progress, effectively starting interpolating position in euclidean plane.
        /// </summary>
        /// <param name="oldLane">The lane this agent changed from.</param>
        public void ConfirmLaneChange(RegularRoadSegment oldLane)
        {
            _oldLane = oldLane;
            _laneChangeProgress = 0;

            NewLane = null;
        }

        /// <summary>
        /// Checks if this agent wants to follow given path.
        /// </summary>
        /// <param name="path">
        /// List of segments, where each pair of segments adjacent in list is a lane change decision checked.
        /// </param>
        /// <returns>
        /// True, if for all n &lt; path.Count - 1 it is true, that DecideOnSegmentChange(path[n]) == path[n+1],
        /// otherwise false.
        /// </returns>
        public bool FollowsPath(List<IRoadSegment> path)
        {
            if (path != null && path.Count > 1)
            {
                for (int i = 0; i < path.Count - 1; ++i)
                {
                    if (DecideOnSegmentChange(path[i]) != path[i + 1]) return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Draws this agent on the screen.
        /// </summary>
        /// <param name="window">The SFML window object that should be drawn to.</param>
        /// <param name="roadSegmentItsOn">The road segment this agent lies on.</param>
        /// <param name="direction">Current segment change direction (if applies).</param>
        /// <remarks>
        /// The vehicle's hue is determined by its speed, unless <see cref="trafficsim.Simulation.Vehicle.Agent.Color"/>
        /// is specified.
        /// </remarks>
        public void Draw(RenderWindow window, IRoadSegment roadSegmentItsOn, TurnDirection direction = TurnDirection.Forward)
        {
            UpdateXYPosition(roadSegmentItsOn);
            Vector frontToTail = _tailXyPosition.Value - _frontXyPosition;
            RectangleShape shape = new RectangleShape(new Vector2f((float) Length, (float) Width))
            {
                FillColor = Color ?? HSVColor.FromHSV(Velocity * 10.0, 0.8, 0.9),
                OutlineColor = SFML.Graphics.Color.Black,
                OutlineThickness = Selected ? 0.5f : 0,
                Position = new Vector2f((float) _frontXyPosition.X, (float) _frontXyPosition.Y),
                Rotation = (float) (Math.Atan2(frontToTail.Y, frontToTail.X) * 180 / Math.PI),
                Origin = new Vector2f(0.5f, (float) Width / 2f)
            };
            window.Draw(shape);

            if (direction != TurnDirection.Forward)
            {
                CircleShape turnSignal = new CircleShape(.5f)
                {
                    FillColor = new Color(255, 255, 0, (byte)(_turnSignalLit ? 255 : 127)),
                    Position = shape.Position,
                    Rotation = shape.Rotation,
                    Origin = new Vector2f(0, .5f + (float) (Width / 2 * (direction == TurnDirection.Left ? -1 : 1)))
                };
                window.Draw(turnSignal);
            }
        }

        /// <summary>
        /// Gets position of the center of the car in world space.
        /// </summary>
        public Vector GetXYPosition()
        {
            if (_tailXyPosition.HasValue)
            {
                return _frontXyPosition + (_tailXyPosition.Value - _frontXyPosition) / 2;
            }
            else
            {
                return _frontXyPosition;
            }
        }

        /// <summary>
        /// Updates current position of the vehicle on the euclidean plane.
        /// </summary>
        /// <param name="roadSegment">The road segment this agent lies on.</param>
        private void UpdateXYPosition(IRoadSegment roadSegment)
        {
            if (_oldLane != null && roadSegment is RegularRoadSegment newLane)
            {
                double oldLanePosition = Position * _oldLane.Length / newLane.Length;
                Vector oldLaneXY = _oldLane.LongitudinalPositionToXY(oldLanePosition);
                Vector newLaneXY = newLane.LongitudinalPositionToXY(Position);
                _frontXyPosition = oldLaneXY + (newLaneXY - oldLaneXY) * (Math.Cos(_laneChangeProgress * Math.PI) * -0.5 + 0.5);
            }
            else
            {
                _frontXyPosition = roadSegment.LongitudinalPositionToXY(Position);
            }

            if (_tailXyPosition.HasValue)
            {
                Vector frontToTail = _tailXyPosition.Value - _frontXyPosition;
                if (frontToTail.Length > 0)
                {
                    _tailXyPosition = _frontXyPosition + frontToTail / frontToTail.Length * (Length - 1);
                }
                else
                {
                    _tailXyPosition = roadSegment.LongitudinalPositionToXY(Position - Length + 1);
                }
            }
            else
            {
                _tailXyPosition = roadSegment.LongitudinalPositionToXY(Position - Length + 1);
            }
        }
    }
}