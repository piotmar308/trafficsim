﻿using trafficsim.Simulation.Vehicle;

namespace trafficsim.Simulation.Factory
{
    /// <summary>
    /// Interface for agent factories.
    /// </summary>
    public interface IVehicleFactory
    {
        /// <returns>Newly constructed agent object.</returns>
        Agent GetNewAgent();

        void ResetRNG();
    }
}