﻿using System;
using System.Collections.Generic;
using System.Linq;
using trafficsim.Simulation.Model;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Struct;

namespace trafficsim.Simulation.Intersection
{
    public class TrafficLightsIntersectionManager: IntersectionManager<TrafficLightsIntersectionSegment>
    {
        public List<TrafficLightsPhase> Phases { get; } = null;
        private int _currentPhaseIndex = 0;
        private double _timeSincePhaseStart;

        public double AmberLightTimeSeconds { get; set; } = 3;

        public TrafficLightsIntersectionManager(
            IEnumerable<(TrafficLightsIntersectionSegment prioritySegment, TrafficLightsIntersectionSegment yieldingSegment)> crossings,
            IEnumerable<TrafficLightsPhase> phases) : base(crossings)
        {
            if (!phases.Any())
            {
                throw new ArgumentException("TrafficLightsIntersectionManager must have at least one phase defined.");
            }
            Phases = new List<TrafficLightsPhase>(phases);
        }

        public override void ComputeIntersection(double timeDiff)
        {
            _timeSincePhaseStart += timeDiff;
            if (_timeSincePhaseStart >= GetCurrentPhase().PhaseLengthSeconds)
            {
                _timeSincePhaseStart -= GetCurrentPhase().PhaseLengthSeconds;
                ++_currentPhaseIndex;
                if (_currentPhaseIndex >= Phases.Count)
                {
                    _currentPhaseIndex = 0;
                }
            }

            foreach (TrafficLightsIntersectionSegment segment in _segments)
            {
                segment.RedLight = segment.AmberLight = false;
            }

            base.ComputeIntersection(timeDiff);

            foreach (TrafficLightsIntersectionSegment segment in _segments)
            {
                segment.RedLight = IsRedLight(segment);
                segment.AmberLight = IsAmberLight(segment);
            }
        }

        protected override void ComputeTimeFrames()
        {
            base.ComputeTimeFrames();

            foreach (TrafficLightsIntersectionSegment segment in _segments.Where(IsRedLight))
            {
                _allTimeFrames[segment] = new TimeFrameInfo[] {};
                ApproachingAgentsTimeFrames.Remove(segment);
            }
        }

        private bool IsRedLight(TrafficLightsIntersectionSegment segment)
        {
            return !GetCurrentPhase().OpenSegments.Contains(segment);
        }

        private bool IsAmberLight(TrafficLightsIntersectionSegment segment)
        {
            // Amber light can't be on if red light is currently on, or if for a given segment red light never turns on.
            if (IsRedLight(segment) || Phases.All(phase => phase.OpenSegments.Contains(segment)))
            {
                return false;
            }

            double timeToRedLight = GetCurrentPhase().PhaseLengthSeconds - _timeSincePhaseStart;
            int nextPhaseIndex = _currentPhaseIndex + 1 >= Phases.Count ? 0 : _currentPhaseIndex + 1;
            while (Phases[nextPhaseIndex].OpenSegments.Contains(segment))
            {
                timeToRedLight += Phases[nextPhaseIndex].PhaseLengthSeconds;
                nextPhaseIndex = nextPhaseIndex + 1 >= Phases.Count ? 0 : nextPhaseIndex + 1;
            }

            return timeToRedLight <= AmberLightTimeSeconds;
        }

        private TrafficLightsPhase GetCurrentPhase()
        {
            return Phases[_currentPhaseIndex];
        }
    }
}