﻿namespace trafficsim.Simulation.Intersection
{
    public interface IIntersectionManager
    {
        /// <summary>
        /// Time padding (in seconds) for agents' presence on the intersection. An agent will not enter an intersection
        /// unless no other agent crossed his path within the timespan of SafetyGap.
        /// </summary>
        double SafetyGap { get; set; }

        /// <summary>
        /// Opens or closes routes through the intersection.
        /// </summary>
        void ComputeIntersection(double timeDiff);
    }
}