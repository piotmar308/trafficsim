﻿using System.Collections.Generic;
using System.Linq;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Struct;

namespace trafficsim.Simulation.Intersection
{
    /// <summary>
    /// Class for managing an uncoordinated intersection with priorities defined for each pair of colliding routes
    /// through it.
    /// </summary>
    public class IntersectionManager<T>: IIntersectionManager where T: RightOfWayIntersectionSegment
    {
        /// <summary>
        /// Recurrence limiter for entry times gathering. Information is gathered from road segments within reach of
        /// given distance limit (in meters), including about agents outside this limit on checked segments.
        /// </summary>
        public static double TimeFrameComputationDistanceLimit = 100;
        /// <summary>
        /// Time padding (in seconds) for agents' presence on the intersection. An agent will not enter an intersection
        /// unless no other agent crossed his path within the timespan of SafetyGap.
        /// </summary>
        public double SafetyGap { get; set; } = 1;
        /// <summary>
        /// Flat collection of all road segments belonging to this intersection.
        /// </summary>
        protected readonly HashSet<T> _segments = new HashSet<T>();
        /// <summary>
        /// Collection of all road segments belonging to this intersection grouped by common entry points.
        /// </summary>
        protected readonly HashSet<HashSet<T>> _entries = new HashSet<HashSet<T>>();
        /// <summary>
        /// Complete priority definition. An agent can't enter the intersection unless in the time window in which he
        /// would occupy his route (plus SafetyGap on both sides of the window) no agent would be present on priority
        /// route (in relation to the route the primary agent is about to take).
        /// </summary>
        public HashSet<(T prioritySegment, T yieldingSegment)> Crossings { get; } = null;
        /// <summary>
        /// Computed segment occupation time frames.
        /// </summary>
        protected readonly Dictionary<T, IEnumerable<TimeFrameInfo>> _allTimeFrames =
            new Dictionary<T, IEnumerable<TimeFrameInfo>>();
        /// <summary>
        /// <see cref="_allTimeFrames"/> reduced to one time frame per one entry (see <see cref="_entries"/>), the
        /// closest one to the intersection.
        /// </summary>
        protected readonly Dictionary<T, TimeFrameInfo> ApproachingAgentsTimeFrames =
            new Dictionary<T, TimeFrameInfo>();

        /// <summary>
        /// Constructs the intersection object.
        /// </summary>
        /// <param name="crossings">
        /// Complete priority definition. An agent can't enter the intersection unless in the time window in which he
        /// would occupy his route (plus SafetyGap on both sides of the window) no agent would be present on priority
        /// route (in relation to the route the primary agent is about to take).
        /// </param>
        public IntersectionManager(
            IEnumerable<(T prioritySegment, T yieldingSegment)> crossings)
        {
            Crossings = new HashSet<(T, T)>(crossings);
            foreach ((T prioritySegment, T yieldingSegment) in crossings)
            {
                prioritySegment.CrossingSegments.Add(yieldingSegment);
                yieldingSegment.CrossingSegments.Add(prioritySegment);
                AddSegmentToEntry(prioritySegment);
                AddSegmentToEntry(yieldingSegment);
            }
        }

        /// <summary>
        /// Opens or closes routes through the intersection depending on <see cref="_allTimeFrames">computed time
        /// frames</see> and <see cref="Crossings">priority definition</see>.
        /// </summary>
        public virtual void ComputeIntersection(double timeDiff)
        {
            foreach (T segment in _segments)
            {
                segment.Closed = false;
            }

            ComputeTimeFrames();

            foreach (KeyValuePair<T, TimeFrameInfo>
                approachingAgentTimeFrame in ApproachingAgentsTimeFrames)
            {
                T entry = approachingAgentTimeFrame.Key;
                TimeFrameInfo timeFrame = approachingAgentTimeFrame.Value;

                if (entry.AgentsInCollisionCourseExist())
                {
                    entry.Closed = true;
                }
                else
                {
                    if (_allTimeFrames[entry].Any())
                    {
                        timeFrame.Enter -= SafetyGap;
                        timeFrame.Leave += SafetyGap;

                        foreach (T prioritySegment in GetPrioritySegments(entry))
                        {
                            // If approaching agent's time frame overlaps with any of the time frames on priority
                            // segments (ignoring time frames of vehicles standing still, except those nearest the
                            // intersection)
                            if (_allTimeFrames[prioritySegment]
                                .Where(tf => ApproachingAgentsTimeFrames.ContainsValue(tf) || !tf.Still)
                                .Any(tf => tf.Enter < timeFrame.Leave && tf.Leave > timeFrame.Enter))
                            {
                                entry.Closed = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        /// <returns>IEnumerable of priority routes in relation to specified route.</returns>
        protected IEnumerable<T> GetPrioritySegments(T inRelationTo)
        {
            return Crossings.Where(crossing => crossing.yieldingSegment == inRelationTo)
                .Select(crossing => crossing.prioritySegment);
        }

        /// <summary>
        /// Populates <see cref="_allTimeFrames"/> and <see cref="ApproachingAgentsTimeFrames"/>.
        /// </summary>
        protected virtual void ComputeTimeFrames()
        {
            _allTimeFrames.Clear();
            ApproachingAgentsTimeFrames.Clear();

            foreach (HashSet<T> group in _entries)
            {
                TimeFrameInfo? approachingAgentTimeFrame = null;
                T approachingAgentEntry = null;

                foreach (T entry in group)
                {
                    _allTimeFrames[entry] = entry.GetEntryTimes(TimeFrameComputationDistanceLimit)
                        .OrderBy(t => t.time)
                        .Select(t => new TimeFrameInfo
                        {
                            Enter = t.time,
                            Leave = t.time + entry.Length / entry.SpeedLimit,
                            Still = t.still
                        });

                    if (_allTimeFrames[entry].Any() && (!approachingAgentTimeFrame.HasValue ||
                                                        approachingAgentTimeFrame.Value.Enter >
                                                        _allTimeFrames[entry].First().Enter))
                    {
                        approachingAgentTimeFrame = _allTimeFrames[entry].First();
                        approachingAgentEntry = entry;
                    }
                }

                if (approachingAgentTimeFrame.HasValue)
                {
                    ApproachingAgentsTimeFrames[approachingAgentEntry] = approachingAgentTimeFrame.Value;
                }
            }
        }

        /// <summary>
        /// Used to populate <see cref="_entries"/> and <see cref="_segments"/>.
        /// </summary>
        /// <param name="segment">
        /// Route through the intersection. May or may not already be in these collections.
        /// </param>
        private void AddSegmentToEntry(T segment)
        {
            HashSet<T> entry = _entries.FirstOrDefault(e => e.Any(s => s.HasSamePredecessors(segment)));

            if (entry != null)
            {
                entry.Add(segment);
            }
            else
            {
                _entries.Add(new HashSet<T> {segment});
            }

            _segments.Add(segment);
        }
    }
}