﻿using System;
using System.Collections.Generic;
using System.Linq;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Struct;

namespace trafficsim.Simulation.Intersection
{
    /// <summary>
    /// Class for managing an uncoordinated intersection where right hand rule is present.
    /// </summary>
    public class RightHandRuleIntersectionManager: IntersectionManager<RightHandRuleIntersectionSegment>
    {
        /// <summary>
        /// Definition of intersection sides. A collection of routes through the intersection, grouped by the side from
        /// which they enter it, ordered clockwise.
        /// </summary>
        public List<HashSet<RightHandRuleIntersectionSegment>> SidesClockwise { get; } = null;
        /// <summary>
        /// RNG object.
        /// </summary>
        private readonly Random _random = new Random(0);

        /// <inheritdoc/>
        /// <param name="sidesClockwise">
        /// Collection of routes through the intersection, grouped by the side from which they enter it, ordered
        /// clockwise.
        /// </param>
        public RightHandRuleIntersectionManager(
            IEnumerable<(RightHandRuleIntersectionSegment prioritySegment, RightHandRuleIntersectionSegment yieldingSegment)> crossings,
            IEnumerable<IEnumerable<RightHandRuleIntersectionSegment>> sidesClockwise) : base(crossings)
        {
            SidesClockwise =
                new List<HashSet<RightHandRuleIntersectionSegment>>(sidesClockwise.Select(side =>
                    new HashSet<RightHandRuleIntersectionSegment>(side)));
        }

        /// <summary>
        /// Opens or closes routes through the intersection depending on
        /// <see cref="IntersectionManager._allTimeFrames">computed time frames</see>,
        /// <see cref="IntersectionManager{T}.Crossings">priority definition</see>, and
        /// <see cref="SidesClockwise">sides definition</see>.
        /// </summary>
        public override void ComputeIntersection(double timeDiff)
        {
            base.ComputeIntersection(timeDiff);

            // Hard dead lock detection
            IEnumerable<RightHandRuleIntersectionSegment> hardLockedApproachingAgentsSegments = ApproachingAgentsTimeFrames
                .Where(tfd => tfd.Key.Closed && !tfd.Key.LetOneAgentThrough && tfd.Value.Still).Select(tfd => tfd.Key);

            if (SidesClockwise.All(side => side.Intersect(hardLockedApproachingAgentsSegments).Any()))
            {
                IEnumerable<RightHandRuleIntersectionSegment> hardLockedClearSegments =
                    hardLockedApproachingAgentsSegments.Where(s => !s.AgentsInCollisionCourseExist()).ToArray();

                if (hardLockedClearSegments.Any())
                {
                    hardLockedClearSegments.ElementAt(_random.Next(hardLockedClearSegments.Count()))
                        .LetOneAgentThrough = true;
                }
            }

            // Soft dead lock detection (if hard lock is not in resolution)
            if (!ApproachingAgentsTimeFrames.Any(s => s.Key.IsHardDeadLockInResolution()))
            {
                IEnumerable<RightHandRuleIntersectionSegment> softLockedApproachingAgentsSegments = ApproachingAgentsTimeFrames
                    .Where(tfd => ApproachingAgentsTimeFrames.Any(otherTfd =>
                        otherTfd.Value.Enter < tfd.Value.Leave + SafetyGap &&
                        otherTfd.Value.Leave > tfd.Value.Enter - SafetyGap))
                    .Select(tfd => tfd.Key).ToArray();
                int softLockedSidesCount =
                    SidesClockwise.Count(side => side.Intersect(softLockedApproachingAgentsSegments).Any());

                if (softLockedApproachingAgentsSegments.All(s => s.Closed) && softLockedSidesCount > 2 &&
                    softLockedSidesCount < SidesClockwise.Count())
                {
                    int lastNotLockedSideIndex =
                        SidesClockwise.FindLastIndex(s => !s.Intersect(softLockedApproachingAgentsSegments).Any());
                    int firstLockedSideIndex = lastNotLockedSideIndex + 1;
                    if (firstLockedSideIndex == SidesClockwise.Count()) firstLockedSideIndex = 0;
                    foreach (RightHandRuleIntersectionSegment segment in SidesClockwise[firstLockedSideIndex]
                        .Where(s => !s.AgentsInCollisionCourseExist()))
                    {
                        segment.Closed = false;
                    }
                }
            }
        }
    }
}