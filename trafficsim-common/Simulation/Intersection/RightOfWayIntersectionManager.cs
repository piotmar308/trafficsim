﻿using System.Collections.Generic;
using trafficsim.Simulation.RoadSegment;

namespace trafficsim.Simulation.Intersection
{
    public class RightOfWayIntersectionManager: IntersectionManager<RightOfWayIntersectionSegment>
    {
        public RightOfWayIntersectionManager(IEnumerable<(RightOfWayIntersectionSegment prioritySegment, RightOfWayIntersectionSegment yieldingSegment)> crossings) : base(crossings)
        {
        }
    }
}