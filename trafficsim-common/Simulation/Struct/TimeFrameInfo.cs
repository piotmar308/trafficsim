﻿namespace trafficsim.Simulation.Struct
{
    /// <summary>
    /// Time frame data.
    /// </summary>
    public struct TimeFrameInfo
    {
        /// <summary>
        /// Relative time of entry, in seconds.
        /// </summary>
        public double Enter;
        /// <summary>
        /// Relative time of leaving, in seconds.
        /// </summary>
        public double Leave;
        /// <summary>
        /// Whether the agent stands still (or moves slow enough to be considered still).
        /// </summary>
        public bool Still;
    }
}