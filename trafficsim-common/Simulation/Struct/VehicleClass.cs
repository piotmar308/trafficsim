﻿using System.Collections.Generic;
using trafficsim.Simulation.Model;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Simulation.Struct
{
    /// <summary>
    /// Class of agent with IDM and MOBIL model.
    /// </summary>
    public struct VehicleClass
    {
        /// <summary>
        /// Relative possibility of spawning an agent of this class.
        /// </summary>
        public double Probability;

        /// <summary>
        /// Range of possible values for <see cref="Agent.Width"/>.
        /// </summary>
        public (double min, double max) Width;
        /// <summary>
        /// Range of possible values for <see cref="Agent.Length"/>.
        /// </summary>
        public (double min, double max) Length;
        /// <summary>
        /// Range of possible values for <see cref="Agent.SafeDecelerationThreshold"/>.
        /// </summary>
        public (double min, double max) SafeDecelerationThreshold;
        /// <summary>
        /// Range of possible values for <see cref="Agent.LaneChangeTime"/>.
        /// </summary>
        public (double min, double max) LaneChangeTime;
        /// <summary>
        /// Range of possible values for initial <see cref="Agent.RegardForSpeedLimit"/>.
        /// </summary>
        public (double min, double max) RegardForSpeedLimit;

        /// <summary>
        /// Range of possible values for <see cref="IntelligentDriverModel.SafeTimeHeadway"/>.
        /// </summary>
        public (double min, double max) SafeTimeHeadway;
        /// <summary>
        /// Range of possible values for <see cref="IntelligentDriverModel.MaximumAcceleration"/>.
        /// </summary>
        public (double min, double max) MaximumAcceleration;
        /// <summary>
        /// Range of possible values for <see cref="IntelligentDriverModel.ComfortableDeceleration"/>.
        /// </summary>
        public (double min, double max) ComfortableDeceleration;
        /// <summary>
        /// Range of possible values for <see cref="IntelligentDriverModel.AccelerationExponent"/>.
        /// </summary>
        public (double min, double max) AccelerationExponent;
        /// <summary>
        /// Range of possible values for <see cref="IntelligentDriverModel.MinimumDistance"/>.
        /// </summary>
        public (double min, double max) MinimumDistance;

        /// <summary>
        /// Range of possible values for <see cref="MOBILModel.Politeness"/>.
        /// </summary>
        public (double min, double max) Politeness;
        /// <summary>
        /// Range of possible values for <see cref="MOBILModel.LaneChangeThreshold"/>.
        /// </summary>
        public (double min, double max) LaneChangeThreshold;

        public static List<VehicleClass> GetDefaultVehicleClasses()
        {
            return new List<VehicleClass>
            {
                new VehicleClass
                {
                    Width = (1.7, 2),
                    Length = (4, 5),
                    SafeDecelerationThreshold = (6, 7),
                    LaneChangeTime = (2.5, 3.5),
                    SafeTimeHeadway = (0.5, 1.5),
                    MaximumAcceleration = (2.5, 3.5),
                    ComfortableDeceleration = (3, 4),
                    AccelerationExponent = (4, 4),
                    MinimumDistance = (1.8, 2.2),
                    Politeness = (0.05, 0.2),
                    LaneChangeThreshold = (0.15, 0.2),
                    RegardForSpeedLimit = (0.9, 1.2),
                    Probability = 9
                },
                new VehicleClass
                {
                    Width = (2.3, 2.6),
                    Length = (7, 9),
                    SafeDecelerationThreshold = (5, 6),
                    LaneChangeTime = (3, 4),
                    SafeTimeHeadway = (1, 2),
                    MaximumAcceleration = (1.5, 2.5),
                    ComfortableDeceleration = (2, 3),
                    AccelerationExponent = (4, 4),
                    MinimumDistance = (2, 2.5),
                    Politeness = (0.05, 0.1),
                    LaneChangeThreshold = (0.15, 0.25),
                    RegardForSpeedLimit = (0.8, 1.1),
                    Probability = 1
                }
            };
        }
    }
}