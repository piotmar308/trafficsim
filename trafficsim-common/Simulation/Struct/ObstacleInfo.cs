﻿namespace trafficsim.Simulation.Struct
{
    /// <summary>
    /// Information about an obstacle.
    /// </summary>
    public struct ObstacleInfo
    {
        public ObstacleInfo (ObstacleSeekInfo si)
        {
            DistanceToObstacle = si.Distance;
            ObstacleForwardVelocity = si.Agent?.Velocity ?? 0;
        }

        /// <summary>
        /// Distance to the obstacle, in meters.
        /// </summary>
        public double DistanceToObstacle;
        /// <summary>
        /// Absolute velocity, with which the obstacle moves, in meters per second. 0 of it's a static obstacle, like
        /// end of the road. Velocity of the vehicle in the way, if it's an agent.
        /// </summary>
        public double ObstacleForwardVelocity;
    }
}