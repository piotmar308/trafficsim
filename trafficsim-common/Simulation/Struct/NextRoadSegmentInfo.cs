﻿using trafficsim.Simulation.RoadSegment;

namespace trafficsim.Simulation.Struct
{
    public enum TurnDirection
    {
        Forward,
        Left,
        Right
    }

    /// <summary>
    /// Information about a forward connection to the next road segment.
    /// </summary>
    public struct NextRoadSegmentInfo
    {
        /// <summary>
        /// The next road segment.
        /// </summary>
        public IRoadSegment Segment;
        /// <summary>
        /// Relative probability to take that forward connection in case of a road split.
        /// </summary>
        public double Probability;
        /// <summary>
        /// The direction of that forward connection. Should be <see cref="TurnDirection.Forward"/>, unless the
        /// connection is made to a path through intersection that turns the agent left or right.
        /// </summary>
        public TurnDirection Direction;
    }
}