﻿using System.Collections.Generic;
using trafficsim.Simulation.RoadSegment;

namespace trafficsim.Simulation.Model
{
    public struct TrafficLightsPhase
    {
        public double PhaseLengthSeconds;
        public IEnumerable<IRoadSegment> OpenSegments;
    }
}