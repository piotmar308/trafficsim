﻿using trafficsim.Simulation.RoadSegment;

namespace trafficsim.Simulation.Struct
{
    /// <summary>
    /// Information about connection to an adjacent lane.
    /// </summary>
    public struct AdjacentLaneInfo
    {
        /// <summary>
        /// Road segment, which is the adjacent lane.
        /// </summary>
        public RegularRoadSegment Lane;
        /// <summary>
        /// Bias for changing to that lane, in meters per second squared.
        /// </summary>
        public double Bias;
    }
}