﻿using trafficsim.Simulation.Vehicle;

namespace trafficsim.Simulation.Struct
{
    /// <summary>
    /// Information about an obstacle.
    /// </summary>
    public struct ObstacleSeekInfo
    {
        /// <summary>
        /// Agent, that is the obstacle. Null, if it's a non-agent obstacle, like end of road.
        /// </summary>
        public Agent Agent;
        /// <summary>
        /// Distance to the obstacle, in meters.
        /// </summary>
        public double Distance;
    }
}