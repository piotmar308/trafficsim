﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using trafficsim.Simulation.Struct;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Simulation.RoadSegment
{
    /// <summary>
    /// Class for modelling paths through intersections.
    /// </summary>
    public class RightOfWayIntersectionSegment: RegularRoadSegment
    {
        /// <summary>
        /// Whether this road segment should be considered closed off.
        /// </summary>
        public bool Closed { get; set; } = false;
        /// <summary>
        /// Set of segments, for which agents can potentially be on collision path with agents on this segment.
        /// </summary>
        public readonly HashSet<RightOfWayIntersectionSegment> CrossingSegments = new HashSet<RightOfWayIntersectionSegment>();

        /// <inheritdoc/>
        public RightOfWayIntersectionSegment(IEnumerable<Vector> vertices) : base(vertices)
        {
        }

        /// <summary>
        /// Method behaves similarly to <see cref="RegularRoadSegment.SeekObstacleForwards"/>, unless the segment is
        /// closed (see remarks of <see cref="ShouldSegmentDisallowEntry"/> for when segment is considered to be closed)
        /// and the rear bumper of the first vehicle is past the entry of the segment (or there are no agents on this
        /// segment), then it behaves as if it was not there.
        /// </summary>
        /// <inheritdoc/>
        public override ObstacleSeekInfo SeekObstacleForwards(double position, Agent agentWhoWantsIt, double addDistance = 0, int limit = 99)
        {
            ObstacleSeekInfo baseFirstObstacle = base.SeekObstacleForwards(position, agentWhoWantsIt, addDistance, limit - 1);
            ObstacleSeekInfo closedFirstObstacle = new ObstacleSeekInfo
            {
                Distance = baseFirstObstacle.Distance >= addDistance ? addDistance : baseFirstObstacle.Distance,
                Agent = baseFirstObstacle.Distance >= addDistance ? null : baseFirstObstacle.Agent
            };

            return agentWhoWantsIt != null && !Agents.Contains(agentWhoWantsIt) &&
                   ShouldSegmentDisallowEntry(agentWhoWantsIt) ? closedFirstObstacle : baseFirstObstacle;
        }

        /// <returns>True, if any segment this segment crosses has any agent.</returns>
        public bool AgentsInCollisionCourseExist()
        {
            return CrossingSegments.Any(segment => segment.Agents.Any());
        }

        /// <summary>
        /// When checked for for first obstacle, whether to behave as if this road segment existed, which is the case
        /// when Closed = true. Child classes override this method to change the definition of what it means for the
        /// segment to be closed.
        /// </summary>
        protected virtual bool ShouldSegmentDisallowEntry(Agent agent)
        {
            return Closed;
        }
    }
}