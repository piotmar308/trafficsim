﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using SFML.Graphics;
using trafficsim.Helper;
using trafficsim.Simulation.Factory;
using trafficsim.Simulation.Struct;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Simulation.RoadSegment
{
    /// <summary>
    /// Class for adding new agents to the simulation.
    /// </summary>
    public class SpawnerSegment: IRoadSegment
    {
        public string Id { get; set; } = null;
        /// <summary>
        /// Mean spawning rate. Agents are spawned at random with exponential distribution.
        /// </summary>
        public double SpawnFrequency
        {
            set
            {
                _spawnFrequency = value > 0 ? value : double.Epsilon;
                _spawnTimer = - Math.Log(1 - _random.NextDouble()) / value;
            }
            get => _spawnFrequency;
        }
        /// <summary>
        /// Collection of road segments, which a spawning agent can take.
        /// </summary>
        public ICollection<IRoadSegment> Route { get; set; } = null;
        /// <summary>
        /// Number of agents who finished their route
        /// </summary>
        public Ref<int> RouteThroughput { get; set; } = new Ref<int>(0);
        /// <summary>
        /// Reference to agents pool size. When an agent gets spawned by this segment, the pool shrinks by one agent.
        /// The spawner will not spawn any new agents if the pool is empty. When all spawners and despawners have
        /// reference to the same pool size, a steady number of agents in the simulation is maintained.
        /// </summary>
        public Ref<int> AgentLimit { get; set; } = null;

        public Ref<int> TotalAgentsSpawned { get; set; } = null;
        /// <summary>
        /// Collection of forward road segment connections.
        /// </summary>
        public HashSet<NextRoadSegmentInfo> Next { get; } = new HashSet<NextRoadSegmentInfo>();
        /// <summary>
        /// The factory object used for constructing new agents.
        /// </summary>
        public IVehicleFactory Factory { get; } = null;
        /// <summary>
        /// Amount of time left before next spawn.
        /// </summary>
        private double _spawnTimer;
        /// <summary>
        /// Newly created agent object, about to be placed on a next segment.
        /// </summary>
        private Agent _newAgent = null;
        /// <summary>
        /// Used to predictably pseudo-randomize next SpawnerSegment objects.
        /// </summary>
        public static int RandomSeed = 0;
        /// <summary>
        /// RNG object.
        /// </summary>
        private readonly Random _random = new Random(RandomSeed++);
        /// <summary>
        /// Backing field for SpawnFrequency.
        /// </summary>
        private double _spawnFrequency = 0.1;

        /// <summary>
        /// Creates the spawner segment object.
        /// </summary>
        /// <param name="factory">Factory object, responsible for constructing new agent objects.</param>
        public SpawnerSegment(IVehicleFactory factory)
        {
            Factory = factory;
        }

        /// <summary>
        /// Adds connection to a next segment. Spawner will spawn a new agent on one of the next segments.
        /// </summary>
        /// <inheritdoc/>
        public void AddNext(IRoadSegment next, double probability = 1, TurnDirection direction = TurnDirection.Forward)
        {
            if (Next.All(turn => turn.Segment != next))
            {
                Next.Add(new NextRoadSegmentInfo { Segment = next, Probability = probability, Direction = direction});
                next.AddPrevious(this);
            }
        }

        /// <summary>
        /// Does nothing. No segment should have a forward connection to a spawner.
        /// </summary>
        public void AddPrevious(IRoadSegment previous)
        {
        }

        /// <inheritdoc/>
        public IRoadSegment GetRandomNext(ICollection<IRoadSegment> intersectNextSegments = null)
        {
            HashSet<NextRoadSegmentInfo> nextToChooseFrom = new HashSet<NextRoadSegmentInfo>(Next.Where(s =>
                intersectNextSegments == null || intersectNextSegments.Contains(s.Segment)));
            double sumOfProbabilities = nextToChooseFrom.Select(s => s.Probability).Sum();
            double n = _random.NextDouble() * sumOfProbabilities;
            foreach (NextRoadSegmentInfo next in nextToChooseFrom)
            {
                n -= next.Probability;
                if (n <= 0) return next.Segment;
            }

            return nextToChooseFrom.Last().Segment;
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        public void SimulateAccelerationChanges()
        {
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        public void SimulateLaneChanges()
        {
        }

        /// <summary>
        /// Updates the spawn timer.
        /// </summary>
        public void UpdateAgentsPositionsAndVelocities(double timeDiff)
        {
            _spawnTimer -= timeDiff;
        }

        /// <summary>
        /// Decides whether or not spawn a new agent.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Once the spawn timer counts down to 0 the method constructs a new agent, asks it which one of the next
        /// segments does it want to spawn on (as a decision on a road split) and decides whether to spawn it there with
        /// initial velocity defined by agent class. If the AgentLimit pool is empty, or there is no space to spawn the
        /// agent, or if the agent right after spawning would have had to brake harder, than his
        /// SafeDecelerationThreshold allows, decision fails and retries on the next execution of this method (next
        /// tick). When retrying, the method still tries to spawn the same agent, but asks him again for a choice of
        /// spawning destination.
        /// </para>
        /// <para>
        /// After a success the timer resets to a new random value from SpawnPeriod range, the AgentLimit pool shrinks
        /// by one agent and the agent spawns.
        /// </para>
        /// </remarks>
        public void MoveAgentsToRespectiveSegments()
        {
            if (_spawnTimer < 0)
            {
                if (_newAgent == null)
                {
                    _newAgent = Factory.GetNewAgent();
                    _newAgent.RouteThroughput = RouteThroughput;
                    if (Route != null)
                    {
                        _newAgent.Route = Route;
                    }
                }
                else
                {
                    _newAgent.ForgetDecisionOnSegmentChange(this);
                }
                RegularRoadSegment nextSegment = _newAgent.DecideOnSegmentChange(this) as RegularRoadSegment;
                if (nextSegment == null)
                {
                    throw new NullReferenceException("Route contains no valid starting segments.");
                }
                _newAgent.Velocity = nextSegment?.SpeedLimit ?? 0;
                ObstacleSeekInfo obstacle = SeekObstacleForwards(0, _newAgent);
                double acc = _newAgent.GetAccelerationForObstacle(new ObstacleInfo(obstacle), nextSegment?.SpeedLimit ?? 0);
                if((AgentLimit?.Value ?? 1) > 0 && obstacle.Distance > 0 && acc > -_newAgent.SafeDecelerationThreshold)
                {
                    _spawnTimer = - Math.Log(1 - _random.NextDouble()) / SpawnFrequency;
                    if (AgentLimit != null) --AgentLimit.Value;
                    if (TotalAgentsSpawned != null) ++TotalAgentsSpawned.Value;
                    nextSegment.InsertAgent(_newAgent);
                    _newAgent = null;
                }
            }
        }

        /// <inheritdoc/>
        public ObstacleSeekInfo SeekObstacleForwards(double position, Agent agentWhoWantsIt, double addDistance = 0, int limit = 99)
        {
            if (limit <= 0)
            {
                return new ObstacleSeekInfo {Distance = 0, Agent = null};
            }

            IRoadSegment nextSegment = agentWhoWantsIt?.DecideOnSegmentChange(this);
            if (nextSegment != null)
            {
                return nextSegment.SeekObstacleForwards(0, agentWhoWantsIt, addDistance, limit - 1);
            }
            else
            {
                return new ObstacleSeekInfo {
                    Distance = addDistance,
                    Agent = null
                };
            }
        }

        /// <returns>Information about a static obstacle at distance 0.</returns>
        public ObstacleSeekInfo SeekObstacleBackwards(double position, double addDistance = 0, int limit = 99)
        {
            return new ObstacleSeekInfo
            {
                Agent = null,
                Distance = 0
            };
        }

        /// <returns>Information about a static obstacle at distance 0.</returns>
        public ObstacleSeekInfo SeekObstacleFromEnd(double addDistance = 0, int limit = 99)
        {
            return new ObstacleSeekInfo
            {
                Agent = null,
                Distance = 0
            };
        }

        /// <summary>
        /// Dpawner does not have a graphical representation, therefore this method does nothing.
        /// </summary>
        public void DrawRoad(RenderWindow window)
        {
        }

        /// <summary>
        /// Spawner does not draw agents, therefore this method does nothing.
        /// </summary>
        public void DrawAgents(RenderWindow window)
        {
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        public void InsertAgent(Agent agent)
        {
        }

        /// <returns>Vector (0, 0).</returns>
        public Vector LongitudinalPositionToXY(double position)
        {
            return new Vector(0, 0);
        }

        public double GetLength() { return 0; }

        /// <summary>
        /// Checks if the route has dead ends by the method of flood filling.
        /// </summary>
        public bool RouteHasDeadEnds()
        {
            if (Route == null)
            {
                return false;
            }

            Queue<IRoadSegment> nextToCheck = new Queue<IRoadSegment>();
            HashSet<IRoadSegment> segmentsChecked = new HashSet<IRoadSegment>();
            foreach (NextRoadSegmentInfo nextSegmentInfo in Next.Where(nrsi => nrsi.Probability > 0))
            {
                nextToCheck.Enqueue(nextSegmentInfo.Segment);
            }

            while (nextToCheck.Count > 0)
            {
                IRoadSegment s = nextToCheck.Dequeue();

                if (s is RegularRoadSegment segment)
                {
                    if (!segmentsChecked.Contains(segment))
                    {
                        segmentsChecked.Add(segment);

                        if (!segment.Next.Any(i => i.Probability > 0) && !segment.AdjacentLanes.Any())
                        {
                            return true;
                        }

                        foreach (NextRoadSegmentInfo nrsi in segment.Next.Where(i => i.Probability > 0))
                        {
                            nextToCheck.Enqueue(nrsi.Segment);
                        }

                        foreach (AdjacentLaneInfo adjacent in segment.AdjacentLanes)
                        {
                            nextToCheck.Enqueue(adjacent.Lane);
                        }
                    }
                }
            }

            return false;
        }
    }
}