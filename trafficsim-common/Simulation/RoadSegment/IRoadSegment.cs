﻿using System.Collections.Generic;
using System.Windows;
using SFML.Graphics;
using trafficsim.Simulation.Struct;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Simulation.RoadSegment
{
    /// <summary>
    /// Interface of classes for containing and managing agents.
    /// </summary>
    public interface IRoadSegment
    {
        /// <summary>
        /// Adds connection to a next segment.
        /// </summary>
        /// <param name="next">
        /// The road segment that this one should connect to.
        /// </param>
        /// <param name="probability">
        /// Number defining probability of taking this connection above other ones from this segment. If one
        /// connection's "probability" is equal to 2, and the sum of all "probabilities" of connections from this
        /// segment is equal to 5, then agents will take this connection with 40% likelihood.
        /// </param>
        /// <param name="direction">
        /// Turn direction of this connection. Affects only visualization. If an agent currently on this segment is
        /// about to take connection with direction other than Forward, then it will blink its turn signals.
        /// </param>
        void AddNext(IRoadSegment next, double probability = 1, TurnDirection direction = TurnDirection.Forward);
        /// <summary>
        /// Adds connection to a previous segment. Recommended to only use inside AddNext method definition. This method
        /// exists to help maintain bidirectionality of forward-backward segment connections.
        /// </summary>
        /// <param name="previous">The road segment that this one should connect to.</param>
        void AddPrevious(IRoadSegment previous);
        /// <returns>Random next road segment. Respects "probability" property of connections.</returns>
        /// <param name="intersectNextSegments">
        /// If present, next segments collection will intersect this collection.
        /// </param>
        IRoadSegment GetRandomNext(ICollection<IRoadSegment> intersectNextSegments = null);
        /// <summary>
        /// Update this segment's agents' acceleration. This method can't mutate any other segments.
        /// </summary>
        void SimulateAccelerationChanges();
        /// <summary>
        /// Move the agents to adjacent lanes if their behavioral models want so. This method may mutate other segments.
        /// </summary>
        void SimulateLaneChanges();
        /// <summary>
        /// Update this segment's agents' positions and velocities. This method can't mutate any other IRoadSegment.
        /// </summary>
        /// <param name="timeDiff">Amount of time simulated.</param>
        void UpdateAgentsPositionsAndVelocities(double timeDiff);
        /// <summary>
        /// Move this segment's agents, who require it, to other road segments. This method can mutate other road
        /// segments and should mutate agents to make it highly unlikely for them to require moving again in this
        /// simulation tick.
        /// </summary>
        void MoveAgentsToRespectiveSegments();
        /// <param name="position">
        /// Position, from which to seek.
        /// </param>
        /// <param name="addDistance">
        /// How many meters to add to returned struct's distance.
        /// </param>
        /// <param name="agentWhoWantsIt">
        /// Agent, for whom knowledge about next obstacle is gathered. If this argument is provided, on road splits
        /// obstacle seeking follows vehicle's future path. If this argument is null, road splits are treated as an
        /// obstacle.
        /// </param>
        /// <param name="limit">
        /// Recurrence limiter.
        /// </param>
        /// <returns>
        /// If agentWhoWantsIt == null, then information about first obstacle looking forwards from specified position
        /// on this segment. If agentWhoWantsIt is provided, information about the obstacle posing greatest threat to
        /// given agent.
        /// </returns>
        ObstacleSeekInfo SeekObstacleForwards(double position, Agent agentWhoWantsIt, double addDistance = 0, int limit = 99);
        /// <param name="position">
        /// Position, from which to seek.
        /// </param>
        /// <param name="addDistance">
        /// How many meters to add to returned struct's distance.
        /// </param>
        /// <param name="limit">
        /// Recurrence limiter.
        /// </param>
        /// <returns>
        /// Information about first obstacle looking backwards from specified position on this segment.
        /// </returns>
        ObstacleSeekInfo SeekObstacleBackwards(double position, double addDistance = 0, int limit = 99);
        /// <param name="addDistance">
        /// How many meters to add to returned struct's distance.
        /// </param>
        /// <param name="limit">
        /// Recurrence limiter.
        /// </param>
        /// <returns>
        /// Information about first obstacle looking backwards from the end of this segment.
        /// </returns>
        ObstacleSeekInfo SeekObstacleFromEnd(double addDistance = 0, int limit = 99);
        /// <summary>
        /// Draws this road segment.
        /// </summary>
        /// <param name="window">SFML window object to draw to.</param>
        void DrawRoad(RenderWindow window);
        /// <summary>
        /// Draws vehicles on this road segment.
        /// </summary>
        /// <param name="window">SFML window object to draw to.</param>
        void DrawAgents(RenderWindow window);
        /// <summary>
        /// Inserts agent to this road segment.
        /// </summary>
        /// <param name="agent">Agent to insert.</param>
        void InsertAgent(Agent agent);
        /// <summary>
        /// For a given longitudinal position determines euclidean plane coordinates.
        /// </summary>
        /// <param name="position">Longitudinal position in meters.</param>
        /// <returns>Euclidean plane coordinates this position corresponds to.</returns>
        /// <remarks>
        /// Negative positions and positions greater than total length of the segment are legal. Implementations may
        /// interpolate to determine them.
        /// </remarks>
        Vector LongitudinalPositionToXY(double position);
        /// <returns>Total length of this road segment</returns>
        double GetLength();
    }
}