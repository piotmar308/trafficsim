﻿namespace trafficsim.Simulation.RoadSegment
{
    public enum IntersectionRoadSegmentType
    {
        RightOfWayIntersectionSegment,
        RightHandRuleIntersectionSegment,
        TrafficLightsIntersectionSegment
    }
}