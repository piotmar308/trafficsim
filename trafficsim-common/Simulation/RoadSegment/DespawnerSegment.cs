﻿using System.Collections.Generic;
using System.Windows;
using SFML.Graphics;
using trafficsim.Helper;
using trafficsim.Simulation.Struct;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Simulation.RoadSegment
{
    /// <summary>
    /// Class for removing agents who drove to this segment from the simulation.
    /// </summary>
    public class DespawnerSegment: IRoadSegment
    {
        /// <summary>
        /// Reference to agents pool size. When an agent gets removed from the simulation by this segment, the pool
        /// grows larger by one agent. When all spawners and despawners have reference to the same pool size, a steady
        /// number of agents in the simulation is maintained.
        /// </summary>
        public Ref<int> AgentLimit { get; set; } = null;

        /// <summary>
        /// Amount of agents destroyed.
        /// </summary>
        public Ref<int> Throughput { get; set; } = null;

        public Ref<double> AgentLifetimeSum { get; set; } = null;

        /// <summary>
        /// Despawner can't have a forward connection.
        /// </summary>
        public void AddNext(IRoadSegment next, double probability = 1, TurnDirection direction = TurnDirection.Forward)
        {
        }

        /// <summary>
        /// Attempting to add backwards connection does nothing. Despawner does not need to maintain bidirectionality
        /// of connections.
        /// </summary>
        public void AddPrevious(IRoadSegment previous)
        {
        }

        /// <summary>
        /// Despawner does not need have forward connection, therefore this method always returns null.
        /// </summary>
        public IRoadSegment GetRandomNext(ICollection<IRoadSegment> intersectNextSegments = null)
        {
            return null;
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        public void SimulateAccelerationChanges()
        {
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        public void SimulateLaneChanges()
        {
        }

        /// <summary>
        /// Despawner does not hold agents, therefore this method does nothing.
        /// </summary>
        public void UpdateAgentsPositionsAndVelocities(double timeDiff)
        {
        }

        /// <summary>
        /// Despawner does not hold agents, therefore this method does nothing.
        /// </summary>
        public void MoveAgentsToRespectiveSegments()
        {
        }

        /// <returns>A static obstacle 1 km away.</returns>
        /// <remarks>
        /// Despawner attempts to behave like a black hole, but this method needs to return some obstacle.
        /// </remarks>
        public ObstacleInfo GetFirstObstacle(Agent agentWhoWantsIt, int limit = 99)
        {
            return new ObstacleInfo
            {
                DistanceToObstacle = 1000,
                ObstacleForwardVelocity = 0
            };
        }

        /// <returns>A static obstacle 1 km away.</returns>
        /// <remarks>
        /// Despawner attempts to behave like a black hole, but this method needs to return some obstacle.
        /// </remarks>
        public ObstacleSeekInfo SeekObstacleForwards(double position, Agent agentWhoWantsIt, double addDistance = 0, int limit = 99)
        {
            return new ObstacleSeekInfo
            {
                Agent = null,
                Distance = 1000
            };
        }

        /// <returns>A static obstacle 1 km away.</returns>
        /// <remarks>
        /// Despawner attempts to behave like a black hole, but this method needs to return some obstacle.
        /// </remarks>
        public ObstacleSeekInfo SeekObstacleBackwards(double position, double addDistance = 0, int limit = 99)
        {
            return new ObstacleSeekInfo
            {
                Agent = null,
                Distance = 1000
            };
        }

        /// <returns>A static obstacle 1 km away.</returns>
        /// <remarks>
        /// Despawner attempts to behave like a black hole, but this method needs to return some obstacle.
        /// </remarks>
        public ObstacleSeekInfo SeekObstacleFromEnd(double addDistance = 0, int limit = 99)
        {
            return new ObstacleSeekInfo
            {
                Agent = null,
                Distance = 1000
            };
        }

        /// <summary>
        /// Despawner does not have a graphical representation, therefore this method does nothing.
        /// </summary>
        public void DrawRoad(RenderWindow window)
        {
        }

        /// <summary>
        /// Despawner does not hold agents, therefore this method does nothing.
        /// </summary>
        public void DrawAgents(RenderWindow window)
        {
        }

        /// <summary>Does nothing but increment AgentLimit.</summary>
        /// <param name="agent">The agent to remove.</param>
        /// <remarks>
        /// Because this method is relied upon for maintaining a reference to an
        /// agent (moving vehicles for one segment to another is implemented by removing an agent from one road segment
        /// and inserting it to another), by not doing anything with the agent this effectively removes him from the
        /// simulation.
        /// </remarks>
        public void InsertAgent(Agent agent)
        {
            if (AgentLimit != null) ++AgentLimit.Value;
            if (Throughput != null) ++Throughput.Value;
            if (AgentLifetimeSum != null) AgentLifetimeSum.Value += agent.Lifetime;
            ++agent.RouteThroughput.Value;
        }

        /// <returns>Vector (0, 0).</returns>
        public Vector LongitudinalPositionToXY(double position)
        {
            return new Vector(0, 0);
        }

        public double GetLength() { return 0; }
    }
}