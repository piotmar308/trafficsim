﻿using System;
using System.Collections.Generic;
using System.Windows;
using SFML.Graphics;
using SFML.System;
using trafficsim.Simulation.Intersection;
using trafficsim.Simulation.Struct;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Simulation.RoadSegment
{
    /// <summary>
    /// Class for modelling paths through intersections.
    /// </summary>
    public class TrafficLightsIntersectionSegment: RightOfWayIntersectionSegment
    {
        public bool RedLight { get; set; } = false;
        public bool AmberLight { get; set; } = false;

        /// <inheritdoc/>
        public TrafficLightsIntersectionSegment(IEnumerable<Vector> vertices) : base(vertices)
        {
        }

        public override void DrawRoad(RenderWindow window)
        {
            VertexArray lines = new VertexArray(PrimitiveType.LineStrip);
            Color color = Color.Green;
            if (RedLight)
            {
                color = Color.Red;
            }
            else if (AmberLight)
            {
                color = new Color(255, 192, 0);
            }
            foreach (Vector vertex in Vertices)
            {
                lines.Append(new Vertex(new Vector2f((float) vertex.X, (float) vertex.Y), color));
            }
            window.Draw(lines);
        }

        /// <summary>
        /// When checked for for first obstacle, whether to behave as if this road segment existed, which is the case
        /// when Closed = true, RedLight = true or when AmberLight = true and agent isn't moving or is able to safely
        /// decelerate before entering this segment.
        /// </summary>
        protected override bool ShouldSegmentDisallowEntry(Agent agent)
        {
            double distanceFromAgent = GetDistanceFromAgentToThisSegment(agent,
                TrafficLightsIntersectionManager.TimeFrameComputationDistanceLimit);
            ObstacleInfo closedFirstObstacle = new ObstacleInfo
            {
                DistanceToObstacle = distanceFromAgent >= 0 ? distanceFromAgent : 0,
                ObstacleForwardVelocity = 0
            };
            bool agentCanSafelyStop = agent.GetAccelerationForObstacle(closedFirstObstacle, 99) >
                                      -agent.SafeDecelerationThreshold;

            return Closed || RedLight || AmberLight && (agentCanSafelyStop || agent.Velocity < 1);
        }
    }
}