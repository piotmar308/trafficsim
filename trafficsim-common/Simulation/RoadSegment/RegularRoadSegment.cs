﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using SFML.Graphics;
using SFML.System;
using trafficsim.Simulation.Struct;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Simulation.RoadSegment
{
    /// <summary>
    /// Class for modelling road segments that are not part of intersections.
    /// </summary>
    public class RegularRoadSegment: IRoadSegment
    {
        /// <summary>
        /// Vertices of the broken line making the shape of the road segment.
        /// </summary>
        public List<Vector> Vertices { get; }
        /// <summary>
        /// Lengths of the subsegments of the broken line.
        /// </summary>
        public List<double> SubSegmentLengths { get; } = new List<double>();
        /// <summary>
        /// Collection of adjacent lanes.
        /// </summary>
        public HashSet<AdjacentLaneInfo> AdjacentLanes { get; } = new HashSet<AdjacentLaneInfo>();
        /// <summary>
        /// Speed limit on this segment in m/s.
        /// </summary>
        public double SpeedLimit { get; set; } = 14;
        /// <summary>
        /// Total length of this road segment, in meters.
        /// </summary>
        public double Length { get; } = 0;
        /// <summary>
        /// Collection of forward road segment connections.
        /// </summary>
        public HashSet<NextRoadSegmentInfo> Next { get; } = new HashSet<NextRoadSegmentInfo>();
        /// <summary>
        /// Collection of road segments connecting to this one.
        /// </summary>
        public HashSet<IRoadSegment> Previous { get; } = new HashSet<IRoadSegment>();
        /// <summary>
        /// Collection of all agents on this road segment.
        /// </summary>
        protected readonly SortedSet<Agent> Agents = new SortedSet<Agent>(new AgentPositionComparer());
        /// <summary>
        /// RNG object set up for repeatability.
        /// </summary>
        private readonly Random _random = new Random(0);

        /// <summary>
        /// Creates the road segment in shape of a broken line with given vertices.
        /// </summary>
        /// <param name="vertices">Vertices of the broken line.</param>
        public RegularRoadSegment(IEnumerable<Vector> vertices)
        {
            Vertices = new List<Vector>(vertices ?? new Vector[] {});
            for (int i = 0; i < Vertices.Count - 1; ++i)
            {
                double subSegmentLength = new Vector(Vertices[i].X - Vertices[i + 1].X, Vertices[i].Y - Vertices[i + 1].Y).Length;
                SubSegmentLengths.Add(subSegmentLength);
                Length += subSegmentLength;
            }
        }

        /// <inheritdoc/>
        public void AddNext(IRoadSegment next, double probability = 1, TurnDirection direction = TurnDirection.Forward)
        {
            if (Next.All(turn => turn.Segment != next))
            {
                Next.Add(new NextRoadSegmentInfo { Segment = next, Probability = probability, Direction = direction});
                next.AddPrevious(this);
            }
        }

        /// <inheritdoc/>
        public void AddPrevious(IRoadSegment previous)
        {
            if (!Previous.Contains(previous))
            {
                Previous.Add(previous);
                previous.AddNext(this);
            }
        }

        /// <param name="segment">Checked other segment.</param>
        /// <returns>
        /// True, if to this segment and to the other one the same road segments make forward connections to, otherwise
        /// false.
        /// </returns>
        public bool HasSamePredecessors(RegularRoadSegment segment)
        {
            return Previous.SetEquals(segment.Previous);
        }

        /// <inheritdoc/>
        public IRoadSegment GetRandomNext(ICollection<IRoadSegment> intersectNextSegments = null)
        {
            HashSet<NextRoadSegmentInfo> nextToChooseFrom = new HashSet<NextRoadSegmentInfo>(Next.Where(s =>
                intersectNextSegments == null || intersectNextSegments.Contains(s.Segment)));
            if (nextToChooseFrom.Count == 0)
            {
                return null;
            }
            double sumOfProbabilities = nextToChooseFrom.Select(s => s.Probability).Sum();
            double n = _random.NextDouble() * sumOfProbabilities;
            foreach (NextRoadSegmentInfo next in nextToChooseFrom)
            {
                n -= next.Probability;
                if (n <= 0) return next.Segment;
            }

            return nextToChooseFrom.Last().Segment;
        }

        /// <returns>
        /// The next road segment, if there is exactly one forward connection and it is to a RegularRoadSegment,
        /// otherwise null.
        /// </returns>
        public RegularRoadSegment GetNextRegularRoadSegment()
        {
            return Next.Count == 1 ? Next.First().Segment as RegularRoadSegment : null;
        }

        /// <inheritdoc/>
        public virtual void SimulateAccelerationChanges()
        {
            foreach (Agent agent in Agents)
            {
                ObstacleInfo obstacle = new ObstacleInfo(SeekObstacleFromAgent(agent, false));
                double a = agent.GetAccelerationForObstacle(obstacle, SpeedLimit);
                agent.UpdateAcceleration(obstacle, SpeedLimit);
            }
        }

        /// <inheritdoc/>
        public virtual void SimulateLaneChanges()
        {
            foreach (AdjacentLaneInfo adjacentLane in AdjacentLanes)
            {
                foreach (Agent agent in Agents.Where(a => a.Route == null || a.Route.Contains(adjacentLane.Lane)).ToArray())
                {
                    double newPosition = agent.Position * adjacentLane.Lane.Length / Length;
                    ObstacleSeekInfo oldObstacleInFront = SeekObstacleFromAgent(agent, false);
                    ObstacleSeekInfo oldObstacleBehind = SeekObstacleFromAgent(agent, true);
                    ObstacleSeekInfo newObstacleInFront = adjacentLane.Lane.SeekObstacleForwards(newPosition, agent);
                    ObstacleSeekInfo newObstacleBehind = adjacentLane.Lane.SeekObstacleBackwards(newPosition);

                    if (newObstacleInFront.Distance > 0 && newObstacleBehind.Distance > agent.Length) // if there is space to change lane
                    {
                        double newObstacleBehindNewAcceleration =
                            newObstacleBehind.Agent?.GetAccelerationForObstacle(
                                new ObstacleInfo
                                {
                                    DistanceToObstacle = newObstacleBehind.Distance - agent.Length,
                                    ObstacleForwardVelocity = agent.Velocity
                                }, adjacentLane.Lane.SpeedLimit) ?? 0;
                        double newObstacleBehindDecelerationThreshold =
                            newObstacleBehind.Agent?.SafeDecelerationThreshold ?? 0;

                        if (newObstacleBehindNewAcceleration >= -newObstacleBehindDecelerationThreshold) // if it is safe to change lane
                        {
                            double newAcceleration = agent.GetAccelerationForObstacle(
                                new ObstacleInfo
                                {
                                    DistanceToObstacle = newObstacleInFront.Distance,
                                    ObstacleForwardVelocity = newObstacleInFront.Agent?.Velocity ?? 0
                                }, adjacentLane.Lane.SpeedLimit);
                            double oldObstacleBehindNewAcceleration =
                                oldObstacleBehind.Agent?.GetAccelerationForObstacle(
                                    new ObstacleInfo
                                    {
                                        DistanceToObstacle = oldObstacleBehind.Distance + oldObstacleInFront.Distance,
                                        ObstacleForwardVelocity = oldObstacleInFront.Agent?.Velocity ?? 0
                                    }, SpeedLimit) ?? 0;

                            agent.DecideOnLaneChange
                            (
                                adjacentLane: adjacentLane,
                                newAcceleration: newAcceleration,
                                oldFollowerOldAcceleration: oldObstacleBehind.Agent?.Acceleration ?? 0,
                                oldFollowerNewAcceleration: oldObstacleBehindNewAcceleration,
                                newFollowerOldAcceleration: newObstacleBehind.Agent?.Acceleration ?? 0,
                                newFollowerNewAcceleration: newObstacleBehindNewAcceleration
                            );

                            if (agent.NewLane != null)
                            {
                                Agents.RemoveWhere(v => v == agent);
                                agent.Position *= agent.NewLane.Length / Length;
                                agent.NewLane.InsertAgent(agent);
                                agent.ConfirmLaneChange(this);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Update this segment's agents' positions and velocities. This method only mutates agent on this road segment
        /// and does not move or remove them.
        /// </summary>
        /// <param name="timeDiff">Amount of time simulated.</param>
        public void UpdateAgentsPositionsAndVelocities(double timeDiff)
        {
            foreach (Agent agent in Agents)
            {
                agent.UpdateVelocity(timeDiff);
                agent.UpdatePosition(timeDiff);
                agent.Lifetime += timeDiff;
            }
        }

        /// <summary>
        /// Move this segment's agents, who require it, to next road segments. This method can mutate other
        /// road segments and moved agents.
        /// </summary>
        public virtual void MoveAgentsToRespectiveSegments()
        {
            // Because SortedSet evaluates order on insertion, order can't be changed while object is in the set.
            // Otherwise the set breaks. Thus objects are first fetched, then mutated, and then added to the destination
            // set and removed from the source set.

            if (Next.Count > 0)
            {
                Agent[] agentsWhichDroveToNextSegment = Agents.Where(v => v.Position >= Length).ToArray();
                Agents.RemoveWhere(v => v.Position >= Length);
                foreach (Agent agent in agentsWhichDroveToNextSegment)
                {
                    IRoadSegment next = agent.DecideOnSegmentChange(this); // next shouldn't be null here
                    agent.Position -= Length;
                    next.InsertAgent(agent);
                    agent.ConfirmSegmentChange(this);
                }
            }
        }

        /// <inheritdoc/>
        public virtual ObstacleSeekInfo SeekObstacleForwards(double position, Agent agent, double addDistance = 0, int limit = 99)
        {
            if (limit <= 0)
            {
                return new ObstacleSeekInfo {Distance = addDistance, Agent = null};
            }

            if (Agents.Count > 0 && Agents.Last().Position > position)
            {
                Agent agentInFront = Agents.SkipWhile(v => v.Position <= position).First();

                ObstacleSeekInfo agentInFrontObstacle = new ObstacleSeekInfo
                {
                    Distance = agentInFront.Position - position - agentInFront.Length + addDistance,
                    Agent = agentInFront
                };

                if (agent != null && agent.DecideOnSegmentChange(this) == null)
                {
                    ObstacleSeekInfo nextSegmentObstacle = new ObstacleSeekInfo
                            {Agent = null, Distance = Length - position + addDistance};

                    // An agent's predecessor might be driving at full speed onto the next road segment, but he might
                    // not be able to do so as well. To account for that, the car following model is given the option
                    // to decide on his acceleration by looking at the vehicle in front, as well as only the next
                    // segment and the "safer" result is used.
                    if (agent.GetAccelerationForObstacle(new ObstacleInfo(agentInFrontObstacle), SpeedLimit) <=
                        agent.GetAccelerationForObstacle(new ObstacleInfo(nextSegmentObstacle), SpeedLimit))
                    {
                        return agentInFrontObstacle;
                    }
                    else
                    {
                        return nextSegmentObstacle; // This right here is the special case where the function might not
                                                    // return the obstacle that is closest to the agent, but one, that
                                                    // is more dangerous.
                    }
                }
                else
                {
                    return agentInFrontObstacle;
                }
            }
            else
            {
                if (agent != null)
                {
                    IRoadSegment nextSegment = agent.DecideOnSegmentChange(this);
                    if (nextSegment != null)
                    {
                        return nextSegment.SeekObstacleForwards(0, agent, Length - position + addDistance, limit);
                    }
                    else
                    {
                        return new ObstacleSeekInfo
                            {Agent = null, Distance = Length - position + addDistance};
                    }
                }
                else if (Next.Count == 1)
                {
                    return Next.First().Segment.SeekObstacleForwards(0, agent, Length - position + addDistance, limit - 1);
                }
                else
                {
                    return new ObstacleSeekInfo {Distance = Length - position + addDistance, Agent = null};
                }
            }
        }

        /// <inheritdoc/>
        public ObstacleSeekInfo SeekObstacleBackwards(double position, double addDistance = 0, int limit = 99)
        {
            if (limit <= 0)
            {
                return new ObstacleSeekInfo {Distance = addDistance, Agent = null};
            }

            if (Agents.Count > 0 && Agents.First().Position < position)
            {
                Agent agentBehind = Agents.TakeWhile(v => v.Position < position).Last();

                return new ObstacleSeekInfo
                {
                    Distance = position - agentBehind.Position + addDistance,
                    Agent = agentBehind
                };
            }
            else
            {
                if (Previous.Count == 1)
                {
                    return Previous.First().SeekObstacleFromEnd(addDistance + position, limit - 1);
                }
                else
                {
                    return new ObstacleSeekInfo {Distance = position + addDistance, Agent = null};
                }
            }
        }

        /// <inheritdoc/>
        public ObstacleSeekInfo SeekObstacleFromEnd(double addDistance = 0, int limit = 99)
        {
            return SeekObstacleBackwards(Length, addDistance, limit);
        }

        /// <param name="agent">Whose agent's position to seek from.</param>
        /// <param name="seekBackwards">True to seek backwards, false to seek forwards.</param>
        /// <returns>
        /// Information about first obstacle looking in a specified direction from a position of a given agent.
        /// </returns>
        public ObstacleSeekInfo SeekObstacleFromAgent(Agent agent, bool seekBackwards)
        {
            return seekBackwards ? SeekObstacleBackwards(agent.Position) : SeekObstacleForwards(agent.Position, agent);
        }

        /// <inheritdoc/>
        public virtual void DrawRoad(RenderWindow window)
        {
            VertexArray lines = new VertexArray(PrimitiveType.LineStrip);
            foreach (Vector vertex in Vertices)
            {
                lines.Append(new Vertex(new Vector2f((float) vertex.X, (float) vertex.Y), Color.Black)); //IsSubjectToManualTransformation() ? Color.Black : Color.Magenta));
            }
            window.Draw(lines);
        }

        /// <inheritdoc/>
        public void DrawAgents(RenderWindow window)
        {
            foreach (Agent agent in Agents)
            {
                IRoadSegment next = agent.DecideOnSegmentChange(this);
                agent.Draw(window, this,
                    next != null
                        ? Next.First(n => n.Segment == next).Direction
                        : TurnDirection.Forward);
            }
        }

        /// <inheritdoc/>
        public virtual void InsertAgent(Agent agent)
        {
            Agents.Add(agent);
        }

        /// <returns>Array of agents on this road segment, copy of the internal Agents collection.</returns>
        public Agent[] GetAgents()
        {
            return Agents.ToArray();
        }

        /// <param name="distanceSoftLimit">
        /// Recurrence limiter. Information is gathered from road segments within reach of given distance limit (in
        /// meters), including about agents outside this limit on checked segments.
        /// </param>
        /// <param name="wherePath">
        /// Used for recurrence to only gather information about agents, who are actually going to follow the checked
        /// path.
        /// </param>
        /// <returns>
        /// Relative times of when agents on RegularRoadSegments segments are expected to enter this road segment.
        /// Estimation is based on speed limits on road segments
        /// </returns>
        /// <remarks>
        /// Information is only gathered from preceding road segments, not adjacent ones. Stillness is defined as
        /// moving slower, than 1 m/s and not accelerating.
        /// </remarks>
        public IEnumerable<(double time, bool still)> GetEntryTimes(double distanceSoftLimit,
            IEnumerable<IRoadSegment> wherePath = null)
        {
            if (distanceSoftLimit < 0) return new HashSet<(double, bool)>();
            if (wherePath == null) wherePath = new[] {this};

            HashSet<(double time, bool still)> entryTimes = new HashSet<(double, bool)>();
            foreach (RegularRoadSegment seg in Previous.OfType<RegularRoadSegment>())
            {
                List<IRoadSegment> checkedPath = new List<IRoadSegment>(wherePath);
                checkedPath.Insert(0, seg);

                entryTimes.UnionWith(seg.Agents
                    .Where(a => a.FollowsPath(checkedPath))
                    .Select(agent => (time: (seg.Length - agent.Position) / seg.SpeedLimit,
                        still: agent.Velocity < 1 && agent.Acceleration <= 0)));
                entryTimes.UnionWith(seg
                    .GetEntryTimes(distanceSoftLimit - seg.Length, checkedPath)
                    .Select(t => (time: t.time + seg.Length / seg.SpeedLimit, still: t.still)));
            }

            return entryTimes;
        }

        /// <param name="agent">
        /// Agent whose distance to this road segment we are measuring.
        /// </param>
        /// <param name="distanceSoftLimit">
        /// Recurrence limiter. Information is gathered from road segments within reach of given distance limit (in
        /// meters), including about agents outside this limit on checked segments.
        /// </param>
        /// <param name="wherePath">
        /// Used for recurrence to only gather information about agents, who are actually going to follow the checked
        /// path.
        /// </param>
        /// <returns>
        /// Distance from agent's position to the beginning of this road segment, or -1 if agent can't be found.
        /// </returns>
        public double GetDistanceFromAgentToThisSegment(Agent agent, double distanceSoftLimit,
            IEnumerable<IRoadSegment> wherePath = null)
        {
            if (distanceSoftLimit < 0) return 0;
            if (wherePath == null) wherePath = new[] {this};

            // Checking directly preceding segments
            foreach (RegularRoadSegment seg in Previous.OfType<RegularRoadSegment>())
            {
                foreach (Agent precedingSegmentAgent in seg.GetAgents())
                {
                    if (precedingSegmentAgent == agent)
                    {
                        return seg.Length - precedingSegmentAgent.Position;
                    }
                }
            }

            // Checking previous segments recurrently
            foreach (RegularRoadSegment seg in Previous.OfType<RegularRoadSegment>())
            {
                List<IRoadSegment> checkedPath = new List<IRoadSegment>(wherePath);
                checkedPath.Insert(0, seg);

                double distance =
                    seg.GetDistanceFromAgentToThisSegment(agent, distanceSoftLimit - seg.Length, checkedPath);
                if (distance >= 0)
                {
                    return distance + seg.Length;
                }
            }

            return -1;
        }

        /// <summary>
        /// For a given longitudinal position determines euclidean plane coordinates.
        /// </summary>
        /// <param name="position">Longitudinal position in meters.</param>
        /// <returns>Euclidean plane coordinates this position corresponds to.</returns>
        /// <remarks>
        /// Negative positions and positions greater than total length of the segment are legal. First and last
        /// subsegments are interpolated to get these coordinates.
        /// </remarks>
        public Vector LongitudinalPositionToXY(double position)
        {
            if (position > Length)
            {
                if (Next.Count == 1) return Next.First().Segment.LongitudinalPositionToXY(position - Length);
                else position = Length;
            }

            int subSegmentIndex = 0;
            double positionInSubSegment = position;
            while (subSegmentIndex < SubSegmentLengths.Count &&
                   positionInSubSegment > SubSegmentLengths[subSegmentIndex])
            {
                positionInSubSegment -= SubSegmentLengths[subSegmentIndex];
                ++subSegmentIndex;
            }

            if (subSegmentIndex >= SubSegmentLengths.Count)
            {
                subSegmentIndex = SubSegmentLengths.Count - 1;
            }

            Vector subSegmentVector = Vertices[subSegmentIndex + 1] - Vertices[subSegmentIndex];
            return Vertices[subSegmentIndex] +
                   subSegmentVector * positionInSubSegment / SubSegmentLengths[subSegmentIndex];
        }

        public double GetLength()
        {
            return Length;
        }

        public bool IsNextToToIntersection()
        {
            return Next.Any(s => s.Segment is RightOfWayIntersectionSegment) ||
                   Previous.Any(s => s is RightOfWayIntersectionSegment);
        }

        public bool CanHaveHandPickedLanes()
        {
            return !IsNextToToIntersection() || this is RightOfWayIntersectionSegment;
        }
    }
}