﻿using System.Collections.Generic;
using System.Windows;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Simulation.RoadSegment
{
    /// <summary>
    /// Class for modelling paths through intersections.
    /// </summary>
    public class RightHandRuleIntersectionSegment: RightOfWayIntersectionSegment
    {
        /// <summary>
        /// Set to true, if one agent should be able to enter this road segment.
        /// </summary>
        /// <remarks>
        /// Setting LetOneAgentThrough = true forces the road to be open regardless of the property Closed.
        /// </remarks>
        public bool LetOneAgentThrough { get; set; } = false;
        /// <summary>
        /// Agent, who was let through with LetOneAgentThrough = true and who needs to leave this segment for it to open
        /// back up.
        /// </summary>
        /// <remarks>
        /// Setting _keepClosedUntilAgentLeaves != null forces the road to be closed regardless of properties Closed and
        /// LetOneAgentThrough.
        /// </remarks>
        private Agent _keepClosedUntilAgentLeaves = null;

        /// <inheritdoc/>
        public RightHandRuleIntersectionSegment(IEnumerable<Vector> vertices) : base(vertices)
        {
        }

        /// <summary>
        /// When checked for for first obstacle, whether to behave as if this road segment existed, which is the case
        /// when Closed = true, LetOneAgentThrough = false , or if _keepClosedUntilAgentLeaves != null.
        /// </summary>
        protected override bool ShouldSegmentDisallowEntry(Agent agent)
        {
            return Closed && !LetOneAgentThrough || Agents.Contains(_keepClosedUntilAgentLeaves);
        }

        /// <summary>
        /// Inserts agent to this road segment and if LetOneAgentThrough = true, then it resets this boolean and sets
        /// _keepClosedUntilAgentLeaves to the agent who entered.
        /// </summary>
        /// <param name="agent">Agent to insert.</param>
        public override void InsertAgent(Agent agent)
        {
            if (LetOneAgentThrough)
            {
                LetOneAgentThrough = false;
                _keepClosedUntilAgentLeaves = agent;
            }

            base.InsertAgent(agent);
        }

        /// <inheritdoc/>
        /// <remarks>
        /// If _keepClosedUntilAgentLeaves exists, but has moved from this segment, then it resets this field.
        /// </remarks>
        public override void MoveAgentsToRespectiveSegments()
        {
            base.MoveAgentsToRespectiveSegments();

            if (_keepClosedUntilAgentLeaves != null && !Agents.Contains(_keepClosedUntilAgentLeaves))
            {
                _keepClosedUntilAgentLeaves = null;
            }
        }

        /// <returns>True, if LetOneAgentThrough = true or _keepClosedUntilAgentLeaves != null</returns>
        public bool IsHardDeadLockInResolution()
        {
            return LetOneAgentThrough || _keepClosedUntilAgentLeaves != null;
        }
    }
}