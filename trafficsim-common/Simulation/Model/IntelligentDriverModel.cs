﻿using System;
using trafficsim.Simulation.Struct;

namespace trafficsim.Simulation.Model
{
    /// <summary>
    /// Class implementing Intelligent Driver Model.
    /// </summary>
    public class IntelligentDriverModel: ICarFollowingModel
    {
        /// <summary>
        /// Minimum allowed time it would take to reach vehicle in front (or any other obstacle), in seconds.
        /// </summary>
        public double SafeTimeHeadway { set; get; } = 1.5;
        /// <summary>
        /// The maximum vehicle acceleration, in meters per second squared.
        /// </summary>
        public double MaximumAcceleration { set; get; } = 3;
        /// <summary>
        /// Comfortable braking deceleration, in meters per second squared.
        /// </summary>
        public double ComfortableDeceleration { set; get; } = 1.67;
        /// <summary>
        /// Parameter without direct real world meaning, usually set to 4.
        /// </summary>
        public double AccelerationExponent { set; get; } = 4;
        /// <summary>
        /// Minimum desired spacing between modeled vehicle and the vehicle in front (or any other obstacle), in meters.
        /// </summary>
        public double MinimumDistance { set; get; } = 2;

        /// <inheritdoc/>
        public double GetAcceleration(double vehicleForwardVelocity, ObstacleInfo obstacleInfo, double speedLimit)
        {
            return MaximumAcceleration *
                   (1 - Math.Pow(vehicleForwardVelocity / speedLimit, AccelerationExponent) -
                    Math.Pow(
                        SStar(vehicleForwardVelocity, vehicleForwardVelocity - obstacleInfo.ObstacleForwardVelocity) /
                        Math.Max(0, obstacleInfo.DistanceToObstacle), 2));
        }

        private double SStar(double vehicleForwardVelocity, double obstacleRelativeVelocity)
        {
            return MinimumDistance +
                   Math.Max(0.0,
                       vehicleForwardVelocity * SafeTimeHeadway +
                       (vehicleForwardVelocity * obstacleRelativeVelocity) /
                       (2.0 * Math.Sqrt(MaximumAcceleration * ComfortableDeceleration)));
        }
    }
}