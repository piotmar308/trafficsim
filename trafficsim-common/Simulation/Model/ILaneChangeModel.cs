﻿namespace trafficsim.Simulation.Model
{
    /// <summary>
    /// Interface for lane changing behavioral models.
    /// </summary>
    public interface ILaneChangeModel
    {
        /// <param name="oldAcceleration">
        /// Current acceleration of the modelled vehicle.
        /// </param>
        /// <param name="newAcceleration">
        /// Acceleration the modelled vehicle would have if it changed lane.
        /// </param>
        /// <param name="oldFollowerOldAcceleration">
        /// Current acceleration of the vehicle behind the modelled vehicle.
        /// </param>
        /// <param name="oldFollowerNewAcceleration">
        /// Acceleration of the vehicle currently behind the modelled vehicle if the modelled vehicle had changed lane.
        /// </param>
        /// <param name="newFollowerOldAcceleration">
        /// Current acceleration of the vehicle, that would be behind the modelled vehicle, if the modelled vehicle had
        /// changed lane.
        /// </param>
        /// <param name="newFollowerNewAcceleration">
        /// Acceleration of the vehicle, that would be behind the modelled vehicle if the modelled vehicle had changed
        /// lane, if the modelled vehicle had changed lane.
        /// </param>
        /// <param name="bias">
        /// Bias for changing to the adjacent lane, in meters per second squared.
        /// </param>
        /// <returns>
        /// Whether to change to an adjacent lane assuming that it is possible and safe, given the situation.
        /// </returns>
        /// <remarks>Acceleration values should be in meter per second squared.</remarks>
        bool IncentiveToChange(double oldAcceleration, double newAcceleration, double oldFollowerOldAcceleration,
            double oldFollowerNewAcceleration, double newFollowerOldAcceleration, double newFollowerNewAcceleration,
            double bias);
    }
}