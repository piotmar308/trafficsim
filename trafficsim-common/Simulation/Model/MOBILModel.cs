﻿namespace trafficsim.Simulation.Model
{
    /// <summary>
    /// Class implementing Minimizing Overall Braking Induced by Lane change behavioral model.
    /// </summary>
    public class MOBILModel: ILaneChangeModel
    {
        /// <summary>
        /// <para>
        /// Value characterizing how much modelled agent cares about overall traffic flow, or himself.
        /// </para>
        /// <para>
        /// Greater than 0.5 is altruistic behavior. Agent will prioritize increasing the acceleration of other agents.
        /// </para>
        /// <para>
        /// Between 0 and 0.5 is normal behavior.
        /// </para>
        /// <para>
        /// 0 is purely selfish behavior. Agent will optimize for his own acceleration disregarding other agents.
        /// </para>
        /// <para>
        /// Less than 0 is actively disruptive behavior. Agent will try to slow other agents down at no one's benefit.
        /// </para>
        /// </summary>
        public double Politeness { get; set; } = 0.1;
        /// <summary>
        /// Threshold of advantage from lane changing, below which lane change will not occur, in meters per second
        /// squared.
        /// </summary>
        public double LaneChangeThreshold { get; set; } = 0.1;

        /// <inheritdoc/>
        public bool IncentiveToChange(double oldAcceleration, double newAcceleration, double oldFollowerOldAcceleration,
            double oldFollowerNewAcceleration, double newFollowerOldAcceleration, double newFollowerNewAcceleration,
            double bias)
        {
            return newAcceleration - oldAcceleration + Politeness *
                   (newFollowerNewAcceleration - newFollowerOldAcceleration + oldFollowerNewAcceleration -
                    oldFollowerOldAcceleration) > LaneChangeThreshold - bias;
        }
    }
}