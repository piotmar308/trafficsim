﻿using trafficsim.Simulation.Struct;

namespace trafficsim.Simulation.Model
{
    /// <summary>
    /// Interface for car following behavioral models.
    /// </summary>
    public interface ICarFollowingModel
    {
        /// <param name="vehicleForwardVelocity">Current velocity of the vehicle in meters per second.</param>
        /// <param name="obstacleInfo">Closest obstacle in front of the vehicle.</param>
        /// <param name="speedLimit">The speed limit that the agent should adhere to.</param>
        /// <returns>Acceleration of the vehicle in a given situation.</returns>
        double GetAcceleration(double vehicleForwardVelocity, ObstacleInfo obstacleInfo, double speedLimit);
    }
}