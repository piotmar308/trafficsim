﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using trafficsim.Helper;
using trafficsim.Simulation;
using trafficsim.Simulation.Intersection;
using trafficsim.Simulation.Model;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Struct;

namespace trafficsim_scenario_transformer
{
    public enum TransformerMode
    {
        Binary,
        Gray,
        LaneCount
    }

    public class Transformer
    {
        public static int LayersMax = 8;
        public static double DistanceOffset = 0.5;

        public static Scenario CopyAndTransform(Scenario inputScenario, byte[] genotype, TransformerMode mode)
        {
            Ref<int> agentLimit =
                inputScenario.AgentLimit != null ? new Ref<int>(inputScenario.AgentLimit.Value) : null;
            Ref<int> totalAgentsSpawned = new Ref<int>(0);
            DespawnerSegment despawner = new DespawnerSegment { AgentLimit = agentLimit, Throughput = new Ref<int>(0), AgentLifetimeSum = new Ref<double>(0)};
            IEnumerable<SpawnerSegment> inputSpawners = inputScenario.RoadSegments.Where(s => s is SpawnerSegment).Cast<SpawnerSegment>();
            RegularRoadSegment[] inputSegments = inputScenario.RoadSegments.Where(s => s is RegularRoadSegment).Cast<RegularRoadSegment>().ToArray();
            bool[,] fillMatrix = new bool[inputSegments.Length, LayersMax];
            bool[] autofill = new bool[inputSegments.Length];
            for (int segmentIndex = 0, genotypeIndex = 0; segmentIndex < inputSegments.Length; ++segmentIndex)
            {
                if (inputSegments[segmentIndex].CanHaveHandPickedLanes())
                {
                    byte layerMask = 1, layer = 0;
                    do
                    {
                        if (mode == TransformerMode.Binary && (genotype[genotypeIndex] & layerMask) != 0 ||
                            mode == TransformerMode.Gray && ((genotype[genotypeIndex] ^ (genotype[genotypeIndex] >> 1)) & layerMask) != 0 ||
                            mode == TransformerMode.LaneCount && layer < genotype[genotypeIndex])
                        {
                            fillMatrix[segmentIndex, layer] = true;
                        }
                        layerMask <<= 1;
                        ++layer;
                    } while (layer < LayersMax);

                    ++genotypeIndex;
                }
                else
                {
                    autofill[segmentIndex] = true;
                }
            }

            for (int segmentIndex = 0; segmentIndex < inputSegments.Length; ++segmentIndex)
            {
                if (autofill[segmentIndex])
                {
                    foreach (RegularRoadSegment nextOrPrevSegment in inputSegments[segmentIndex].Next
                        .Where(nrsi => nrsi.Segment is RegularRoadSegment)
                        .Select(nrsi => nrsi.Segment as RegularRoadSegment)
                        .Concat(inputSegments[segmentIndex].Previous
                            .Where(s => s is RegularRoadSegment).Cast<RegularRoadSegment>()))
                    {
                        int nextOrPrevSegmentIndex = Array.FindIndex(inputSegments, s => s == nextOrPrevSegment);

                        for (int j = 0; j < LayersMax; ++j)
                        {
                            if (fillMatrix[nextOrPrevSegmentIndex, j])
                            {
                                fillMatrix[segmentIndex, j] = true;
                            }
                        }
                    }
                }
            }

            RegularRoadSegment[,] segmentsMatrix = new RegularRoadSegment[inputSegments.Length, LayersMax];
            int i = 0;
            foreach (RegularRoadSegment inputSegment in inputSegments)
            {
                for (int j = 0; j < LayersMax; ++j)
                {
                    if (fillMatrix[i, j])
                    {
                        switch (inputSegment)
                        {
                            case TrafficLightsIntersectionSegment _:
                                segmentsMatrix[i, j] = new TrafficLightsIntersectionSegment(OffsetVertices(inputSegment, j * DistanceOffset));
                                break;
                            case RightHandRuleIntersectionSegment _:
                                segmentsMatrix[i, j] = new RightHandRuleIntersectionSegment(OffsetVertices(inputSegment, j * DistanceOffset));
                                break;
                            case RightOfWayIntersectionSegment _:
                                segmentsMatrix[i, j] = new RightOfWayIntersectionSegment(OffsetVertices(inputSegment, j * DistanceOffset));
                                break;
                            default:
                                segmentsMatrix[i, j] = new RegularRoadSegment(OffsetVertices(inputSegment, j * DistanceOffset));
                                break;
                        }

                        segmentsMatrix[i, j].SpeedLimit = inputSegment.SpeedLimit;
                    }
                }
                ++i;
            }
            List<SpawnerSegment> spawners = new List<SpawnerSegment>();
            SpawnerSegment.RandomSeed = 0;
            foreach (SpawnerSegment inputSpawner in inputSpawners)
            {
                inputSpawner.Factory.ResetRNG();
                spawners.Add(new SpawnerSegment(inputSpawner.Factory) {
                    SpawnFrequency = inputSpawner.SpawnFrequency,
                    AgentLimit = agentLimit,
                    Id = inputSpawner.Id,
                    Route = GenerateRoute(segmentsMatrix, inputSegments, inputSpawner.Route, despawner),
                    TotalAgentsSpawned = totalAgentsSpawned
                });
            }

            ConnectAdjacentSegments(segmentsMatrix, inputSegments);
            ConnectConsecutiveSegments(segmentsMatrix, inputSegments, despawner);
            ConnectSegmentsLayers(segmentsMatrix);
            ConnectSpawnersToSegments(spawners, inputSpawners, segmentsMatrix, inputSegments);
            IEnumerable<IRoadSegment> allSegments = segmentsMatrix.Cast<IRoadSegment>()
                .Where(segment => segment != null).Concat(spawners).Append(despawner);

            List<IIntersectionManager> intersections = new List<IIntersectionManager>();
            foreach (IIntersectionManager inputIntersection in inputScenario.Intersections)
            {
                switch (inputIntersection)
                {
                    case TrafficLightsIntersectionManager trafficLightsInputIntersection:
                        intersections.Add(new TrafficLightsIntersectionManager(
                            GenerateCrossings(segmentsMatrix, inputSegments, trafficLightsInputIntersection.Crossings),
                            GenerateTrafficLightsPhases(segmentsMatrix, inputSegments,
                                trafficLightsInputIntersection.Phases)));
                        break;
                    case RightHandRuleIntersectionManager rightHandRuleInputIntersection:
                        intersections.Add(new RightHandRuleIntersectionManager(
                            GenerateCrossings(segmentsMatrix, inputSegments, rightHandRuleInputIntersection.Crossings),
                            GenerateSidesClockwise(segmentsMatrix, inputSegments,
                                rightHandRuleInputIntersection.SidesClockwise)));
                        break;
                    case RightOfWayIntersectionManager rightOfWayInputIntersection:
                        intersections.Add(new RightOfWayIntersectionManager(
                            GenerateCrossings(segmentsMatrix, inputSegments, rightOfWayInputIntersection.Crossings)));
                        break;
                }
            }

            return new Scenario(allSegments, intersections) { ViewRadius = inputScenario.ViewRadius, TotalThroughput = despawner.Throughput, TotalAgentsSpawned = totalAgentsSpawned, AgentLifetimeSum = despawner.AgentLifetimeSum, AgentLimit = despawner.AgentLimit };
        }

        private static ICollection<IRoadSegment> GenerateRoute(RegularRoadSegment[,] segmentsMatrix, RegularRoadSegment[] inputSegments, ICollection<IRoadSegment> inputSpawnerRoute, DespawnerSegment despawnerSegment)
        {
            HashSet<IRoadSegment> route = new HashSet<IRoadSegment>();
            foreach (IRoadSegment inputSpawnerRouteSegment in inputSpawnerRoute)
            {
                int segmentIndex = Array.FindIndex(inputSegments, s => s == inputSpawnerRouteSegment);
                if (segmentIndex >= 0)
                {
                    for (int i = 0; i < LayersMax; ++i)
                    {
                        if (segmentsMatrix[segmentIndex, i] != null)
                        {
                            route.Add(segmentsMatrix[segmentIndex, i]);
                        }
                    }
                }
                else
                {
                    route.Add(despawnerSegment);
                }
            }

            return route;
        }

        private static IEnumerable<IEnumerable<RightHandRuleIntersectionSegment>> GenerateSidesClockwise(RegularRoadSegment[,] segmentsMatrix, RegularRoadSegment[] inputSegments, List<HashSet<RightHandRuleIntersectionSegment>> inputSidesClockwise)
        {
            List<HashSet<RightHandRuleIntersectionSegment>> sidesClockwise = new List<HashSet<RightHandRuleIntersectionSegment>>();
            foreach (HashSet<RightHandRuleIntersectionSegment> inputSide in inputSidesClockwise)
            {
                HashSet<RightHandRuleIntersectionSegment> side = new HashSet<RightHandRuleIntersectionSegment>();
                foreach (RightHandRuleIntersectionSegment inputSegment in inputSide)
                {
                    int segmentIndex = Array.FindIndex(inputSegments, s => s == inputSegment);
                    for (int i = 0; i < LayersMax; ++i)
                    {
                        if (segmentsMatrix[segmentIndex, i] != null)
                        {
                            side.Add(segmentsMatrix[segmentIndex, i] as RightHandRuleIntersectionSegment);
                        }
                    }
                }
                sidesClockwise.Add(side);
            }

            return sidesClockwise;
        }

        private static IEnumerable<TrafficLightsPhase> GenerateTrafficLightsPhases(RegularRoadSegment[,] segmentsMatrix, RegularRoadSegment[] inputSegments, List<TrafficLightsPhase> inputPhases)
        {
            List<TrafficLightsPhase> phases = new List<TrafficLightsPhase>();
            foreach (TrafficLightsPhase inputPhase in inputPhases)
            {
                List<IRoadSegment> openSegments = new List<IRoadSegment>();
                foreach (IRoadSegment segment in inputPhase.OpenSegments)
                {
                    int segmentIndex = Array.FindIndex(inputSegments, s => s == segment);
                    for (int i = 0; i < LayersMax; ++i)
                    {
                        if (segmentsMatrix[segmentIndex, i] != null)
                        {
                            openSegments.Add(segmentsMatrix[segmentIndex, i]);
                        }
                    }
                }
                phases.Add(new TrafficLightsPhase
                {
                    PhaseLengthSeconds = inputPhase.PhaseLengthSeconds,
                    OpenSegments = openSegments
                });
            }

            return phases;
        }

        private static IEnumerable<(T prioritySegment, T yieldingSegment)> GenerateCrossings<T>(RegularRoadSegment[,] segmentsMatrix, RegularRoadSegment[] inputSegments, HashSet<(T prioritySegment, T yieldingSegment)> inputCrossings) where T: RightOfWayIntersectionSegment
        {
            HashSet<(T prioritySegment, T yieldingSegment)> crossings = new HashSet<(T prioritySegment, T yieldingSegment)>();
            foreach ((T prioritySegment, T yieldingSegment) in inputCrossings)
            {
                int prioritySegmentIndex = Array.FindIndex(inputSegments, s => s == prioritySegment);
                int yieldingSegmentIndex = Array.FindIndex(inputSegments, s => s == yieldingSegment);
                for (int i = 0; i < LayersMax; ++i)
                {
                    for (int j = 0; j < LayersMax; ++j)
                    {
                        if (segmentsMatrix[prioritySegmentIndex, i] != null && segmentsMatrix[yieldingSegmentIndex, j] != null)
                        {
                            crossings.Add((prioritySegment: segmentsMatrix[prioritySegmentIndex, i] as T,
                                yieldingSegment: segmentsMatrix[yieldingSegmentIndex, j] as T));
                        }
                    }
                }
            }

            return crossings;
        }

        private static void ConnectAdjacentSegments(RegularRoadSegment[,] segmentsMatrix, RegularRoadSegment[] inputSegments)
        {
            for (int segment1Index = 0; segment1Index < inputSegments.Length; ++segment1Index)
            {
                foreach (AdjacentLaneInfo ali in inputSegments[segment1Index].AdjacentLanes)
                {
                    int segment2Index = Array.FindIndex(inputSegments, s => s == ali.Lane);
                    for (int i = 0; i < LayersMax; ++i)
                    {
                        if (segmentsMatrix[segment1Index, i] != null && segmentsMatrix[segment2Index, i] != null)
                        {
                            segmentsMatrix[segment1Index, i].AdjacentLanes.Add(new AdjacentLaneInfo
                            {
                                Bias = ali.Bias,
                                Lane = segmentsMatrix[segment2Index, i]
                            });
                        }
                    }
                }
            }
        }

        private static void ConnectConsecutiveSegments(RegularRoadSegment[,] segmentsMatrix, RegularRoadSegment[] inputSegments, DespawnerSegment despawner)
        {
            for (int segment1Index = 0; segment1Index < inputSegments.Length; ++segment1Index)
            {
                foreach (NextRoadSegmentInfo nrsi in inputSegments[segment1Index].Next)
                {
                    int segment2Index = Array.FindIndex(inputSegments, s => s == nrsi.Segment);
                    if (segment2Index >= 0)
                    {
                        for (int i = 0; i < LayersMax; ++i)
                        {
                            if (segmentsMatrix[segment1Index, i] != null && segmentsMatrix[segment2Index, i] != null)
                            {
                                segmentsMatrix[segment1Index, i].AddNext(segmentsMatrix[segment2Index, i],
                                    nrsi.Probability, nrsi.Direction);
                            }
                        }
                    }
                    else if (nrsi.Segment is DespawnerSegment)
                    {
                        for (int i = 0; i < LayersMax; ++i)
                        {
                            if (segmentsMatrix[segment1Index, i] != null)
                            {
                                segmentsMatrix[segment1Index, i].AddNext(despawner, nrsi.Probability, nrsi.Direction);
                            }
                        }
                    }
                }
            }
        }

        private static void ConnectSegmentsLayers(RegularRoadSegment[,] segmentsMatrix)
        {
            for (int i = 0; i < segmentsMatrix.Length / LayersMax; ++i)
            {
                for (int j = 0; j < LayersMax - 1; ++j)
                {
                    if (segmentsMatrix[i, j] != null && segmentsMatrix[i, j + 1] != null &&
                        !(segmentsMatrix[i, j] is RightOfWayIntersectionSegment) &&
                        !(segmentsMatrix[i, j + 1] is RightOfWayIntersectionSegment))
                    {
                        segmentsMatrix[i, j].AdjacentLanes.Add(new AdjacentLaneInfo
                            {Lane = segmentsMatrix[i, j + 1], Bias = 0});
                        segmentsMatrix[i, j + 1].AdjacentLanes.Add(new AdjacentLaneInfo
                            {Lane = segmentsMatrix[i, j], Bias = 0});
                    }
                }
            }
        }

        private static void ConnectSpawnersToSegments(List<SpawnerSegment> spawners, IEnumerable<SpawnerSegment> inputSpawners, RegularRoadSegment[,] segmentsMatrix, RegularRoadSegment[] inputSegments)
        {
            int i = 0;
            foreach (SpawnerSegment inputSpawner in inputSpawners)
            {
                SpawnerSegment spawner = spawners[i];

                foreach (NextRoadSegmentInfo nrsi in inputSpawner.Next)
                {
                    int segment2Index = Array.FindIndex(inputSegments, s => s == nrsi.Segment);
                    if (segment2Index >= 0)
                    {
                        for (int j = 0; j < LayersMax; ++j)
                        {
                            if (segmentsMatrix[segment2Index, j] != null)
                            {
                                spawner.AddNext(segmentsMatrix[segment2Index, j], nrsi.Probability, nrsi.Direction);
                            }
                        }
                    }
                }

                ++i;
            }
        }

        private static IEnumerable<Vector> OffsetVertices(RegularRoadSegment segment, double distance)
        {
            if (distance == 0)
            {
                return segment.Vertices;
            }

            List<Vector> newVertices = new List<Vector>();
            for (int i = 0; i < segment.Vertices.Count; ++i)
            {
                Vector vertex = segment.Vertices[i];
                Vector nextVertex = i + 1 < segment.Vertices.Count
                    ? segment.Vertices[i + 1]
                    : 2 * segment.Vertices[i] - segment.Vertices[i - 1];
                Vector vector = nextVertex - vertex;
                if (vector.Length == 0)
                {
                    newVertices.Add(vertex);
                    break;
                }
                vector /= vector.Length;
                Vector rotatedUnitVector = new Vector(vector.Y, -vector.X);
                newVertices.Add(vertex - rotatedUnitVector * distance);
            }

            return newVertices;
        }
    }
}