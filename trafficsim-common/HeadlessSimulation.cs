﻿using System;
using trafficsim.Simulation;

namespace trafficsim
{
    public class HeadlessSimulation
    {
        public static void Run(Scenario scenario, double limit, Action<int, Scenario> everyMinuteCallback = null)
        {
            int i = 0;
            while (scenario.TimePassed < limit)
            {
                scenario.DoSimulationTick(1/60.0);
                ++i;

                if (everyMinuteCallback != null && i % 3600 == 0)
                {
                    everyMinuteCallback(i / 3600, scenario);
                }
            }
        }
    }
}