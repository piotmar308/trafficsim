﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using trafficsim.Simulation;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Vehicle;

namespace trafficsim
{
    /// <summary>
    /// Class responsible for managing the GUI window.
    /// </summary>
    public class SimulationViewer
    {
        /// <summary>
        /// Reference to the SFML window object.
        /// </summary>
        private readonly RenderWindow _window;

        private VideoMode _vm = new VideoMode(600, 600);

        private float _viewRadius;

        private Vector2f _center = new Vector2f(0, 0);

        private bool _dragging = false;

        private Vector2i _lastMousePos = new Vector2i(0, 0);

        public static void Run(Scenario scenario, Action<int, Scenario> everyMinuteCallback = null)
        {
            new SimulationViewer(scenario, everyMinuteCallback);
        }

        /// <summary>
        /// Constructing the class causes the GUI window to open, set all its properties, bind all necessary events,
        /// start handling requests to change simulation speed and regularly running simulation ticks.
        /// </summary>
        /// <param name="scenario">The simulated scenario object.</param>
        private SimulationViewer(Scenario scenario, Action<int, Scenario> everyMinuteCallback)
        {
            _window = new RenderWindow(_vm, "Traffic simulator");
            _window.SetFramerateLimit(60);
            _viewRadius = (float) scenario.ViewRadius;
            ReadjustView();

            _window.Closed += (sender, e) => _window.Close();
            _window.Resized += (sender, e) =>
            {
                _vm.Width = e.Width;
                _vm.Height = e.Height;
                ReadjustView();
            };
            _window.KeyPressed += (sender, e) =>
            {
                if (e.Code == Keyboard.Key.Left || e.Code == Keyboard.Key.Right)
                {
                    scenario.TimeWarp = Math.Max(0,
                        scenario.TimeWarp + 1.0 * (e.Shift ? 1.0 : 0.1) * (e.Code == Keyboard.Key.Left ? -1 : 1));
                    Console.WriteLine("Simulation speed changed to " + scenario.TimeWarp.ToString("0.0"));
                }
            };
            _window.MouseButtonPressed += (sender, e) =>
            {
                if (e.Button == Mouse.Button.Left)
                {
                    _lastMousePos = new Vector2i(e.X, e.Y);
                    _dragging = true;
                }
                if (e.Button == Mouse.Button.Right)
                {
                    Vector2f _coords = _window.MapPixelToCoords(new Vector2i(e.X, e.Y));
                    Vector coords = new Vector(_coords.X, _coords.Y);
                    IEnumerable<Agent> allAgents = scenario.RoadSegments.Select(s =>
                            s is RegularRoadSegment ? (s as RegularRoadSegment).GetAgents() : new Agent[] { })
                        .SelectMany(a => a);
                    Agent closestAgent = null;
                    double closestAgentDistance = double.MaxValue;
                    foreach (Agent agent in allAgents)
                    {
                        agent.Selected = false;
                        if (closestAgent == null || (agent.GetXYPosition() - coords).Length < closestAgentDistance)
                        {
                            closestAgent = agent;
                            closestAgentDistance = (agent.GetXYPosition() - coords).Length;
                        }
                    }

                    if (closestAgent != null && closestAgentDistance < 2)
                    {
                        closestAgent.Selected = true;
                    }
                }
            };
            _window.MouseButtonReleased += (sender, e) =>
            {
                if (e.Button == Mouse.Button.Left)
                {
                    _dragging = false;
                }
            };
            _window.MouseMoved += (sender, e) => HandleMouseMove(e);
            _window.MouseWheelScrolled += (sender, e) => HandleWheelScrolled(e);

            while (_window.IsOpen)
            {
                _window.DispatchEvents();
                int oldMinutes = (int) (scenario.TimePassed / 60.0);
                scenario.DoSimulationTick(1.0/60.0);
                int newMinutes = (int) (scenario.TimePassed / 60.0);
                if (everyMinuteCallback != null && newMinutes > oldMinutes)
                {
                    everyMinuteCallback(newMinutes, scenario);
                }
                _window.Clear(Color.White);
                scenario.Draw(_window);
                _window.Display();
            }
        }

        /// <summary>
        /// Method adjusts the view in order to fit a radius specified in the scenario inside the bounds of the window.
        /// </summary>
        private void ReadjustView()
        {
            View view = _vm.Width < _vm.Height
                ? new View(_center, new Vector2f(_viewRadius * 2, _viewRadius * 2 * _vm.Height / _vm.Width))
                : new View(_center, new Vector2f(_viewRadius * 2 * _vm.Width / _vm.Height, _viewRadius * 2));
            _window.SetView(view);
        }

        private float GetPixelsPerMeter()
        {
            return Math.Min(_vm.Width, _vm.Height) / (_viewRadius * 2);
        }

        private void HandleMouseMove(MouseMoveEventArgs e)
        {
            if (_dragging)
            {
                _center.X -= (e.X - _lastMousePos.X) / GetPixelsPerMeter();
                _center.Y -= (e.Y - _lastMousePos.Y) / GetPixelsPerMeter();
                ReadjustView();
                _lastMousePos = new Vector2i(e.X, e.Y);
            }
        }

        private void HandleWheelScrolled(MouseWheelScrollEventArgs e)
        {
            Vector2i mousePixelOffsetFromScreenCenter = new Vector2i((int) (e.X - _vm.Width / 2), (int) (e.Y - _vm.Height / 2));
            float previousPPM = GetPixelsPerMeter();
            _viewRadius /= (float) Math.Exp(e.Delta / 5);
            if (_viewRadius < 10) _viewRadius = 10;
            _center.X += mousePixelOffsetFromScreenCenter.X * (1 / previousPPM - 1 / GetPixelsPerMeter());
            _center.Y += mousePixelOffsetFromScreenCenter.Y * (1 / previousPPM - 1 / GetPixelsPerMeter());
            ReadjustView();
        }
    }
}