﻿using System;
using NUnit.Framework;
using trafficsim.Simulation.Model;
using trafficsim.Simulation.Struct;

namespace trafficsim.Tests
{
    [TestFixture]
    public class IntelligentDriverModelTest
    {
        [Test]
        public void TestAcceleration()
        {
            ICarFollowingModel carFollowingModel = new IntelligentDriverModel()
            {
                SafeTimeHeadway = 2,
                MaximumAcceleration = 1,
                ComfortableDeceleration = 3,
                AccelerationExponent = 5,
                MinimumDistance = 4
            };
            double acceleration = carFollowingModel.GetAcceleration(10,
                new ObstacleInfo {DistanceToObstacle = 15, ObstacleForwardVelocity = 8}, 20);
            Assert.AreEqual(1 - Math.Pow(10.0 / 20.0, 5) - Math.Pow((24 + 20 / (2 * Math.Sqrt(3))) / 15, 2), acceleration);
        }
    }
}