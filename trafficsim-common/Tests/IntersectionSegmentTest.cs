﻿using System.Windows;
using NUnit.Framework;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Struct;

namespace trafficsim.Tests
{
    [TestFixture]
    public class IntersectionSegmentTest
    {
        [Test]
        public void TestObstacleWhenOpen()
        {
            RightOfWayIntersectionSegment segment = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, 0),
                new Vector(5, 0)
            });
            segment.Closed = false;
            ObstacleSeekInfo info = segment.SeekObstacleForwards(0, null);
            Assert.AreEqual(5, info.Distance);
            Assert.IsNull(info.Agent);
        }

        [Test]
        public void TestObstacleWhenClosed()
        {
            RightOfWayIntersectionSegment segment = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, 0),
                new Vector(5, 0)
            });
            segment.Closed = true;
            ObstacleSeekInfo info = segment.SeekObstacleForwards(0, null);
            Assert.AreEqual(0, info.Distance);
            Assert.IsNull(info.Agent);
        }
    }
}