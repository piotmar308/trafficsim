﻿using NUnit.Framework;
using trafficsim.Simulation.Model;
using trafficsim.Simulation.Struct;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Tests
{
    [TestFixture]
    public class VehicleTest
    {
        [Test]
        public void TestChangingAccelerationWithoutAgent()
        {
            Agent agent = new Agent(null, null)
            {
                Position = 2,
                Velocity = 3,
                Acceleration = 4
            };
            agent.UpdateAcceleration(new ObstacleInfo { DistanceToObstacle = 5, ObstacleForwardVelocity = 6 }, 10);
            Assert.AreEqual(4, agent.Acceleration);
        }

        [Test]
        public void TestChangingAccelerationWithAgent()
        {
            Agent agent = new Agent(new TestCarFollowingModelForVehicleTest(1), null)
            {
                Position = 2,
                Velocity = 3,
                Acceleration = 4
            };
            agent.UpdateAcceleration(new ObstacleInfo { DistanceToObstacle = 5, ObstacleForwardVelocity = 6 }, 10);
            Assert.AreEqual(1, agent.Acceleration);
        }

        [Test]
        public void TestChangingVelocity()
        {
            Agent agent = new Agent(null, null)
            {
                Position = 2,
                Velocity = 3,
                Acceleration = 4
            };
            agent.UpdateVelocity(2);
            Assert.AreEqual(11, agent.Velocity);
        }

        [Test]
        public void TestChangingPosition()
        {
            Agent agent = new Agent(null, null)
            {
                Position = 2,
                Velocity = 3,
                Acceleration = 4
            };
            agent.UpdatePosition(2);
            Assert.AreEqual(8, agent.Position);
        }
    }

    internal class TestCarFollowingModelForVehicleTest: ICarFollowingModel
    {
        private double Acceleration;

        public TestCarFollowingModelForVehicleTest(double acceleration)
        {
            Acceleration = acceleration;
        }

        public double GetAcceleration(double vehicleForwardVelocity, ObstacleInfo obstacleInfo, double speedLimit)
        {
            return Acceleration;
        }
    }
}