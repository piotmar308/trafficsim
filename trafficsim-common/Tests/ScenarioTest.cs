﻿using System;
using System.Collections.Generic;
using System.Windows;
using NUnit.Framework;
using SFML.Graphics;
using trafficsim.Simulation;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Struct;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Tests
{
    [TestFixture]
    public class ScenarioTest
    {
        [Test]
        [TestCase(1)]
        [TestCase(0.1)]
        [TestCase(0.2)]
        [TestCase(0.5)]
        [TestCase(1.1)]
        [TestCase(1.5)]
        [TestCase(1.9)]
        [TestCase(2)]
        [TestCase(2.1)]
        [TestCase(2.9)]
        [TestCase(5)]
        [TestCase(5.1)]
        [TestCase(5.9)]
        public void TestTimeWarpValue(double timeWarp)
        {
            TestRoadSegmentForScenarioTest segment = new TestRoadSegmentForScenarioTest {ExpectSmallTimeDiffs = timeWarp < 1};
            Scenario scenario = new Scenario(new[] {segment}) {TimeWarp = timeWarp};
            for (int i = 0; i < 100; ++i)
            {
                scenario.DoSimulationTick(1.0/60.0);
            }
            Assert.That(segment.CumulativeTimeDiff, Is.EqualTo(100.0 / 60.0 * timeWarp).Within(0.0000000000001));
        }

        [Test]
        public void TestTimeWarpValueChecking()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Scenario(new IRoadSegment[] {}) {TimeWarp = -0.1});
        }
    }

    internal class TestRoadSegmentForScenarioTest: IRoadSegment
    {
        public double CumulativeTimeDiff { get; private set; } = 0;
        public bool ExpectSmallTimeDiffs { get; set; } = true;

        public void AddNext(IRoadSegment next, double probability = 1, TurnDirection direction = TurnDirection.Forward) {}
        public void AddPrevious(IRoadSegment previous) {}
        public IRoadSegment GetRandomNext(ICollection<IRoadSegment> intersectNextSegments = null) { return null; }
        public void SimulateAccelerationChanges() {}

        public void UpdateAgentsPositionsAndVelocities(double timeDiff)
        {
            if (!ExpectSmallTimeDiffs)
            {
                Assert.GreaterOrEqual(timeDiff, 1.0/60.0);
            }
            Assert.Less(timeDiff, 2.0/60.0);
            CumulativeTimeDiff += timeDiff;
        }

        public void SimulateLaneChanges() {}
        public void MoveAgentsToRespectiveSegments() {}
        public ObstacleSeekInfo SeekObstacleForwards(double position, Agent agentWhoWantsIt, double addDistance = 0, int limit = 99) { return new ObstacleSeekInfo(); }
        public ObstacleSeekInfo SeekObstacleBackwards(double position, double addDistance = 0, int limit = 99) { return new ObstacleSeekInfo(); }
        public ObstacleSeekInfo SeekObstacleFromEnd(double addDistance = 0, int limit = 99) { return new ObstacleSeekInfo(); }
        public void DrawRoad(RenderWindow window) {}
        public void DrawAgents(RenderWindow window) {}
        public void InsertAgent(Agent agent) {}
        public Vector LongitudinalPositionToXY(double position) { return new Vector(); }
        public double GetLength() { return 0; }
    }
}