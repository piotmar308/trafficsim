﻿using System.Collections.Generic;
using System.Windows;
using NUnit.Framework;
using trafficsim.Simulation;
using trafficsim.Simulation.Factory;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Tests
{
    [TestFixture]
    public class SpawnerSegmentTest
    {
        [Test]
        public void TestTimeChecking()
        {
            SpawnerSegment spawner = new SpawnerSegment(new TestVehicleFactory()) { SpawnFrequency = 0.1 };
            RegularRoadSegment segment = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(100, 0)
            });
            spawner.AddNext(segment);
            Scenario scenario = new Scenario(new IRoadSegment[] { spawner, segment });
            scenario.DoSimulationTick(5);
            Assert.AreEqual(0, segment.GetAgents().Length);
            scenario.DoSimulationTick(10);
            Assert.AreEqual(1, segment.GetAgents().Length);
        }

        [Test]
        public void TestSpaceChecking()
        {
            SpawnerSegment spawner = new SpawnerSegment(new TestVehicleFactory()) { SpawnFrequency = 10 };
            RegularRoadSegment segment = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(100, 0)
            });
            spawner.AddNext(segment);
            Agent obstacleAgent = new Agent(null, null) { Position = 4, Length = 5 };
            segment.InsertAgent(obstacleAgent);
            Scenario scenario = new Scenario(new IRoadSegment[] { spawner, segment });
            scenario.DoSimulationTick(1);
            Assert.AreEqual(1, segment.GetAgents().Length);
            obstacleAgent.Position = 6;
            scenario.DoSimulationTick(1);
            Assert.AreEqual(2, segment.GetAgents().Length);
        }

        [Test]
        public void TestSafetyChecking()
        {
            SpawnerSegment spawner1 = new SpawnerSegment(new TestVehicleFactory {Acceleration = -2}) { SpawnFrequency = 10 };
            SpawnerSegment spawner2 = new SpawnerSegment(new TestVehicleFactory {Acceleration = 0}) { SpawnFrequency = 10 };
            RegularRoadSegment segment = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(100, 0)
            });
            spawner1.AddNext(segment);
            spawner2.AddNext(segment);
            spawner1.UpdateAgentsPositionsAndVelocities(1);
            spawner1.MoveAgentsToRespectiveSegments();
            Assert.AreEqual(0, segment.GetAgents().Length);
            spawner2.UpdateAgentsPositionsAndVelocities(1);
            spawner2.MoveAgentsToRespectiveSegments();
            Assert.AreEqual(1, segment.GetAgents().Length);
        }

        [Test]
        public void TestRouting()
        {
            SpawnerSegment spawner = new SpawnerSegment(new TestVehicleFactory()) { SpawnFrequency = 10 };
            RegularRoadSegment segment1 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(1, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(0, 1)
            });
            spawner.AddNext(segment1, 0);
            spawner.AddNext(segment2, 1);
            spawner.Route = new List<IRoadSegment>(new[] {segment1});
            spawner.UpdateAgentsPositionsAndVelocities(1);
            spawner.MoveAgentsToRespectiveSegments();
            Assert.AreEqual(1, segment1.GetAgents().Length);
            Assert.AreEqual(0, segment2.GetAgents().Length);
        }
    }

    internal class TestVehicleFactory : IVehicleFactory
    {
        public double Acceleration { get; set; } = 0;
        public double SafeDecelerationThreshold { get; set; } = 1;

        public Agent GetNewAgent()
        {
            return new Agent(null, null)
            {
                Acceleration = Acceleration,
                SafeDecelerationThreshold = SafeDecelerationThreshold
            };
        }

        public void ResetRNG()
        {
        }
    }
}