﻿using System.Collections.Generic;
using System.Windows;
using NUnit.Framework;
using trafficsim.Simulation;
using trafficsim.Simulation.Model;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Struct;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Tests
{
    [TestFixture]
    public class RegularRoadSegmentTest
    {
        private Vector[][] TestVertices = new[]
        {
            null,
            new Vector[] {},
            new Vector[] { new Vector(1, 1) },
            new Vector[] { new Vector(1, 1), new Vector(1, 2) },
            new Vector[] { new Vector(1, 1), new Vector(1, 0), new Vector(3, 0) }
        };

        private double[][] ExpectedSubSegmentLengths = new[]
        {
            new double[] {},
            new double[] {},
            new double[] {},
            new double[] { 1 },
            new double[] { 1, 2 }
        };

        [Test]
        [TestCase(0, 0)]
        [TestCase(1, 0)]
        [TestCase(2, 0)]
        [TestCase(3, 1)]
        [TestCase(4, 3)]
        public void TestTotalLengthCalculation(int verticesArrayIndex, int expectedLength)
        {
            RegularRoadSegment segment = new RegularRoadSegment(TestVertices[verticesArrayIndex]);
            Assert.AreEqual(expectedLength, segment.Length);
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void TestSubSegmentLengthsCalculation(int verticesArrayIndex)
        {
            RegularRoadSegment segment = new RegularRoadSegment(TestVertices[verticesArrayIndex]);
            Assert.That(segment.SubSegmentLengths, Is.EquivalentTo(ExpectedSubSegmentLengths[verticesArrayIndex]));
        }

        [Test]
        public void TestVehicleInsertionOrder()
        {
            RegularRoadSegment segment = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(6, 0)
            });
            segment.InsertAgent(new TestAgentForRegularRoadSegmentTest(1, 0) { Position = 2, Length = 0 });
            segment.InsertAgent(new TestAgentForRegularRoadSegmentTest(1, 0) { Position = 4, Length = 0 });
            segment.InsertAgent(new TestAgentForRegularRoadSegmentTest(1, 0) { Position = 1, Length = 0 });
            segment.InsertAgent(new TestAgentForRegularRoadSegmentTest(1, 0) { Position = 3, Length = 0 });
            segment.InsertAgent(new TestAgentForRegularRoadSegmentTest(1, 0) { Position = 5, Length = 0 });
            segment.SimulateAccelerationChanges();
        }

        [Test]
        public void TestObstacleCalculationWithVehicleOnTheSameSegment()
        {
            RegularRoadSegment segment = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(5, 0)
            });
            segment.InsertAgent(new TestAgentForRegularRoadSegmentTest(2, 5) { Position = 1, Length = 1, Velocity = 5});
            segment.InsertAgent(new TestAgentForRegularRoadSegmentTest(1, 0) { Position = 4, Length = 1, Velocity = 5 });
            segment.SimulateAccelerationChanges();
        }

        [Test]
        public void TestObstacleCalculationWithVehicleOnTheNextSegment()
        {
            RegularRoadSegment segment1 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(5, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new[]
            {
                new Vector(5, 0),
                new Vector(10, 0)
            });
            segment1.AddNext(segment2);
            segment1.InsertAgent(new TestAgentForRegularRoadSegmentTest(7, 5) { Position = 1, Length = 1, Velocity = 5});
            segment2.InsertAgent(new TestAgentForRegularRoadSegmentTest(1, 0) { Position = 4, Length = 1, Velocity = 5 });
            segment1.SimulateAccelerationChanges();
            segment2.SimulateAccelerationChanges();
        }

        [Test]
        public void TestObstacleCalculationWithVehicleOnTheSegmentAfterTheNext()
        {
            RegularRoadSegment segment1 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(5, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new[]
            {
                new Vector(5, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment segment3 = new RegularRoadSegment(new[]
            {
                new Vector(10, 0),
                new Vector(15, 0)
            });
            segment1.AddNext(segment2);
            segment2.AddNext(segment3);
            segment1.InsertAgent(new TestAgentForRegularRoadSegmentTest(12, 5) { Position = 1, Length = 1, Velocity = 5});
            segment3.InsertAgent(new TestAgentForRegularRoadSegmentTest(1, 0) { Position = 4, Length = 1, Velocity = 5 });
            segment1.SimulateAccelerationChanges();
            segment2.SimulateAccelerationChanges();
            segment3.SimulateAccelerationChanges();
        }

        [Test]
        public void TestMovingVehiclesToNextSegment()
        {
            RegularRoadSegment segment1 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(5, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new[]
            {
                new Vector(5, 0),
                new Vector(10, 0)
            });
            segment1.AddNext(segment2);
            Agent vehicle1 = new Agent(null, null) { Position = 1, Velocity = 3};
            Agent vehicle2 = new Agent(null, null) { Position = 3, Velocity = 3};
            Agent vehicle3 = new Agent(null, null) { Position = 4, Velocity = 3};
            segment1.InsertAgent(vehicle1);
            segment1.InsertAgent(vehicle2);
            segment1.InsertAgent(vehicle3);
            Scenario scenario = new Scenario(new[] { segment1, segment2 }, null);
            scenario.DoSimulationTick(1);
            Assert.AreEqual(4, vehicle1.Position);
            Assert.AreEqual(1, vehicle2.Position);
            Assert.AreEqual(2, vehicle3.Position);
            Assert.That(segment1.GetAgents(), Is.EquivalentTo(new[] { vehicle1 }));
            Assert.That(segment2.GetAgents(), Is.EquivalentTo(new[] { vehicle2, vehicle3 }));
        }

        [Test]
        public void TestVehicleSeekingFromPositionForwards()
        {
            TestRegularRoadSegment segment1 = new TestRegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(5, 0)
            });
            TestRegularRoadSegment segment2 = new TestRegularRoadSegment(new[]
            {
                new Vector(5, 0),
                new Vector(10, 0)
            });
            TestRegularRoadSegment segment3 = new TestRegularRoadSegment(new[]
            {
                new Vector(10, 0),
                new Vector(15, 0)
            });
            TestRegularRoadSegment segment4 = new TestRegularRoadSegment(new[]
            {
                new Vector(10, 0),
                new Vector(10, 5)
            });
            Agent vehicle1 = new Agent(null, null)
            {
                Position = 3,
                Length = 4
            };
            Agent vehicle2 = new Agent(null, null)
            {
                Position = 4,
                Length = 4
            };
            ObstacleSeekInfo info;
            info = segment1.SeekObstacleForwards(2, null);
            Assert.AreEqual(3, info.Distance);
            Assert.AreEqual(null, info.Agent);
            segment1.InsertAgent(vehicle1);
            segment1.InsertAgent(vehicle2);
            info = segment1.SeekObstacleForwards(2, null);
            Assert.AreEqual(-3, info.Distance);
            Assert.AreEqual(vehicle1, info.Agent);
            segment1.RemoveAllVehicles();
            segment1.AddNext(segment2);
            info = segment1.SeekObstacleForwards(2, null);
            Assert.AreEqual(8, info.Distance);
            Assert.AreEqual(null, info.Agent);
            segment2.InsertAgent(vehicle1);
            segment2.InsertAgent(vehicle2);
            info = segment1.SeekObstacleForwards(2, null);
            Assert.AreEqual(2, info.Distance);
            Assert.AreEqual(vehicle1, info.Agent);
            segment2.RemoveAllVehicles();
            segment2.AddNext(segment3);
            info = segment1.SeekObstacleForwards(2, null);
            Assert.AreEqual(13, info.Distance);
            Assert.AreEqual(null, info.Agent);
            segment3.InsertAgent(vehicle1);
            segment3.InsertAgent(vehicle2);
            info = segment1.SeekObstacleForwards(2, null);
            Assert.AreEqual(7, info.Distance);
            Assert.AreEqual(vehicle1, info.Agent);
            segment2.AddNext(segment4);
            info = segment1.SeekObstacleForwards(2, null);
            Assert.AreEqual(8, info.Distance);
            Assert.AreEqual(null, info.Agent);
        }

        [Test]
        public void TestVehicleSeekingFromPositionBackwards()
        {
            TestRegularRoadSegment segment1 = new TestRegularRoadSegment(new[]
            {
                new Vector(-5, 0),
                new Vector(0, 0)
            });
            TestRegularRoadSegment segment2 = new TestRegularRoadSegment(new[]
            {
                new Vector(-10, 0),
                new Vector(-5, 0)
            });
            TestRegularRoadSegment segment3 = new TestRegularRoadSegment(new[]
            {
                new Vector(-15, 0),
                new Vector(-10, 0)
            });
            TestRegularRoadSegment segment4 = new TestRegularRoadSegment(new[]
            {
                new Vector(-10, -5),
                new Vector(-10, 0)
            });
            Agent vehicle1 = new Agent(null, null)
            {
                Position = 1,
                Length = 4
            };
            Agent vehicle2 = new Agent(null, null)
            {
                Position = 2,
                Length = 4
            };
            ObstacleSeekInfo info;
            info = segment1.SeekObstacleBackwards(4);
            Assert.AreEqual(4, info.Distance);
            Assert.AreEqual(null, info.Agent);
            segment1.InsertAgent(vehicle1);
            segment1.InsertAgent(vehicle2);
            info = segment1.SeekObstacleBackwards(4);
            Assert.AreEqual(2, info.Distance);
            Assert.AreEqual(vehicle2, info.Agent);
            segment1.RemoveAllVehicles();
            segment2.AddNext(segment1);
            info = segment1.SeekObstacleBackwards(4);
            Assert.AreEqual(9, info.Distance);
            Assert.AreEqual(null, info.Agent);
            segment2.InsertAgent(vehicle1);
            segment2.InsertAgent(vehicle2);
            info = segment1.SeekObstacleBackwards(4);
            Assert.AreEqual(7, info.Distance);
            Assert.AreEqual(vehicle2, info.Agent);
            segment2.RemoveAllVehicles();
            segment3.AddNext(segment2);
            info = segment1.SeekObstacleBackwards(4);
            Assert.AreEqual(14, info.Distance);
            Assert.AreEqual(null, info.Agent);
            segment3.InsertAgent(vehicle1);
            segment3.InsertAgent(vehicle2);
            info = segment1.SeekObstacleBackwards(4);
            Assert.AreEqual(12, info.Distance);
            Assert.AreEqual(vehicle2, info.Agent);
            segment4.AddNext(segment2);
            info = segment1.SeekObstacleBackwards(4);
            Assert.AreEqual(9, info.Distance);
            Assert.AreEqual(null, info.Agent);
        }

        [Test]
        public void TestVehicleSeekingFromIndexForwards()
        {
            TestRegularRoadSegment segment1 = new TestRegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(10, 0)
            });
            TestRegularRoadSegment segment2 = new TestRegularRoadSegment(new[]
            {
                new Vector(10, 0),
                new Vector(12, 0)
            });
            Agent vehicle1 = new Agent(null, null)
            {
                Position = 1,
                Length = 5
            };
            Agent vehicle2 = new Agent(null, null)
            {
                Position = 2,
                Length = 3
            };
            Agent vehicle3 = new Agent(null, null)
            {
                Position = 6,
                Length = 2
            };
            segment1.InsertAgent(vehicle1);
            segment1.InsertAgent(vehicle2);
            segment1.InsertAgent(vehicle3);
            ObstacleSeekInfo info;
            info = segment1.SeekObstacleFromAgent(vehicle1, false);
            Assert.AreEqual(-2, info.Distance);
            Assert.AreEqual(vehicle2, info.Agent);
            info = segment1.SeekObstacleFromAgent(vehicle2, false);
            Assert.AreEqual(2, info.Distance);
            Assert.AreEqual(vehicle3, info.Agent);
            info = segment1.SeekObstacleFromAgent(vehicle3, false);
            Assert.AreEqual(4, info.Distance);
            Assert.IsNull(info.Agent);
            segment1.AddNext(segment2);
            info = segment1.SeekObstacleFromAgent(vehicle3, false);
            Assert.AreEqual(6, info.Distance);
            Assert.IsNull(info.Agent);
        }

        [Test]
        public void TestVehicleSeekingFromIndexBackwards()
        {
            TestRegularRoadSegment segment1 = new TestRegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(10, 0)
            });
            TestRegularRoadSegment segment2 = new TestRegularRoadSegment(new[]
            {
                new Vector(-2, 0),
                new Vector(0, 0)
            });
            Agent vehicle1 = new Agent(null, null)
            {
                Position = 1,
                Length = 5
            };
            Agent vehicle2 = new Agent(null, null)
            {
                Position = 3,
                Length = 3
            };
            Agent vehicle3 = new Agent(null, null)
            {
                Position = 7,
                Length = 2
            };
            segment1.InsertAgent(vehicle1);
            segment1.InsertAgent(vehicle2);
            segment1.InsertAgent(vehicle3);
            ObstacleSeekInfo info;
            info = segment1.SeekObstacleFromAgent(vehicle1, true);
            Assert.AreEqual(1, info.Distance);
            Assert.AreEqual(null, info.Agent);
            info = segment1.SeekObstacleFromAgent(vehicle2, true);
            Assert.AreEqual(2, info.Distance);
            Assert.AreEqual(vehicle1, info.Agent);
            info = segment1.SeekObstacleFromAgent(vehicle3, true);
            Assert.AreEqual(4, info.Distance);
            Assert.AreEqual(vehicle2, info.Agent);
            segment2.AddNext(segment1);
            info = segment1.SeekObstacleFromAgent(vehicle1, true);
            Assert.AreEqual(3, info.Distance);
            Assert.AreEqual(null, info.Agent);
        }

        [Test]
        [TestCase(15, 2)]
        [TestCase(13, 2)]
        [TestCase(10, 2)]
        [TestCase(15, 7)]
        public void TestLaneChangeImpossible(double adjacentVehiclePosition, double adjacentVehicleLength)
        {
            RegularRoadSegment segment1 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new[]
            {
                new Vector(0, 5),
                new Vector(20, 5)
            });
            segment1.AdjacentLanes.Add(new AdjacentLaneInfo {Bias = 0, Lane = segment2});
            Agent vehicle1 = new Agent(null, new TestLaneChangeModel(true))
            {
                Position = 7,
                Length = 5
            };
            segment1.InsertAgent(vehicle1);
            Agent vehicle2 = new Agent(null, null)
            {
                Position = adjacentVehiclePosition,
                Length = adjacentVehicleLength
            };
            segment2.InsertAgent(vehicle2);
            segment1.SimulateLaneChanges();
            Assert.That(segment1.GetAgents(), Is.EquivalentTo(new[] { vehicle1 }));
            Assert.That(segment2.GetAgents(), Is.EquivalentTo(new[] { vehicle2 }));
        }

        [Test]
        [TestCase(17)]
        [TestCase(8)]
        public void TestLaneChangePossible(double adjacentVehiclePosition)
        {
            RegularRoadSegment segment1 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new[]
            {
                new Vector(0, 5),
                new Vector(20, 5)
            });
            segment1.AdjacentLanes.Add(new AdjacentLaneInfo {Bias = 0, Lane = segment2});
            Agent vehicle1 = new Agent(null, new TestLaneChangeModel(true))
            {
                Position = 7,
                Length = 5
            };
            segment1.InsertAgent(vehicle1);
            Agent vehicle2 = new Agent(null, null)
            {
                Position = adjacentVehiclePosition,
                Length = 2
            };
            segment2.InsertAgent(vehicle2);
            segment1.SimulateLaneChanges();
            Assert.That(segment1.GetAgents(), Is.Empty);
            Assert.That(segment2.GetAgents(), Is.EquivalentTo(new[] { vehicle1, vehicle2 }));
        }

        [Test]
        public void TestLaneChangeUnsafe()
        {
            RegularRoadSegment segment1 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new[]
            {
                new Vector(0, 5),
                new Vector(10, 5)
            });
            segment1.AdjacentLanes.Add(new AdjacentLaneInfo {Bias = 0, Lane = segment2});
            Agent vehicle1 = new Agent(null, new TestLaneChangeModel(true))
            {
                Position = 7,
                Length = 5
            };
            segment1.InsertAgent(vehicle1);
            Agent vehicle2 = new Agent(null, null)
            {
                Position = 1,
                Length = 2,
                SafeDecelerationThreshold = 5,
                Acceleration = -6
            };
            segment2.InsertAgent(vehicle2);
            segment1.SimulateLaneChanges();
            Assert.That(segment1.GetAgents(), Is.EquivalentTo(new[] { vehicle1 }));
            Assert.That(segment2.GetAgents(), Is.EquivalentTo(new[] { vehicle2 }));
        }

        [Test]
        public void TestLaneChangeSafe()
        {
            RegularRoadSegment segment1 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new[]
            {
                new Vector(0, 5),
                new Vector(10, 5)
            });
            segment1.AdjacentLanes.Add(new AdjacentLaneInfo {Bias = 0, Lane = segment2});
            Agent vehicle1 = new Agent(null, new TestLaneChangeModel(true))
            {
                Position = 7,
                Length = 5
            };
            segment1.InsertAgent(vehicle1);
            Agent vehicle2 = new Agent(null, null)
            {
                Position = 1,
                Length = 2,
                SafeDecelerationThreshold = 5,
                Acceleration = -4
            };
            segment2.InsertAgent(vehicle2);
            segment1.SimulateLaneChanges();
            Assert.That(segment1.GetAgents(), Is.Empty);
            Assert.That(segment2.GetAgents(), Is.EquivalentTo(new[] { vehicle1, vehicle2 }));
        }

        [Test]
        public void TestLaneChangeDesired()
        {
            RegularRoadSegment segment1 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new[]
            {
                new Vector(0, 5),
                new Vector(10, 5)
            });
            segment1.AdjacentLanes.Add(new AdjacentLaneInfo {Bias = 0, Lane = segment2});
            Agent vehicle1 = new Agent(null, new TestLaneChangeModel(true))
            {
                Position = 7,
                Length = 5
            };
            segment1.InsertAgent(vehicle1);
            Agent vehicle2 = new Agent(null, null)
            {
                Position = 1,
                Length = 2
            };
            segment2.InsertAgent(vehicle2);
            segment1.SimulateLaneChanges();
            Assert.That(segment1.GetAgents(), Is.Empty);
            Assert.That(segment2.GetAgents(), Is.EquivalentTo(new[] { vehicle1, vehicle2 }));
        }

        [Test]
        public void TestLaneChangeUndesired()
        {
            RegularRoadSegment segment1 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new[]
            {
                new Vector(0, 5),
                new Vector(10, 5)
            });
            segment1.AdjacentLanes.Add(new AdjacentLaneInfo {Bias = 0, Lane = segment2});
            Agent vehicle1 = new Agent(null, new TestLaneChangeModel(false))
            {
                Position = 7,
                Length = 5
            };
            segment1.InsertAgent(vehicle1);
            Agent vehicle2 = new Agent(null, null)
            {
                Position = 1,
                Length = 2
            };
            segment2.InsertAgent(vehicle2);
            segment1.SimulateLaneChanges();
            Assert.That(segment1.GetAgents(), Is.EquivalentTo(new[] { vehicle1 }));
            Assert.That(segment2.GetAgents(), Is.EquivalentTo(new[] { vehicle2 }));
        }

        [Test]
        public void TestSeekingLimiting()
        {
            RegularRoadSegment segment1 = new RegularRoadSegment(new[]
            {
                new Vector(0, 0),
                new Vector(2, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new[]
            {
                new Vector(2, 0),
                new Vector(4, 0)
            });
            RegularRoadSegment segment3 = new RegularRoadSegment(new[]
            {
                new Vector(4, 0),
                new Vector(6, 0)
            });
            RegularRoadSegment segment4 = new RegularRoadSegment(new[]
            {
                new Vector(6, 0),
                new Vector(8, 0)
            });
            RegularRoadSegment segment5 = new RegularRoadSegment(new[]
            {
                new Vector(8, 0),
                new Vector(10, 0)
            });
            segment1.AddNext(segment2);
            segment2.AddNext(segment3);
            segment3.AddNext(segment4);
            segment4.AddNext(segment5);
            ObstacleSeekInfo info = segment1.SeekObstacleForwards(1, null, 0, 3);
            Assert.AreEqual(5, info.Distance);
        }

        [Test]
        public void TestGettingNextRegularRoadSegment()
        {
            RegularRoadSegment segmentWithNoNext = new RegularRoadSegment(new Vector[] {});
            Assert.IsNull(segmentWithNoNext.GetNextRegularRoadSegment());
            RegularRoadSegment segmentWithRegularNext = new RegularRoadSegment(new Vector[] {});
            RegularRoadSegment next = new RegularRoadSegment(new Vector[] {});
            segmentWithRegularNext.AddNext(next);
            Assert.AreEqual(next, segmentWithRegularNext.GetNextRegularRoadSegment());
            RegularRoadSegment segmentWithNotRegularNext = new RegularRoadSegment(new Vector[] {});
            segmentWithNotRegularNext.AddNext(new DespawnerSegment());
            Assert.IsNull(segmentWithNotRegularNext.GetNextRegularRoadSegment());
            RegularRoadSegment segmentWithMultipleNext = new RegularRoadSegment(new Vector[] {});
            segmentWithMultipleNext.AddNext(new RegularRoadSegment(new Vector[] {}));
            segmentWithMultipleNext.AddNext(new RegularRoadSegment(new Vector[] {}));
            Assert.IsNull(segmentWithMultipleNext.GetNextRegularRoadSegment());
        }

        [Test]
        public void TestEntryTimesNoVehicles()
        {
            RegularRoadSegment to = new RegularRoadSegment(new []
            {
                new Vector(5, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment from = new RegularRoadSegment(new []
            {
                new Vector(0, 0),
                new Vector(5, 0)
            });
            from.AddNext(to);
            to.InsertAgent(new Agent(null, null) { Position = 2} );
            Assert.That(to.GetEntryTimes(1000), Is.EquivalentTo(new (double, bool)[] { }));
        }

        [Test]
        public void TestEntryTimesOneVehicle()
        {
            RegularRoadSegment to = new RegularRoadSegment(new []
            {
                new Vector(5, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment from = new RegularRoadSegment(new []
            {
                new Vector(0, 0),
                new Vector(5, 0)
            }) { SpeedLimit = 10 };
            from.AddNext(to);
            from.InsertAgent(new Agent(null, null) { Position = 2, Velocity = 4 } );
            Assert.That(to.GetEntryTimes(1000), Is.EquivalentTo(new [] { (0.3, false) }));
        }

        [Test]
        public void TestEntryTimesMultipleVehicles()
        {
            RegularRoadSegment to = new RegularRoadSegment(new []
            {
                new Vector(5, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment from = new RegularRoadSegment(new []
            {
                new Vector(0, 0),
                new Vector(5, 0)
            }) { SpeedLimit = 10 };
            from.AddNext(to);
            from.InsertAgent(new Agent(null, null) { Position = 2, Velocity = 4 } );
            from.InsertAgent(new Agent(null, null) { Position = 4.5, Velocity = 5 } );
            from.InsertAgent(new Agent(null, null) { Position = 4, Velocity = 5 } );
            Assert.That(to.GetEntryTimes(1000), Is.EquivalentTo(new []
            {
                (0.05, false),
                (0.1, false),
                (0.3, false)
            }));
        }

        [Test]
        public void TestEntryTimesMultipleSegments()
        {
            RegularRoadSegment to = new RegularRoadSegment(new []
            {
                new Vector(20, 0),
                new Vector(21, 0)
            });
            RegularRoadSegment from2 = new RegularRoadSegment(new []
            {
                new Vector(5, 0),
                new Vector(20, 0)
            }) { SpeedLimit = 10 };
            RegularRoadSegment from1 = new RegularRoadSegment(new []
            {
                new Vector(0, 0),
                new Vector(5, 0)
            }) { SpeedLimit = 8 };
            from1.AddNext(from2);
            from2.AddNext(to);
            from1.InsertAgent(new Agent(null, null) { Position = 2, Velocity = 4 } );
            from2.InsertAgent(new Agent(null, null) { Position = 4.5, Velocity = 5 } );
            from2.InsertAgent(new Agent(null, null) { Position = 4, Velocity = 5 } );
            Assert.That(to.GetEntryTimes(1000), Is.EquivalentTo(new []
            {
                (1.05, false),
                (1.1, false),
                (1.875, false)
            }));
        }

        [Test]
        public void TestEntryTimesBranching()
        {
            RegularRoadSegment to = new RegularRoadSegment(new []
            {
                new Vector(20, 0),
                new Vector(21, 0)
            });
            RegularRoadSegment from2 = new RegularRoadSegment(new []
            {
                new Vector(5, 0),
                new Vector(20, 0)
            }) { SpeedLimit = 10 };
            RegularRoadSegment from1 = new RegularRoadSegment(new []
            {
                new Vector(20, 5),
                new Vector(20, 0)
            }) { SpeedLimit = 8 };
            from1.AddNext(to);
            from2.AddNext(to);
            from1.InsertAgent(new Agent(null, null) { Position = 2, Velocity = 4 } );
            from2.InsertAgent(new Agent(null, null) { Position = 4.5, Velocity = 5 } );
            from2.InsertAgent(new Agent(null, null) { Position = 4, Velocity = 5 } );
            Assert.That(to.GetEntryTimes(1000), Is.EquivalentTo(new []
            {
                (1.05, false),
                (1.1, false),
                (0.375, false)
            }));
        }

        [Test]
        public void TestEntryTimesDistanceLimit()
        {
            RegularRoadSegment to = new RegularRoadSegment(new []
            {
                new Vector(15, 0),
                new Vector(20, 0)
            });
            RegularRoadSegment from3 = new RegularRoadSegment(new []
            {
                new Vector(10, 0),
                new Vector(15, 0)
            }) { SpeedLimit = 10 };
            RegularRoadSegment from2 = new RegularRoadSegment(new []
            {
                new Vector(5, 0),
                new Vector(10, 0)
            }) { SpeedLimit = 10 };
            RegularRoadSegment from1 = new RegularRoadSegment(new []
            {
                new Vector(0, 0),
                new Vector(5, 0)
            }) { SpeedLimit = 10 };
            from1.AddNext(from2);
            from2.AddNext(from3);
            from3.AddNext(to);
            from1.InsertAgent(new Agent(null, null) { Position = 2, Velocity = 4 } );
            from2.InsertAgent(new Agent(null, null) { Position = 2, Velocity = 4 } );
            from3.InsertAgent(new Agent(null, null) { Position = 2, Velocity = 4 } );
            Assert.That(to.GetEntryTimes(6), Is.EquivalentTo(new []
            {
                (0.3, false),
                (0.8, false)
            }));
        }

        [Test]
        public void TestEntryTimesSplitDecision()
        {
            RegularRoadSegment to1 = new RegularRoadSegment(new []
            {
                new Vector(5, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment to2 = new RegularRoadSegment(new []
            {
                new Vector(5, 0),
                new Vector(5, 5)
            });
            RegularRoadSegment from2 = new RegularRoadSegment(new []
            {
                new Vector(0, 0),
                new Vector(5, 0)
            }) { SpeedLimit = 10 };
            RegularRoadSegment from1 = new RegularRoadSegment(new []
            {
                new Vector(-5, 0),
                new Vector(0, 0)
            }) { SpeedLimit = 10 };
            from1.AddNext(from2);
            from2.AddNext(to1);
            from2.AddNext(to2);
            Agent[] agents1 = new []
            {
                new Agent(null, null) { Position = 1, Velocity = 4 },
                new Agent(null, null) { Position = 2, Velocity = 4 },
                new Agent(null, null) { Position = 3, Velocity = 4 },
                new Agent(null, null) { Position = 4, Velocity = 4 }
            };
            Agent[] agents2 = new []
            {
                new Agent(null, null) { Position = 1, Velocity = 4 },
                new Agent(null, null) { Position = 2, Velocity = 4 },
                new Agent(null, null) { Position = 3, Velocity = 4 },
                new Agent(null, null) { Position = 4, Velocity = 4 }
            };
            List<(double, bool)> expectedEntryTimesTo1 = new List<(double, bool)>();
            List<(double, bool)> expectedEntryTimesTo2 = new List<(double, bool)>();
            foreach (Agent agent in agents1)
            {
                from1.InsertAgent(agent);
                if (agent.DecideOnSegmentChange(from2) == to1)
                {
                    expectedEntryTimesTo1.Add(((10 - agent.Position) / 10, false));
                }
                else
                {
                    expectedEntryTimesTo2.Add(((10 - agent.Position) / 10, false));
                }
            }
            foreach (Agent agent in agents2)
            {
                from2.InsertAgent(agent);
                if (agent.DecideOnSegmentChange(from2) == to1)
                {
                    expectedEntryTimesTo1.Add(((5 - agent.Position) / 10, false));
                }
                else
                {
                    expectedEntryTimesTo2.Add(((5 - agent.Position) / 10, false));
                }
            }
            Assert.That(to1.GetEntryTimes(1000), Is.EquivalentTo(expectedEntryTimesTo1));
            Assert.That(to2.GetEntryTimes(1000), Is.EquivalentTo(expectedEntryTimesTo2));
        }

        [Test]
        public void TestEntryTimesStillness()
        {
            RegularRoadSegment to = new RegularRoadSegment(new []
            {
                new Vector(5, 0),
                new Vector(10, 0)
            });
            RegularRoadSegment from = new RegularRoadSegment(new []
            {
                new Vector(0, 0),
                new Vector(5, 0)
            }) { SpeedLimit = 10 };
            from.AddNext(to);
            from.InsertAgent(new Agent(null, null) { Position = 2, Velocity = 4 } );
            from.InsertAgent(new Agent(null, null) { Position = 3, Velocity = 0.01 } );
            Assert.That(to.GetEntryTimes(1000), Is.EquivalentTo(new []
            {
                (0.2, true),
                (0.3, false)
            }));
        }

        [Test]
        public void TestGettingDistanceFromAgentToRoad()
        {
            RegularRoadSegment segment1 = new RegularRoadSegment(new []
            {
                new Vector(2, 0),
                new Vector(0, 0)
            });
            RegularRoadSegment segment2 = new RegularRoadSegment(new []
            {
                new Vector(4, 0),
                new Vector(2, 0)
            });
            RegularRoadSegment segment3 = new RegularRoadSegment(new []
            {
                new Vector(6, 0),
                new Vector(4, 0)
            });
            RegularRoadSegment segment4 = new RegularRoadSegment(new []
            {
                new Vector(2, 2),
                new Vector(2, 0)
            });
            RegularRoadSegment segment5 = new RegularRoadSegment(new []
            {
                new Vector(4, 2),
                new Vector(4, 0)
            });
            RegularRoadSegment segment6 = new RegularRoadSegment(new []
            {
                new Vector(0, 2),
                new Vector(2, 2)
            });
            RegularRoadSegment segment7 = new RegularRoadSegment(new []
            {
                new Vector(2, 4),
                new Vector(2, 2)
            });
            segment2.AddNext(segment1);
            segment3.AddNext(segment2);
            segment4.AddNext(segment1);
            segment5.AddNext(segment2);
            segment6.AddNext(segment4);
            segment7.AddNext(segment4);
            Agent agent1 = new Agent(null, null) { Position = 1 };
            Agent agent2 = new Agent(null, null) { Position = 1 };
            Agent agent3 = new Agent(null, null) { Position = 1 };
            segment4.InsertAgent(agent1);
            segment5.InsertAgent(agent2);
            Assert.AreEqual(1, segment1.GetDistanceFromAgentToThisSegment(agent1, 99));
            Assert.AreEqual(3, segment1.GetDistanceFromAgentToThisSegment(agent2, 99));
            Assert.AreEqual(-1, segment1.GetDistanceFromAgentToThisSegment(agent3, 99));
        }
    }

    internal class TestAgentForRegularRoadSegmentTest : Agent
    {
        private readonly double _expectedObstacleDistance;
        private readonly double _expectedObstacleVelocity;

        public TestAgentForRegularRoadSegmentTest(double expectedObstacleDistance, double expectedObstacleVelocity) :
            base(null, null)
        {
            _expectedObstacleDistance = expectedObstacleDistance;
            _expectedObstacleVelocity = expectedObstacleVelocity;
        }
        public override void UpdateAcceleration(ObstacleInfo obstacleInfo, double speedLimit)
        {
            Assert.AreEqual(_expectedObstacleDistance, obstacleInfo.DistanceToObstacle);
            Assert.AreEqual(_expectedObstacleVelocity, obstacleInfo.ObstacleForwardVelocity);
        }
    }

    internal class TestRegularRoadSegment : RegularRoadSegment
    {
        public TestRegularRoadSegment(IEnumerable<Vector> vertices) : base(vertices) {}

        public void RemoveAllVehicles()
        {
            Agents.Clear();
        }
    }

    internal class TestLaneChangeModel : ILaneChangeModel
    {
        private readonly bool _shouldChange;

        public TestLaneChangeModel(bool shouldChange)
        {
            _shouldChange = shouldChange;
        }

        public bool IncentiveToChange(double oldAcceleration, double newAcceleration, double oldFollowerOldAcceleration,
            double oldFollowerNewAcceleration, double newFollowerOldAcceleration, double newFollowerNewAcceleration, double bias)
        {
            return _shouldChange;
        }
    }
}