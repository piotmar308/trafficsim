﻿using NUnit.Framework;
using trafficsim.Simulation.Model;

namespace trafficsim.Tests
{
    [TestFixture]
    public class MOBILModelTest
    {
        [Test]
        public void TestLaneChangeDesired()
        {
            ILaneChangeModel model = new MOBILModel()
            {
                LaneChangeThreshold = 1,
                Politeness = 0.1
            };
            Assert.True(model.IncentiveToChange(
                oldAcceleration: 1,
                newAcceleration: 2,
                oldFollowerOldAcceleration: 3,
                oldFollowerNewAcceleration: 1,
                newFollowerOldAcceleration: 5,
                newFollowerNewAcceleration: 4,
                bias: 0.5
            ));
        }

        [Test]
        public void TestLaneChangeUndesired()
        {
            ILaneChangeModel model = new MOBILModel()
            {
                LaneChangeThreshold = 1,
                Politeness = 0.2
            };
            Assert.False(model.IncentiveToChange(
                oldAcceleration: 1,
                newAcceleration: 2,
                oldFollowerOldAcceleration: 3,
                oldFollowerNewAcceleration: 1,
                newFollowerOldAcceleration: 5,
                newFollowerNewAcceleration: 4,
                bias: 0.5
            ));
        }
    }
}