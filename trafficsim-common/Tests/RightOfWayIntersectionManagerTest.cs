﻿using System.Windows;
using NUnit.Framework;
using trafficsim.Simulation.Intersection;
using trafficsim.Simulation.RoadSegment;
using trafficsim.Simulation.Vehicle;

namespace trafficsim.Tests
{
    [TestFixture]
    public class RightOfWayIntersectionManagerTest
    {
        [Test]
        public void TestOccupationOfPrioritySegmentChecking()
        {
            RightOfWayIntersectionSegment segment1 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(-5, 0),
                new Vector(5, 0),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segment2 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, -5),
                new Vector(0, 5),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segmentBeforeSegment1 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(-100, 0),
                new Vector(-5, 0),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segmentBeforeSegment2 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, -100),
                new Vector(0, -5),
            }) { SpeedLimit = 1 };
            segmentBeforeSegment1.AddNext(segment1);
            segmentBeforeSegment2.AddNext(segment2);
            segmentBeforeSegment1.InsertAgent(new Agent(null, null) { Position = 5 });
            segmentBeforeSegment2.InsertAgent(new Agent(null, null) { Position = 50 });
            RightOfWayIntersectionManager manager = new RightOfWayIntersectionManager(new []
            {
                (prioritySegment: segment1, yieldingSegment: segment2)
            });
            manager.ComputeIntersection(0);
            Assert.False(segment1.Closed);
            Assert.False(segment2.Closed);
            segment1.InsertAgent(new Agent(null, null) { Position = 5 });
            manager.ComputeIntersection(0);
            Assert.False(segment1.Closed);
            Assert.True(segment2.Closed);
        }

        [Test]
        public void TestOccupationOfYieldingSegmentChecking()
        {
            RightOfWayIntersectionSegment segment1 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(-5, 0),
                new Vector(5, 0),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segment2 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, -5),
                new Vector(0, 5),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segmentBeforeSegment1 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(-100, 0),
                new Vector(-5, 0),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segmentBeforeSegment2 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, -100),
                new Vector(0, -5),
            }) { SpeedLimit = 1 };
            segmentBeforeSegment1.AddNext(segment1);
            segmentBeforeSegment2.AddNext(segment2);
            segmentBeforeSegment1.InsertAgent(new Agent(null, null) { Position = 5 });
            segmentBeforeSegment2.InsertAgent(new Agent(null, null) { Position = 50 });
            RightOfWayIntersectionManager manager = new RightOfWayIntersectionManager(new []
            {
                (prioritySegment: segment1, yieldingSegment: segment2)
            });
            manager.ComputeIntersection(0);
            Assert.False(segment1.Closed);
            Assert.False(segment2.Closed);
            segment2.InsertAgent(new Agent(null, null) { Position = 5 });
            manager.ComputeIntersection(0);
            Assert.True(segment1.Closed);
            Assert.False(segment2.Closed);
        }

        [Test]
        public void TestPriorityChecking()
        {
            RightOfWayIntersectionSegment segment1 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(-5, 0),
                new Vector(5, 0),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segment2 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, -5),
                new Vector(0, 5),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segmentBeforeSegment1 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(-10, 0),
                new Vector(-5, 0),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segmentBeforeSegment2 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, -10),
                new Vector(0, -5),
            }) { SpeedLimit = 1 };
            segmentBeforeSegment1.AddNext(segment1);
            segmentBeforeSegment2.AddNext(segment2);
            segmentBeforeSegment1.InsertAgent(new Agent(null, null) { Position = 5 });
            segmentBeforeSegment2.InsertAgent(new Agent(null, null) { Position = 5 });
            RightOfWayIntersectionManager manager = new RightOfWayIntersectionManager(new []
            {
                (prioritySegment: segment1, yieldingSegment: segment2)
            });
            manager.ComputeIntersection(0);
            Assert.False(segment1.Closed);
            Assert.True(segment2.Closed);
        }

        [Test]
        public void TestSafetyGapChecking()
        {
            RightOfWayIntersectionSegment segment1 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(-5, 0),
                new Vector(-4, 0),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segment2 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, -5),
                new Vector(0, -4),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segmentBeforeSegment1 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(-10, 0),
                new Vector(-5, 0),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segmentBeforeSegment2 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, -10),
                new Vector(0, -5),
            }) { SpeedLimit = 1 };
            segmentBeforeSegment1.AddNext(segment1);
            segmentBeforeSegment2.AddNext(segment2);
            segmentBeforeSegment1.InsertAgent(new Agent(null, null) { Position = 5, Velocity = 5 });
            segmentBeforeSegment2.InsertAgent(new Agent(null, null) { Position = 8, Velocity = 5 });
            RightOfWayIntersectionManager manager = new RightOfWayIntersectionManager(new[]
            {
                (prioritySegment: segment1, yieldingSegment: segment2)
            })
            {
                SafetyGap = 1.5
            };
            manager.ComputeIntersection(0);
            Assert.False(segment1.Closed);
            Assert.False(segment2.Closed);
            manager.SafetyGap = 2.5;
            manager.ComputeIntersection(0);
            Assert.False(segment1.Closed);
            Assert.True(segment2.Closed);
        }

        [Test]
        public void TestStillnessChecking()
        {
            RightOfWayIntersectionSegment segment1 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(-5, 0),
                new Vector(-4, 0),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segment2 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, -5),
                new Vector(0, -4),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segmentBeforeSegment1 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(-10, 0),
                new Vector(-5, 0),
            }) { SpeedLimit = 1 };
            RightOfWayIntersectionSegment segmentBeforeSegment2 = new RightOfWayIntersectionSegment(new []
            {
                new Vector(0, -10),
                new Vector(0, -5),
            }) { SpeedLimit = 1 };
            segmentBeforeSegment1.AddNext(segment1);
            segmentBeforeSegment2.AddNext(segment2);
            Agent agent1 = new Agent(null, null) { Position = 5, Velocity = 5 };
            Agent agent2 = new Agent(null, null) { Position = 8, Velocity = 5 };
            Agent agent3 = new Agent(null, null) { Position = 8, Velocity = 5 };
            segmentBeforeSegment1.InsertAgent(agent1);
            segmentBeforeSegment1.InsertAgent(agent2);
            segmentBeforeSegment2.InsertAgent(agent3);
            RightOfWayIntersectionManager manager = new RightOfWayIntersectionManager(new[]
            {
                (prioritySegment: segment1, yieldingSegment: segment2)
            })
            {
                SafetyGap = 0
            };
            manager.ComputeIntersection(0);
            Assert.False(segment1.Closed);
            Assert.True(segment2.Closed);
            agent2.Velocity = 0;
            manager.ComputeIntersection(0);
            Assert.False(segment1.Closed);
            Assert.True(segment2.Closed);
            agent3.Position = 5;
            manager.ComputeIntersection(0);
            Assert.False(segment1.Closed);
            Assert.True(segment2.Closed);
            agent1.Velocity = 0;
            manager.ComputeIntersection(0);
            Assert.False(segment1.Closed);
            Assert.False(segment2.Closed);
        }
    }
}