﻿using System;
using System.Globalization;
using System.Linq;
using trafficsim;
using trafficsim.Simulation;
using trafficsim.Simulation.RoadSegment;

namespace trafficsim_scenario_transformer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Scenario scenario = Scenario.FromFile(args[0]);
            CultureInfo ci = new CultureInfo("en");
            int genotypeSize = scenario.RoadSegments.Count(s =>
                s is RegularRoadSegment segment && segment.CanHaveHandPickedLanes());
            Console.Out.WriteLine($"Input scenario total length: {scenario.RoadSegments.Sum(s => s.GetLength()).ToString(ci)} m, genotype size: {genotypeSize}.");
            if (args.Length > 1)
            {
                if (args.Length != genotypeSize + 2 && args.Length != genotypeSize + 3)
                {
                    Console.Out.WriteLine($"Invalid genotype size. Expected genotype size: {genotypeSize} chromosomes");
                }
                else if (!args.Skip(2).Take(genotypeSize).All(chromosomeString => byte.TryParse(chromosomeString, out _)))
                {
                    Console.Out.WriteLine("All chromosomes must be numbers between 0 and 255.");
                    foreach (string str in args.Skip(2).Take(genotypeSize))
                    {
                        if (!byte.TryParse(str, out _))
                        {
                            Console.Out.WriteLine($"Invalid chromosome \"{str}\".");
                        }
                    }
                }
                else
                {
                    byte[] genotype = args.Skip(2).Take(genotypeSize).Select(byte.Parse).ToArray();
                    TransformerMode mode;
                    switch (args[1])
                    {
                        case "binary":
                            mode = TransformerMode.Binary;
                            break;
                        case "gray":
                            mode = TransformerMode.Gray;
                            break;
                        case "lanecount":
                            mode = TransformerMode.LaneCount;
                            break;
                        default:
                            Console.Out.WriteLine("Invalid transformer mode, expected binary|gray|lanecount");
                            return;
                    }
                    Scenario newScenario = Transformer.CopyAndTransform(scenario, genotype, mode);
                    if (newScenario.HasDeadEndRoutes())
                    {
                        Console.Out.WriteLine("At least one route has dead ends.");
                    }
                    Console.Out.WriteLine($"Output scenario total length: {newScenario.RoadSegments.Sum(s => s.GetLength()).ToString(ci)} m.");

                    DateTime time1 = DateTime.Now;
                    Action<int, Scenario> afterEveryMinute = (i, scenario1) =>
                    {
                        DateTime time2 = DateTime.Now;
                        TimeSpan diff = time2 - time1;
                        Console.Out.WriteLine($"After {i} minutes: " +
                                              $"spawned: {newScenario.TotalAgentsSpawned.Value} agents, " +
                                              $"throughput: {newScenario.TotalThroughput.Value} agents, " +
                                              $"agents' lifetime sum: {newScenario.AgentLifetimeSum.Value} seconds, " +
                                              $"computation time from start: {diff.TotalSeconds} seconds");
                    };

                    if (args.Length > genotypeSize + 2)
                    {
                        double limit = double.Parse(args[genotypeSize + 2], new CultureInfo("en"));
                        HeadlessSimulation.Run(newScenario, limit, afterEveryMinute);
                        newScenario.TimeWarp = 0;
                    }
                    SimulationViewer.Run(newScenario, afterEveryMinute);

                    if (newScenario.TimePassed > 0)
                    {
                        int minutes = (int) Math.Floor(newScenario.TimePassed / 60);
                        int seconds = (int) Math.Floor(newScenario.TimePassed) - minutes * 60;
                        Console.Out.WriteLine($"Finished after {minutes} minutes {seconds} seconds. Total throughput: {newScenario.TotalThroughput.Value.ToString(ci)} agents");
                        foreach (SpawnerSegment s in newScenario.RoadSegments.Where(s => s is SpawnerSegment)
                            .Cast<SpawnerSegment>())
                        {
                            Console.Out.WriteLine($"Throughput for route \"{s.Id}\": {s.RouteThroughput.Value.ToString(ci)} " +
                                                  "agents");
                        }
                        Console.Out.WriteLine($"Total throughput: {newScenario.TotalThroughput.Value} agents");
                    }
                }
            }
            else
            {
                Console.Out.WriteLine($"Genotype size: {genotypeSize} chromosomes.");
            }
        }
    }
}