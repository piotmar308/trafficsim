﻿using System;
using System.Globalization;
using System.Linq;
using trafficsim.Simulation;
using trafficsim.Simulation.RoadSegment;

namespace trafficsim
{
    /// <summary>
    /// Main program class.
    /// </summary>
    class Program
    {
        /// <summary>
        /// Entry method for the program. Imports traffic scenario from file, displays it in a window and simulates is.
        /// </summary>
        /// <remarks>
        /// If the import fails, an error message is displayed in standard output and the method ends. Otherwise the
        /// method will not finish until the simulation window is closed. During simulation left and right arrow keys
        /// can be used to speed up and slow down the simulation by 0.1x. Pressing the arrow keys while holding Shift
        /// will speed up or slow down by 1x. Upon changing the speed, new speed is written to standard output.
        /// </remarks>
        public static void Main(string[] args)
        {
            if (args.Length == 0 || args[0] == "-h" || args[0] == "--help")
            {
                Console.Out.WriteLine("Usage: trafficsim [path] [limit]");
                Console.Out.WriteLine("path - absolute or relative path to scenario xml file");
                Console.Out.WriteLine("limit - specifying this argument indicates, that simulation should run in " +
                                      "headless mode for specified number of simulated seconds");
            }
            else
            {
                try
                {
                    Scenario scenario = Scenario.FromFile(args[0]);
                    if (args.Length > 1)
                    {
                        double limit = double.Parse(args[1], new CultureInfo("en"));
                        HeadlessSimulation.Run(scenario, limit);
                    }
                    else
                    {
                        SimulationViewer.Run(scenario);
                    }

                    if (scenario.TimePassed > 0)
                    {
                        CultureInfo ci = new CultureInfo("en");
                        int minutes = (int) Math.Floor(scenario.TimePassed / 60);
                        int seconds = (int) Math.Floor(scenario.TimePassed) - minutes * 60;
                        Console.Out.WriteLine($"Finished after {minutes} minutes {seconds} seconds. Total throughput: {scenario.TotalThroughput.Value.ToString(ci)} agents");
                        foreach (SpawnerSegment s in scenario.RoadSegments.Where(s => s is SpawnerSegment)
                            .Cast<SpawnerSegment>())
                        {
                            Console.Out.WriteLine($"Throughput for route \"{s.Id}\": {s.RouteThroughput.Value.ToString(ci)} " +
                                                  "agents");
                        }
                        Console.Out.WriteLine($"Total throughput: {scenario.TotalThroughput.Value.ToString(ci)} agents");
                    }
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine(e.Message);
                }
            }
        }
    }
}