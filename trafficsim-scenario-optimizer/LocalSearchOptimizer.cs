﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using trafficsim;
using trafficsim.Simulation;
using trafficsim.Simulation.RoadSegment;
using trafficsim_scenario_transformer;

namespace trafficsim_scenario_optimizer
{
    public enum LocalSearchStartingPosition
    {
        Lowest,
        Highest,
        Middle,
        Random
    }

    public class LocalSearchOptimizer
    {
        private readonly Random _random = new Random(0);
        private string _fileName;
        private string _inputScenarioFilename;
        private Scenario _inputScenario;
        private double _desiredLength;
        private int _spaceDimmensionCount;
        private byte _spaceMaxPosition;
        private double _simulationTime;
        private TransformerMode _mode;
        private byte[] _position;
        private double _positionFitness;

        public LocalSearchOptimizer(string inputScenarioFilename, double desiredLength, int maxLaneCount, double simulationTime, TransformerMode mode, LocalSearchStartingPosition startingPosition)
        {
            _fileName = "optimizer-log-" + DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss") + ".txt";
            _inputScenarioFilename = inputScenarioFilename;
            _inputScenario = Scenario.FromFile(inputScenarioFilename);
            _desiredLength = desiredLength;
            _simulationTime = simulationTime;
            _mode = mode;
            string modeString = "";
            switch (mode)
            {
                case TransformerMode.Binary:
                    _spaceMaxPosition = (byte) ((2 << (maxLaneCount - 1)) - 1);
                    modeString = "binary";
                    break;
                case TransformerMode.Gray:
                    _spaceMaxPosition = (byte) ((2 << (maxLaneCount - 1)) - 1);
                    modeString = "gray code";
                    break;
                case TransformerMode.LaneCount:
                    _spaceMaxPosition = (byte) maxLaneCount;
                    modeString = "lane count";
                    break;
            }
            _spaceDimmensionCount = _inputScenario.RoadSegments.Count(s => s is RegularRoadSegment segment && segment.CanHaveHandPickedLanes());
            _position = new byte[_spaceDimmensionCount];
            switch (startingPosition)
            {
                case LocalSearchStartingPosition.Lowest:
                    for (int i = 0; i < _spaceDimmensionCount; ++i)
                    {
                        _position[i] = 1;
                    }
                    break;
                case LocalSearchStartingPosition.Random:
                    for (int i = 0; i < _spaceDimmensionCount; ++i)
                    {
                        _position[i] = (byte) _random.Next(1, _spaceMaxPosition + 1);
                    }
                    break;
                case LocalSearchStartingPosition.Highest:
                    switch (mode)
                    {
                        case TransformerMode.Binary:
                        case TransformerMode.LaneCount:
                            for (int i = 0; i < _spaceDimmensionCount; ++i)
                            {
                                _position[i] = _spaceMaxPosition;
                            }
                            break;
                        case TransformerMode.Gray:
                            for (int i = 0; i < _spaceDimmensionCount; ++i)
                            {
                                _position[i] = (byte) (170 >> (8 - maxLaneCount));
                            }
                            break;
                    }
                    break;
                case LocalSearchStartingPosition.Middle:
                    switch (mode)
                    {
                        case TransformerMode.Binary:
                        case TransformerMode.LaneCount:
                            for (int i = 0; i < _spaceDimmensionCount; ++i)
                            {
                                _position[i] = (byte) ((_spaceMaxPosition + 1) / 2);
                            }
                            break;
                        case TransformerMode.Gray:
                            for (int i = 0; i < _spaceDimmensionCount; ++i)
                            {
                                _position[i] = (byte) (170 >> (8 - maxLaneCount / 2));
                            }
                            break;
                    }
                    break;
            }

            Log($"Input scenario total length: {_inputScenario.RoadSegments.Sum(s => s.GetLength())} m.");
            Log($"Scenario desired length: {desiredLength} m.");
            Log($"Search space dimension count: {_spaceDimmensionCount}.");
            Log($"Search starting position: {string.Join(" ", _position)}.");
            Log($"Maximum lane count: {maxLaneCount} (maximum dimension position: {_spaceMaxPosition}).");
            Log($"Transformer mode: {modeString}.");
            Scenario scenario = Transformer.CopyAndTransform(_inputScenario, _position, mode);
            HeadlessSimulation.Run(scenario, _simulationTime);
            double multiplier = GetScoreMultiplier(scenario.RoadSegments.Sum(x => x.GetLength()));
            _positionFitness = scenario.GetScore() * multiplier;
            Log($"Simulated starting position, got total throughput {scenario.TotalThroughput.Value} agents, fitness {_positionFitness}.");
        }

        public void Optimize()
        {
            bool foundBetterSolution;
            do
            {
                foundBetterSolution = false;
                byte[][] neighbors = GetNeighboringPositions();
                Scenario[] scenarios = neighbors.Select(g => Transformer.CopyAndTransform(Scenario.FromFile(_inputScenarioFilename), g, _mode)).ToArray();
                double[] fitness = new double[neighbors.Length];
                Console.Write($"Simulating {neighbors.Length} neighbors");
                Parallel.ForEach(scenarios, (scenario, pls, index) =>
                {
                    if (scenario.HasDeadEndRoutes())
                    {
                        fitness[index] = 0;
                    }
                    else
                    {
                        HeadlessSimulation.Run(scenario, _simulationTime);
                        double multiplier = GetScoreMultiplier(scenario.RoadSegments.Sum(x => x.GetLength()));
                        fitness[index] = scenario.GetScore() * multiplier;
                    }
                    Console.Write('.');
                });
                Console.WriteLine();
                for (int i = 0; i < neighbors.Length; ++i)
                {
                    if (fitness[i] > _positionFitness)
                    {
                        _positionFitness = fitness[i];
                        _position = neighbors[i];
                        foundBetterSolution = true;
                    }
                }
                Log($"Finished iteration, moved to {string.Join(" ", _position)} with fitness {_positionFitness}.");
            } while (foundBetterSolution);
            Log($"Finished search.");
        }

        private double GetScoreMultiplier(double length)
        {
            double ret = length > _desiredLength ? 1 - (length - _desiredLength) / _desiredLength : 1;
            return ret < 0 ? 0 : ret;
        }

        private byte[][] GetNeighboringPositions()
        {
            List<byte[]> ret = new List<byte[]>();
            for (int i = 0; i < _spaceDimmensionCount; ++i)
            {
                if (_position[i] > 1)
                {
                    ret.Add(_position.Select((pos, j) => (byte)(i == j ? pos - 1 : pos)).ToArray());
                }
                if (_position[i] < _spaceMaxPosition)
                {
                    ret.Add(_position.Select((pos, j) => (byte)(i == j ? pos + 1 : pos)).ToArray());
                }
            }

            return ret.ToArray();
        }

        private void Log(string msg)
        {
            Console.WriteLine(msg);
            StreamWriter sw = File.AppendText(_fileName);
            sw.WriteLine(msg);
            sw.Close();
        }
    }
}