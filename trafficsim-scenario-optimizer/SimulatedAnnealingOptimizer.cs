﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using trafficsim;
using trafficsim.Simulation;
using trafficsim.Simulation.RoadSegment;
using trafficsim_scenario_transformer;

namespace trafficsim_scenario_optimizer
{

    public class SimulatedAnnealingOptimizer
    {
        private readonly Random _random = new Random(0);
        private string _fileName;
        private string _inputScenarioFilename;
        private Scenario _inputScenario;
        private double _desiredLength;
        private int _spaceDimmensionCount;
        private byte _spaceMaxPosition;
        private double _simulationTime;
        private TransformerMode _mode;
        private byte[] _position;
        private double _positionFitness;
        private double _cooldownFactor;
        private int _failedIterationsToBail;
        private double _initialTemperature;
        private int _epochLength;

        public SimulatedAnnealingOptimizer(string inputScenarioFilename, double desiredLength, int maxLaneCount, double simulationTime, TransformerMode mode, LocalSearchStartingPosition startingPosition, double cooldownFactor, int epochLength, int failedIterationsToBail, double initialProbabilityOfAcceptingLowerFitness)
        {
            _fileName = "optimizer-log-" + DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss") + ".txt";
            _inputScenarioFilename = inputScenarioFilename;
            _inputScenario = Scenario.FromFile(inputScenarioFilename);
            _desiredLength = desiredLength;
            _simulationTime = simulationTime;
            _cooldownFactor = cooldownFactor;
            _epochLength = epochLength;
            _failedIterationsToBail = failedIterationsToBail;
            _mode = mode;
            string modeString = "";
            switch (mode)
            {
                case TransformerMode.Binary:
                    _spaceMaxPosition = (byte) ((2 << (maxLaneCount - 1)) - 1);
                    modeString = "binary";
                    break;
                case TransformerMode.Gray:
                    _spaceMaxPosition = (byte) ((2 << (maxLaneCount - 1)) - 1);
                    modeString = "gray code";
                    break;
                case TransformerMode.LaneCount:
                    _spaceMaxPosition = (byte) maxLaneCount;
                    modeString = "lane count";
                    break;
            }
            _spaceDimmensionCount = _inputScenario.RoadSegments.Count(s => s is RegularRoadSegment segment && segment.CanHaveHandPickedLanes());
            _position = new byte[_spaceDimmensionCount];
            switch (startingPosition)
            {
                case LocalSearchStartingPosition.Lowest:
                    for (int i = 0; i < _spaceDimmensionCount; ++i)
                    {
                        _position[i] = 1;
                    }
                    break;
                case LocalSearchStartingPosition.Random:
                    for (int i = 0; i < _spaceDimmensionCount; ++i)
                    {
                        _position[i] = (byte) _random.Next(1, _spaceMaxPosition + 1);
                    }
                    break;
                case LocalSearchStartingPosition.Highest:
                    switch (mode)
                    {
                        case TransformerMode.Binary:
                        case TransformerMode.LaneCount:
                            for (int i = 0; i < _spaceDimmensionCount; ++i)
                            {
                                _position[i] = _spaceMaxPosition;
                            }
                            break;
                        case TransformerMode.Gray:
                            for (int i = 0; i < _spaceDimmensionCount; ++i)
                            {
                                _position[i] = (byte) (170 >> (8 - maxLaneCount));
                            }
                            break;
                    }
                    break;
            }

            Log($"Input scenario total length: {_inputScenario.RoadSegments.Sum(s => s.GetLength())} m.");
            Log($"Scenario desired length: {desiredLength} m.");
            Log($"Search space dimension count: {_spaceDimmensionCount}.");
            Log($"Search starting position: {string.Join(" ", _position)}.");
            Log($"Maximum lane count: {maxLaneCount} (maximum dimension position: {_spaceMaxPosition}).");
            Log($"Transformer mode: {modeString}.");
            Log($"Cooldown factor: {cooldownFactor}.");
            Log($"Epoch length: {epochLength}.");
            Log($"Number of failed epochs to bail: {failedIterationsToBail}.");
            Scenario scenario = Transformer.CopyAndTransform(_inputScenario, _position, mode);
            HeadlessSimulation.Run(scenario, _simulationTime);
            double multiplier = GetScoreMultiplier(scenario.RoadSegments.Sum(x => x.GetLength()));
            _positionFitness = scenario.GetScore() * multiplier;
            Log($"Simulated starting position, got total throughput {scenario.TotalThroughput.Value} agents, fitness {_positionFitness}.");
            Log($"Initial probability of accepting lower fitness: {initialProbabilityOfAcceptingLowerFitness}.");
            Console.WriteLine("Determining starting temperature.");
            _initialTemperature = GetStartingTemperature(initialProbabilityOfAcceptingLowerFitness);
            Log($"Initial temperature: {_initialTemperature}.");
        }

        public void Optimize()
        {
            int failedEpochs = 0;
            int iteration = 0;
            double temperature = _initialTemperature;
            do
            {
                ++failedEpochs;
                
                for (int i = 0; i < _epochLength; ++i)
                {
                    byte[][] neighbors = GetNeighboringPositions();
                    byte[] neighbor = neighbors.ElementAt(_random.Next(neighbors.Length));
                    Log($"Iteration: {iteration}, temperature: {temperature}, current position: {string.Join(" ", _position)}, current fitness: {_positionFitness}, chosen neighbor: {string.Join(" ", neighbor)}.");
                    Scenario scenario = Transformer.CopyAndTransform(Scenario.FromFile(_inputScenarioFilename), neighbor, _mode);
                    double fitness;
                    if (scenario.HasDeadEndRoutes())
                    {
                        fitness = 0;
                    }
                    else
                    {
                        HeadlessSimulation.Run(scenario, _simulationTime);
                        double multiplier = GetScoreMultiplier(scenario.RoadSegments.Sum(x => x.GetLength()));
                        fitness = scenario.GetScore() * multiplier;
                    }
                    Log($"Neighbor fitness: {fitness}.");

                    if (fitness >= _positionFitness)
                    {
                        _positionFitness = fitness;
                        _position = neighbor;
                        failedEpochs = 0;
                        Log("Fitness greater or equal that at current position, moving to neighbor.");
                    }
                    else
                    {
                        double probability = Math.Exp((fitness - _positionFitness) / temperature);
                        double randomNumber = _random.NextDouble();
                        Log($"Probability: {probability}, drawn number: {randomNumber}");
                        if (randomNumber < probability)
                        {
                            _positionFitness = fitness;
                            _position = neighbor;
                            failedEpochs = 0;
                            Log("Fitness lower that at current position, annealing allowed moving to neighbor.");
                        }
                        else
                        {
                            Log($"Fitness lower that at current position, annealing did not allow moving to neighbor.");
                        }
                    }
                        
                    ++iteration;
                }

                temperature *= _cooldownFactor;
                
                Log($"Finished epoch, {failedEpochs} failed epochs so far.");
            } while (failedEpochs < _failedIterationsToBail);
            Log($"Finished search at {string.Join(" ", _position)} with fitness {_positionFitness}.");
        }

        private double GetScoreMultiplier(double length)
        {
            double ret = length > _desiredLength ? 1 - (length - _desiredLength) / _desiredLength : 1;
            return ret < 0 ? 0 : ret;
        }

        private byte[][] GetNeighboringPositions()
        {
            List<byte[]> ret = new List<byte[]>();
            for (int i = 0; i < _spaceDimmensionCount; ++i)
            {
                if (_position[i] > 1)
                {
                    ret.Add(_position.Select((pos, j) => (byte)(i == j ? pos - 1 : pos)).ToArray());
                }
                if (_position[i] < _spaceMaxPosition)
                {
                    ret.Add(_position.Select((pos, j) => (byte)(i == j ? pos + 1 : pos)).ToArray());
                }
            }

            return ret.ToArray();
        }

        private double GetStartingTemperature(double initialProbabilityOfAcceptingLowerFitness)
        {
            byte[][] neighbors = GetNeighboringPositions();
            double[] fitness = new double[_epochLength];
            byte[][] selectedNeighbors = neighbors.OrderBy(x => _random.Next()).Take(_epochLength).ToArray();
            Scenario[] scenarios = selectedNeighbors.Select(g => Transformer.CopyAndTransform(Scenario.FromFile(_inputScenarioFilename), g, _mode)).ToArray();
            Console.Write($"Simulating {_epochLength} random neighbors");
            Parallel.ForEach(scenarios, (scenario, pls, index) =>
            {
                if (scenario.HasDeadEndRoutes())
                {
                    fitness[index] = 0;
                }
                else
                {
                    HeadlessSimulation.Run(scenario, _simulationTime);
                    double multiplier = GetScoreMultiplier(scenario.RoadSegments.Sum(x => x.GetLength()));
                    fitness[index] = scenario.GetScore() * multiplier;
                }
                Console.Write('.');
            });
            Console.WriteLine();
            double[] loss = fitness.Select(val => Math.Abs(val - _positionFitness)).ToArray();
            double avgLoss = loss.Average();
            return -avgLoss / Math.Log(initialProbabilityOfAcceptingLowerFitness);
        }

        private void Log(string msg)
        {
            Console.WriteLine(msg);
            StreamWriter sw = File.AppendText(_fileName);
            sw.WriteLine(msg);
            sw.Close();
        }
    }
}