﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using trafficsim;
using trafficsim.Simulation;
using trafficsim.Simulation.RoadSegment;
using trafficsim_scenario_transformer;

namespace trafficsim_scenario_optimizer
{
    public class GeneticOptimizer
    {
        private readonly Random _random = new Random(0);
        private Scenario _inputScenario;
        private string _inputScenarioFilename;
        private double _desiredLength;
        private int _populationSize, _genotypeSize;
        private byte[][] _population;
        private int _geneMaxValue;
        private double _simulationTime;
        private double _mutationRate;
        private string _fileName;
        private TransformerMode _mode;
        private bool _rank;
        private int _generationLimit;

        public GeneticOptimizer(string inputScenarioFilename, double desiredLength, int populationSize, double mutationRate, int maxLaneCount, double simulationTime, TransformerMode mode, int generationLimit, bool rank)
        {
            _fileName = "optimizer-log-" + DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss") + ".txt";
            _inputScenarioFilename = inputScenarioFilename;
            _inputScenario = Scenario.FromFile(inputScenarioFilename);
            _desiredLength = desiredLength;
            _populationSize = populationSize;
            _mutationRate = mutationRate;
            _generationLimit = generationLimit;
            _rank = rank;
            string modeString = "";
            switch (mode)
            {
                case TransformerMode.Binary:
                    _geneMaxValue = (2 << (maxLaneCount - 1)) - 1;
                    modeString = "binary";
                    break;
                case TransformerMode.Gray:
                    _geneMaxValue = (2 << (maxLaneCount - 1)) - 1;
                    modeString = "gray code";
                    break;
                case TransformerMode.LaneCount:
                    _geneMaxValue = maxLaneCount;
                    modeString = "lane count";
                    break;
            }
            _simulationTime = simulationTime;
            _genotypeSize = _inputScenario.RoadSegments.Count(s => s is RegularRoadSegment segment && segment.CanHaveHandPickedLanes());
            _mode = mode;

            _population = new byte[populationSize][];
            int i = 0;

            Console.WriteLine("Preparing initial population...");
            while (i < populationSize)
            {
                _population[i] = new byte[_genotypeSize];
                for (int j = 0; j < _genotypeSize; ++j)
                {
                    _population[i][j] = (byte) _random.Next(1, _geneMaxValue + 1);
                }

                if (!Transformer.CopyAndTransform(_inputScenario, _population[i], mode).HasDeadEndRoutes())
                {
                    ++i;
                    Console.WriteLine(i);
                }
            }

            Log($"Input scenario total length: {_inputScenario.RoadSegments.Sum(s => s.GetLength())} m.");
            Log($"Scenario desired length: {desiredLength} m.");
            Log($"Population size: {populationSize}.");
            Log($"Mutation rate: {mutationRate}.");
            Log($"Generation limit: {generationLimit}.");
            Log(rank ? "Selection method: rank." : "Selection method: roulette wheel.");
            Log($"Simulation time: {simulationTime} seconds.");
            Log($"Maximum lane count: {maxLaneCount} (maximum chromosome value: {_geneMaxValue}).");
            Log($"Transformer mode: {modeString}.");
        }

        public void Optimize()
        {
            for (int i = 0; i < _generationLimit; ++i)
            {
                Log($"Generation {i+1}.");
                double[] fitness = new double[_populationSize];
                Scenario[] scenarios = _population.Select(g => Transformer.CopyAndTransform(Scenario.FromFile(_inputScenarioFilename), g, _mode)).ToArray();
                bool[] deadEnds = new bool[_populationSize];

                Parallel.ForEach(scenarios, (scenario, pls, index) =>
                {
                    if (scenario.HasDeadEndRoutes())
                    {
                        fitness[index] = 0;
                        deadEnds[index] = true;
                    }
                    else
                    {
                        HeadlessSimulation.Run(scenario, _simulationTime);
                        double multiplier = GetScoreMultiplier(scenario.RoadSegments.Sum(x => x.GetLength()));
                        fitness[index] = scenario.GetScore() * multiplier;
                        deadEnds[index] = false;
                    }
                    Console.Write('.');
                });
                Console.WriteLine();

                double bestFitness = fitness.Max();
                for (int j = 0; j < _populationSize; ++j)
                {
                    if (deadEnds[j])
                    {
                        Log($"Scenario with genotype {string.Join(" ", _population[j])} has dead end routes, assigned fitness 0.");
                    }
                    else
                    {
                        Log($"Simulated scenario with genotype {string.Join(" ", _population[j])}, got total throughput " +
                            $"{scenarios[j].TotalThroughput.Value} agents, fitness {fitness[j]}." + (fitness[j] == bestFitness ? " (BEST)" : ""));
                    }
                }

                byte[][] newPopulation = new byte[_populationSize][];
                int k = 0;
                while (k < _populationSize)
                {
                    newPopulation[k] = Mutate(Crossover(Select(fitness), Select(fitness)));
                    if (!Transformer.CopyAndTransform(_inputScenario, newPopulation[k], _mode).HasDeadEndRoutes())
                    {
                        ++k;
                    }
                }

                _population = newPopulation;
            }
        }

        private double GetScoreMultiplier(double length)
        {
            double ret = length > _desiredLength ? 1 - (length - _desiredLength) / _desiredLength : 1;
            return ret < 0 ? 0 : ret;
        }

        private double NextGaussian()
        {
            double u1 = _random.NextDouble();
            double u2 = _random.NextDouble();

            return Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
        }

        private byte[] Mutate(byte[] genotype)
        {
            byte[] ret = genotype.Select(gene =>
            {
                byte newGene = _random.NextDouble() < _mutationRate ? (byte) (Math.Round(NextGaussian()) + gene) : gene;
                return (byte) (newGene < 1 ? 1 : (newGene > _geneMaxValue ? _geneMaxValue : newGene));
            }).ToArray();

            Log($"Mutating {string.Join(" ", genotype)}, got {string.Join(" ", ret)}.");
            return ret;
        }

        private byte[] Crossover(byte[] a, byte[] b)
        {
            int crossoverPoint = _random.Next(1, _genotypeSize - 1);
            byte[] ret = new byte[_genotypeSize];
            for (int i = 0; i < _genotypeSize; ++i)
            {
                ret[i] = i >= crossoverPoint ? b[i] : a[i];
            }

            Log($"Crossing over {string.Join(" ", a)} and {string.Join(" ", b)} at point " +
                $"{crossoverPoint}, got {string.Join(" ", ret)}.");
            return ret;
        }

        private byte[] Select(double[] fitness)
        {
            double fitnessSum = fitness.Sum();
            if (fitnessSum <= 0)
            {
                Log("Failed to select parent based on fitness, selecting random parent.");
                return _population[_random.Next(_populationSize)];
            }

            double[] _fitness;
            if (_rank)
            {
                _fitness = fitness
                    .Select(fitnessVal => (double) fitness.Count(fitnessVal2 => fitnessVal2 <= fitnessVal)).ToArray();
                fitnessSum = _fitness.Sum();
            }
            else
            {
                _fitness = fitness;
            }

            double rouletteWheelValue = _random.NextDouble() * fitnessSum;
            string sumName = _rank ? "rank value" : "fitness";
            Log($"Selecting a parent with {sumName} sum {fitnessSum}, rolled {rouletteWheelValue} on roulette wheel.");

            for (int i = 0; i < _populationSize; ++i)
            {
                rouletteWheelValue -= _fitness[i];
                if (rouletteWheelValue <= 0)
                {
                    Log($"Selected a parent with fitness {fitness[i]}.");
                    return _population[i];
                }
            }
            Log($"Selected a parent with fitness {fitness[_populationSize - 1]}.");
            return _population[_populationSize - 1];
        }

        private void Log(string msg)
        {
            Console.WriteLine(msg);
            StreamWriter sw = File.AppendText(_fileName);
            sw.WriteLine(msg);
            sw.Close();
        }
    }
}