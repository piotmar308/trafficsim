﻿using System;
using System.Globalization;
using System.Linq;
using trafficsim.Simulation;
using trafficsim_scenario_transformer;

namespace trafficsim_scenario_optimizer
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length == 0 || args.Length == 1)
            {
                Console.Out.WriteLine("Usage:\n" +
                                      "trafficsim_scenario_optimizer.exe (pathToScenario) genetic binary|gray|lanecount (desiredLength) (simulationTimeInSeconds) (generationLimit) (populationSize) (mutationRate) rank|roulettewheel - runs genetic optimizer\n" +
                                      "trafficsim_scenario_optimizer.exe (pathToScenario) localsearch binary|gray|lanecount (desiredLength) (simulationTimeInSeconds) lowest|highest|random - runs local search optimizer\n" +
                                      "trafficsim_scenario_optimizer.exe (pathToScenario) simulatedannealing binary|gray|lanecount (desiredLength) (simulationTimeInSeconds) lowest|highest|random (cooldownFactor) (epochLength) (failedIterationsToBail) (initialProbabilityOfAcceptingLowerFitness) - runs simulated annealing optimizer");
                return;
            }
            double desiredLength = double.Parse(args[3]);
            int simulationTime = int.Parse(args[4]);
            TransformerMode mode;
            switch (args[2])
            {
                case "binary":
                    mode = TransformerMode.Binary;
                    break;
                case "gray":
                    mode = TransformerMode.Gray;
                    break;
                case "lanecount":
                    mode = TransformerMode.LaneCount;
                    break;
                default:
                    Console.Out.WriteLine("Invalid transformer mode, expected binary|gray|lanecount");
                    return;
            }

            if (args[1] == "genetic")
            {
                int generationLimit = int.Parse(args[5]);
                int populationSize = int.Parse(args[6]);
                double mutationRate = double.Parse(args[7]);
                bool rank;
                switch (args[8])
                {
                    case "rank":
                        rank = true;
                        break;
                    case "roulettewheel":
                        rank = false;
                        break;
                    default:
                        Console.Out.WriteLine("Invalid selection mode, expected rank|roulettewheel");
                        return;
                }
                GeneticOptimizer optimizer = new GeneticOptimizer(args[0], desiredLength, populationSize, mutationRate, 3, simulationTime, mode, generationLimit, rank);
                optimizer.Optimize();
            }
            else if (args[1] == "localsearch" || args[1] == "simulatedannealing")
            {
                LocalSearchStartingPosition start;
                switch (args[5])
                {
                    case "lowest":
                        start = LocalSearchStartingPosition.Lowest;
                        break;
                    case "highest":
                        start = LocalSearchStartingPosition.Highest;
                        break;
                    case "middle":
                        start = LocalSearchStartingPosition.Middle;
                        break;
                    case "random":
                        start = LocalSearchStartingPosition.Random;
                        break;
                    default:
                        Console.Out.WriteLine("Invalid starting position, expected lowest|highest|random");
                        return;
                }

                if (args[1] == "localsearch")
                {
                    LocalSearchOptimizer optimizer = new LocalSearchOptimizer(args[0], desiredLength, 3, simulationTime, mode, start);
                    optimizer.Optimize();
                }
                else
                {
                    double cooldownFactor = double.Parse(args[6]);
                    int epochLength = int.Parse(args[7]);
                    int failedIterationsToBail = int.Parse(args[8]);
                    double initialProbabilityOfAcceptingLowerFitness = double.Parse(args[9]);
                    SimulatedAnnealingOptimizer optimizer = new SimulatedAnnealingOptimizer(args[0], desiredLength, 3, simulationTime, mode, start, cooldownFactor, epochLength, failedIterationsToBail, initialProbabilityOfAcceptingLowerFitness);
                    optimizer.Optimize();
                }
            }
            else
            {
                Console.Out.WriteLine("Invalid optimizer, expected genetic|localsearch");
            }

        }
    }
}